<div class="modal" tabindex="-1" role="dialog" id="modal_comp_new_doc">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Compose New Document</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <label>Title</label>
              <input id="inp_title" type="text" class="form-control" name="">
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <label>Category</label>
              <input id="inp_cat" type="text" class="form-control" value="Quick Notes" name="" list="mycatlist">
              <datalist id="mycatlist">
                
              </datalist>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <label>Add Text</label>
              <textarea id="inp_text" class="form-control" placeholder="Add text here..." rows="7"></textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="AddNewText()"  class="btn btn-primary">Create Document</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>


<div class="modal" tabindex="-1" role="dialog" id="modal_edit_doc">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Document</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" id="inp_i" name="">
        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <label>Title</label>
              <input id="inp_edit_title" type="text" class="form-control" name="">
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <label>Category</label>
              <input id="inp_edit_cat" type="text" class="form-control" value="Quick Notes" name="" list="mycatlist">
              <datalist id="mycatlist">
                
              </datalist>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <label>Add Text</label>
              <textarea id="inp_edit_text" class="form-control" placeholder="Add text here..." rows="7"></textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="EditTextDocument()"  class="btn btn-primary">Save Changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Archive Document</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to archive this document?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Yes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  LoadAllCategories();
  function ArchiveDocument(){
    
  }

  function EditTextDocument(){
        var inp_tt = $("#inp_edit_title").val();
    var inp_cc = $("#inp_edit_cat").val();
    var inp_tx = $("#inp_edit_text").val();
    var inp_i = $("#inp_i").val();
    if(inp_cc != "" && inp_tx != ""){
    $.ajax({
    type: "POST",
    url: "func_admin/func.php",
    data: {tag: "edit_document",inp_title:inp_tt,inp_cat:inp_cc,inp_text:inp_tx,inp_id:inp_i},
    success:function(data){
    // alert(data);
    ReloadTableDocu();
    $("#inp_edit_title").val("");
    $("#inp_edit_text").val("");
    $("#inp_edit_cat").val("");
    $("#modal_edit_doc").modal("hide");
    }
    })
    }else{
      alert("Please add at least the Category and Text...");
    }
  }

    function LoadSingleDocument(controlOBJ){
    var docuid = $(controlOBJ).data("idofdoc");
    $.ajax({
      type: "POST",
      url: "func_admin/func.php",
      data: {tag: "get_document_by_id",idofdocument:docuid},
      success: function(data){

        data = JSON.parse(data);
        // alert(data[0]["text_content"]);
        $("#inp_i").val(data[0]["id"]);
        $("#inp_edit_title").val(data[0]["title"]);
        $("#inp_edit_text").val(data[0]["text_content"]);
        $("#inp_edit_cat").val(data[0]["category"]);
      }
    })
  }

  function AddNewText(){

    var inp_tt = $("#inp_title").val();
    var inp_cc = $("#inp_cat").val();
    var inp_tx = $("#inp_text").val();
    if(inp_cc != "" && inp_tx != ""){
    $.ajax({
    type: "POST",
    url: "func_admin/func.php",
    data: {tag: "insert_new_document",inp_title:inp_tt,inp_cat:inp_cc,inp_text:inp_tx},
    success:function(data){
    alert(data);
    ReloadTableDocu();
    $("#inp_title").val("");
    $("#inp_text").val("");
    $("#modal_comp_new_doc").modal("hide");
    }
    })
    }else{
      alert("Please add at least the Category and Text...");
    }

  }
  function LoadAllCategories(){
    $.ajax({
    type: "POST",
    url: "func_admin/func.php",
    data: {tag: "LoadAllCategories"},
    success:function(data){
    // alert(data);
    $("#mycatlist").html(data);
    }
    })
  }
</script>