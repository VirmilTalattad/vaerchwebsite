<nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-color: #607D8B !important;">
  <a class="navbar-brand" href="#">Vaerch | Control Center</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
<?php 

if(isset($_SESSION["vaerch_admin_email"])){
?>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="admin_sitedashboard.php"><i class="fas fa-arrow-circle-right"></i> Manage Site</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="admin_finance.php"><i class="fas fa-arrow-circle-right"></i> Finance Management</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="vaerch_docus.php"><i class="fas fa-arrow-circle-right"></i> Docus</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0" action="func_admin/func.php" method="POST">
    	<input type="hidden" name="tag" value="sign_out_as_admin">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Sign-Out</button>
    </form>
  </div>

  <?php
}
  ?>
</nav>
<br>
<br>