

<form action="func_admin/func.php" method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="newmoneymodal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">New Finance Record</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" value="addnewfinancevalue" name="tag">
        <div class="form-group">
          <label>Type</label>
          <select class="form-control" required="" name="finance_type">
            <option selected="" value="" disabled="">Choose Finance Record Type</option>
            <option>Daily Expense from Pocket</option>
            <option>Daily Expense from Savings</option>
            <option>Pocket Expense</option>
            <option>Savings Expense</option>
            <option>Wallet Transfer to Saving</option>
            <option>Savings Transfer to Wallet</option>
            <option>Add to Saving</option>
            <option>Add to Pocket</option>
          </select>
        </div>
        <div class="form-group">
          <label>Date of use</label>
          <input type="date" class="form-control"  required=""name="finance_dateofuse">
        </div>
        <div class="form-group">
          <label>Amount</label>
          <input type="number" step="0.01" class="form-control" required="" value="0" name="finance_amount">
        </div>
        <div class="form-group">
          <label>Remarks</label>
          <textarea class="form-control" required="" name="finance_remarks"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Add Record</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form>

<form  action="func_admin/func.php" method="POST">
  <input type="hidden" name="tag" value="deletethis_expense">
 
  <div class="modal" tabindex="-1" role="dialog" id="modal_delexpense">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Archive Finance Record Confirmation</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
           <input type="hidden" name="finance_id" value="" id="finIDofrecord">
          <p>Are you sure you want to archive this finance record?</p>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Yes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
  function ShowArchiveFinanceModal(controlOBJ){
    $("#finIDofrecord").val($(controlOBJ).data("finrid"));
  }
</script>



<div class="modal" tabindex="-1" role="dialog" id="daily_fin_modal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-7">


         <div class="form-group">
          <h4>Expense History per Day<br><small class="lessen">In last 7 days</small></h4>
            <table class="table" id="dylyexptbl">
              <thead>
                <tr class="row">
                  <th class="col-sm-3">Day</th>
                  <th class="col-sm-9">Expenses Summary</th>
                <tr/>
              </thead>
              <tbody id="dailyexphistoryperday">

              </tbody>
            </table>
          </div>
        </div>
        <div class="col-sm-5">
         <!-- xx -->
                   <div class="form-group">
          <div class="card">
          <div class="card-body">
           
            <p class="lessen"><strong><i class="fas fa-plus-circle"></i> Add item(s) you buyed today</strong></p>
            <div class="row">
              <div class="col-sm-12">
                            <div class="form-group">
              <label>Source</label>
             <select class="form-control form-control-sm" id="inp_fs" name="funding_source">
              <option>Pocket Expense</option>
               <option>Savings Expense</option>
             </select>
            </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
              <label>Amount</label>
              <input type="number" step="0.01" id="inp_amount" class="form-control form-control-sm" placeholder="Add amount of expense here..." name="expense_amount">
            </div>
              </div>
            </div>
            
            <div class="form-group">
              <label>Expense Description</label>
              <textarea name="expense_description" id="inp_expdesc" class="form-control form-control-sm" placeholder="ex: Milk, Egg, Medicine...."></textarea>
            </div>

          </div>
          <button type="button" onclick="Add_NewPerdayRecord()" class="btn btn-primary btn-sm btn-block"><i class="far fa-save"></i> Add Report</button>
        </div>
        </div>
         <!-- xx -->
        </div>
        </div>

      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  Load_PerdayExpenses();
 function Load_PerdayExpenses(){
   $.ajax({
    type: "POST",
    url: "func_admin/func.php",
    data: {tag: "getMydailyexpenses"},
    success:function(data){
      // alert(data);
      $("#dailyexphistoryperday").html(data);
      $("#inp_amount").val("0");
      $("#inp_expdesc").val("");
    }
  })
 }
 function Add_NewPerdayRecord(){
  var inp_funding_source = $("#inp_fs").val();
  var inp_expense_amount = $("#inp_amount").val();
  var inp_expense_description = $("#inp_expdesc").val();
   if(inp_expense_amount != 0 && inp_expense_description != ""){
    $.ajax({
    type: "POST",
    url: "func_admin/func.php",
    data: {tag: "AddNewPerdayRecord",funding_source:inp_funding_source,expense_amount:inp_expense_amount,expense_description:inp_expense_description},
    success:function(data){
      Load_PerdayExpenses();
    }
  })
   }else{
    alert("Fill all the required fields!");
   }
 }
</script>