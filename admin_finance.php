<?php
include("theme/index.php");
include("func_admin/auth.php");
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
<title>Vaerch Dot.Admin</title>
<?php include("inc/essentials.php") ?>
</head>
<body>
<?php include("admin_components/nav.php") ?>
	<div class="container">
		<br>
		<h1>Vaerch Finance</h1>
		<hr>
		<h4>My Finance Insights</h4>
		<br>
		<div class="row">
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-12">
						<h4 class="lessen"><i class="fas fa-chart-bar"></i> Status</h4>
						<canvas id="line-chart" width="800" height="200"></canvas>


<script type="text/javascript">
	var coco = new Chart(document.getElementById("line-chart"), {
	type: 'line',
	data: {
	labels: [1500,1600,1700],
	datasets: [{ 
	data: [86,114,106],
	label: "Savings",
	borderColor: "#00a8ff",
	fill: false
	}, { 
	data: [282,350,411],
	label: "Pocket Money",
	borderColor: "#fbc531",
	fill: false
	}, { 
	data: [168,170,178],
	label: "Day Expense",
	borderColor: "#4cd137",
	fill: false
	}
	]
	},
	options: {
	title: {
	display: true,
	text: 'Money Timeline'
	}
	}
	});
	coco.height = 200;
</script>

					</div>
					<div class="col-sm-4">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title col_blue"><i class="fas fa-piggy-bank"></i> Savings Budget<br><small>Money you save on your bank or somewhere else.</small></h5>
						<h1 id="val_total_savings">0</h1>
						<hr>
						<p><small>Lifespan based on <span class="col_green" title="Allowance Per Day"><i class="fas fa-coins"></i> APD</span></small><br>
							<strong id="lifespan_saving">1000 Days</strong></p>
							</div>
							
						</div>
					</div>
					<div class="col-sm-4">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title col_yellow"><i class="fas fa-wallet"></i> Pocket Budget<br><small>Money you have right now on your hands.</small></h5>
						<h1 id="val_totalPocket">0</h1>
						<hr>
						<p><small>Lifespan based on <span class="col_green" title="Allowance Per Day"><i class="fas fa-coins"></i> APD</span></small><br>
							<strong id="lifespan_pocket">1000 Days</strong></p>
							</div>
							
						</div>
					</div>
					<div class="col-sm-4">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title col_green"><i class="fas fa-coins"></i> Allowance Per Day<br><small>Money you spend every day for your daily needs.</small></h5>
							<h1 id="val_perdayallowance">0</h1>
							<hr>
							<p><small>View daily expenses record</small><br>
								<a href="#" data-toggle="modal" data-target="#daily_fin_modal"><i class="fas fa-arrow-circle-right" ></i> Manage</a></p>
							</div>
						</div>
					</div>

				</div>
				<hr>
			</div>
			<div class="col-sm-9">
				<h5>Money Records</h5>
				<table class="table table-striped table-sm" id="myrec_table">
					<thead>
						<tr>
							<td>Amount</td>
							<td>Remarks</td>
							<td>Date</td>
							<td>Recorded</td>
							<td>Type</td>
							<td>Action</td>
						</tr>
					</thead>
					<tbody id="record_of_finance">
						
					</tbody>
				</table>

				<h5>Summary</h5>
				<table class="table table-striped table-sm">
				<thead>
					<tr>
						<td>Record Type</td>
						<td>Total</td>
					</tr>
				</thead>
				<tbody id="sum_of_all">
					
				</tbody>
				</table>
			</div>
			<div class="col-sm-3">
				<h5>Actions</h5>
				<div class="btn-group-vertical ">
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newmoneymodal"><i class="fas fa-plus-circle"></i> New Finance Record</button>
</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>



<script type="text/javascript">
	
	// LOAD ALL FINANCE DATA
	$.ajax({
		type: "POST",
		url: "func_admin/func.php",
		data: {tag:"getallofmyfinancerecords",output_type:"1"},
		success: function(data){
			// alert(data);
			$("#record_of_finance").html(data);
			// SET DATA TABLE 
	$("#myrec_table").DataTable();
		}
	})

	// LOAD ALL FINANCE DATA
	$.ajax({
		type: "POST",
		url: "func_admin/func.php",
		data: {tag:"get_sum_of_finance"},
		success: function(data){
			// alert(data);
			$("#sum_of_all").html(data);
		}
	})



$.ajax({
		type: "POST",
		url: "func_admin/func.php",
		data: {tag:"GetTotalAllowancePerDay"},
		success: function(data){
			// alert(data);
			$("#val_perdayallowance").html(data);
			setup_savings();
			setup_wallet();
		}
	})



function setup_savings(){
		$.ajax({
		type: "POST",
		url: "func_admin/func.php",
		data: {tag:"getTotalSavings"},
		success: function(data){
			// alert(data);
			$("#val_total_savings").html(data);

			var moneyval  = parseInt(parseFloat(data.replace(",","")) / parseFloat($("#val_perdayallowance").html().replace(",",""))) + " Days";
			$("#lifespan_saving").html(moneyval);
		}
	})

}

function setup_wallet(){
	$.ajax({
		type: "POST",
		url: "func_admin/func.php",
		data: {tag:"getTotalPocket"},
		success: function(data){
			// alert(data);
			$("#val_totalPocket").html(data);


			var moneyval  = parseInt(parseFloat(data.replace(",","")) / parseFloat($("#val_perdayallowance").html().replace(",",""))) + " Days";
			$("#lifespan_pocket").html(moneyval);
		}
	})
}




</script>
<?php
include("admin_components/finance_modals.php");
include("components/admin_dash_modals.php");
?>