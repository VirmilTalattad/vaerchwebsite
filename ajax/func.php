<?php
include("../conn/c.php");
$tag = $_POST["tag"];
switch($tag){
	case "getStoreInfo":
	$acc_email = $_POST["acc_email"];
	$q = $c->prepare("SELECT * FROM accounts WHERE email=?");
	if($q->execute([$acc_email])){
		if($q->rowCount() != 0){
			$res = $q->fetchall(PDO::FETCH_ASSOC);
			$account_ID = $res[0]["id"];
			$x = $c->prepare("SELECT * FROM super3d_accounts WHERE account_id=? LIMIT 1");
			if($x->execute([$account_ID])){
				echo json_encode($x->fetchall(PDO::FETCH_ASSOC));
			}
		}
	}
	break;
	case "getweblink":
	$q= $c->prepare("SELECT * FROM super3d_accounts JOIN accounts ON super3d_accounts.account_id = accounts.id WHERE accounts.email=?");

	if($q->execute([$_POST["email_v"]])){
		$res = $q->fetchall(PDO::FETCH_ASSOC);
		echo $res[0]["website"];
	}
	break;
	case "check_acc_elegi":
	$email = $_POST["email"];
	$q = $c->prepare("SELECT * FROM accounts JOIN super3d_accounts ON accounts.id = super3d_accounts.account_id WHERE accounts.email=? AND accounts.is_genuine='1' LIMIT 1");
	if($q->execute([$email])){
		if($q->rowCount() == 1){
			echo "1";
		}else{
			echo "0";
		}
	}else{
			echo "0";
	}
	break;
	case "lod_tac":
	$q= $c->prepare("SELECT * FROM infos WHERE info_name=? LIMIT 1");
	if($q->execute(["tac"])){
		$res = $q->fetchall(PDO::FETCH_ASSOC);
		echo $res[0]["value"];
	}
	break;
	case "load_all_single_category":
		$cat_name = $_POST["cat_name"];

$q = $c->prepare("SELECT * FROM homepage_settings WHERE type='online' AND cat=?");
if($q->execute([$cat_name])){
$res = $q->fetchall(PDO::FETCH_ASSOC);
for($i=0; $i < count($res);$i++){
	?>
<div class="col-sm-3" >
	<form action="asset_landingpage.php" method="GET">
	<button type="submit" href="#" class="btn_link_button" name="cont_num" value="<?php echo $res[$i]['id'] ?>">
	<center>
	<div style='border-radius: 4px; width: 100%; height: 200px; background-image: url(images/homecover/<?php echo $res[$i]["image"]; ?>); background-size: 100%; background-repeat: no-repeat; background-position: center; ' >

	</div>
	<p><?php echo $res[$i]["title"]; ?><br><small style="color: rgba(255,255,255,0.3);"><?php echo $res[$i]["special"]; ?></small></p>

	</center>
</button>
</form>
</div>
	<?php
}
}

	break;
	case 'LoadNewAssets':
		$q = $c->prepare("SELECT * FROM homepage_settings WHERE type='online'  ORDER BY post_timestamp DESC LIMIT 6");
		if($q->execute()){
			$res = $q->fetchall(PDO::FETCH_ASSOC);
			for ($i=0; $i < count($res); $i++) { 
				?>
<div class="col-sm-6">
	<div class="card">
		<div class="row" style="height: 150px;">
			<div class="col-sm-8">
				<div class="card-body">
			<p class="float-right"><i class="fas fa-arrow-circle-right"></i> <?php echo $res[$i]["cat"]; ?></p>
			<p><strong><?php 
			if (strlen($res[$i]["title"]) > 20) {
				echo substr($res[$i]["title"],0, 20) . "...";
			}else{
				echo substr($res[$i]["title"],0, 20);
			}
			 ?></strong><br>
				<small style="color: rgba(255,255,255,0.5);"><?php echo $res[$i]["special"]; ?></small>
			</p>
			
			<form action="asset_landingpage.php" method="GET">
			<button class="btn btn-primary" href="#" name="cont_num" value="<?php echo $res[$i]['id'] ?>"><i class="fas fa-arrow-circle-right"></i> Check it out</button>
			</form>
		</div>
			</div>
			<div class="col-sm-4" >
				 <div class="category_favicon_contentbutton" name="cont_num"  value="<?php echo $res[$i]['id'] ?>" href="#" title="<?php echo $res[$i]['title']; ?>" style="border-radius: 0px; width: 100%; height: 100%;
background-size: 100%; background-repeat: no-repeat; border:none; background-position: center; background-image: url(images/homecover/<?php echo $res[$i]["image"];?>);">
					</div>
			</div>
		</div>
	</div>
</div>
				<?php
			}
		}
		break;
	case "LoadCategories":
		$q= $c->prepare("SELECT * FROM homepage_settings WHERE  type='online' GROUP BY cat ");
		if($q->execute()){
			$res = $q->fetchall(PDO::FETCH_ASSOC);
			for ($i=0; $i < count($res); $i++) { 
			
				?>
<div class="col-sm-6">
	<div class="card">
	<div class="card-body">
		<?php
		$x = $c->prepare("SELECT * FROM homepage_settings WHERE cat='" . $res[$i]["cat"] . "' AND type='online'  ORDER BY RAND() LIMIT 5");
		$x->execute();
		$xres = $x->fetchall(PDO::FETCH_ASSOC);
		?>
		<form action="asset_category_view.php" method="GET">
			<button  type="submit" class="float-right btn_cat" name="catname" value="<?php echo $res[$i]['cat']; ?>" href="#"><i class="fas fa-arrow-right"></i> <?php echo $res[$i]["cat"]; ?></button>
		</form>
		
		<div class="row">
			<?php 

			for ($ix=0; $ix < count($xres); $ix++) { 

				$imgDir = $xres[$ix]["image"];
				$assetname = $xres[$ix]["title"];
				?>
					<div class="col-sm-2">
							<form action="asset_landingpage.php" method="GET">


						 <button class="category_favicon_contentbutton" name="cont_num"  value="<?php echo $xres[$ix]['id'] ?>" href="#" title="<?php echo $assetname; ?>" style="border-radius: 0px; width: 50px; height: 50px;
background-size: 100%; background-repeat: no-repeat; background-position: center; background-image: url(images/homecover/<?php echo $imgDir;?>);">
					</button>
					</form>
					</div>
				<?php
			}
			?>
		</div>
	</div>
</div>
</div>
				<?php
			}
		}
	break;
	case "lod_download_bttn":
		$cont_number = $_POST["cont_number"];
		$q = $c->prepare("SELECT * FROM homepage_settings WHERE id=? LIMIT 1");
		if($q->execute([$cont_number])){
			$res = $q->fetchall(PDO::FETCH_ASSOC);
			echo "uploads_packages/assets/" . $res[0]["secondary_file"];
		}
	break;
case "load_coverphoto_latest":
$q = $c->prepare("SELECT * FROM homepage_settings WHERE type='cover' ORDER BY id DESC LIMIT 1");
if($q->execute()){
echo json_encode($q->fetchall(PDO::FETCH_ASSOC));
}
break;
case "load_blogs":
$q = $c->prepare("SELECT * FROM homepage_settings WHERE type='blog' ORDER BY id DESC LIMIT 10");
if($q->execute()){
$res = $q->fetchall(PDO::FETCH_ASSOC);
for($i=0; $i < count($res);$i++){
	?>
<div>
	<small><?php echo date("F d, Y",strtotime($res[$i]["post_timestamp"])); ?></small>
	<h4><?php echo $res[$i]["title"]; ?></h4>
	<img style='border-radius: 4px; max-width:100%; max-height: 300px;' src="images/homecover/<?php echo $res[$i]['image']; ?>">
	<pre class="perfectpre" style="font-family: san; color: rgba(255,255,255,0.7);"><?php echo $res[$i]["description"]; ?></pre>
	<p class="float-right" style="color: rgba(255,255,255,0.3);"><?php echo $res[$i]["special"]; ?></p>
	<a class="btn btn-primary" href="<?php echo $res[$i]['link']; ?>" target="_blank"><?php
		if($res[$i]["link_title"] == ""){
			echo "More";
		}else{
			echo $res[$i]["link_title"];
		}
	 ?></a>
	<hr style="border-top: 1px solid rgba(255,255,255,0.1); border-bottom: 1px solid rgba(0,0,0,0.3);">
</div>
	<?php
}
}
break;

case "load_games":
$q = $c->prepare("SELECT * FROM homepage_settings WHERE type='game' ORDER BY id DESC LIMIT 5");
if($q->execute()){
$res = $q->fetchall(PDO::FETCH_ASSOC);
for($i=0; $i < count($res);$i++){
	?>
<div>


			<img style='border-radius: 4px; height: 100px; width: 100px;' src="images/homecover/<?php echo $res[$i]['image']; ?>">
			<h5><?php echo $res[$i]["title"]; ?></h5>
	
	<p style="font-family: san; color: rgba(255,255,255,0.7);"><?php echo $res[$i]["description"]; ?><br><small style="color: rgba(255,255,255,0.3);"><?php echo $res[$i]["special"]; ?></small></p>
		<a class="btn btn-primary" href="<?php echo $res[$i]['link']; ?>" target="_blank"><?php
		if($res[$i]["link_title"] == ""){
			echo "More";
		}else{
			echo $res[$i]["link_title"];
		}
	 ?></a>

	


	<hr style="border-top: 1px solid rgba(255,255,255,0.1); border-bottom: 1px solid rgba(0,0,0,0.3);">
</div>
	<?php
}
}
break;


case "load_online":
$size = $_POST["size"];

$q = $c->prepare("SELECT * FROM homepage_settings WHERE type='online' ORDER BY RAND() LIMIT 8");
if($q->execute()){
$res = $q->fetchall(PDO::FETCH_ASSOC);
for($i=0; $i < count($res);$i++){
	?>
<div class="col-sm-<?php echo $size; ?>" >
	<form action="asset_landingpage.php" method="GET">
	<button type="submit" href="#" class="btn_link_button" name="cont_num" value="<?php echo $res[$i]['id'] ?>">
	<center>
	<div style='border-radius: 4px; width: 100%; height: 200px; background-image: url(images/homecover/<?php echo $res[$i]["image"]; ?>); background-size: 100%; background-repeat: no-repeat; background-position: center; ' >

	</div>
	<p><?php echo $res[$i]["title"]; ?><br><small style="color: rgba(255,255,255,0.3);"><?php echo $res[$i]["special"]; ?></small></p>

	</center>
</button>
</form>
</div>
	<?php
}
}

break;
case "load_content_info":
$cont_number = $_POST["cont_number"];
$q = $c->prepare("SELECT * FROM homepage_settings WHERE id=?LIMIT 1");

if($q->execute([$cont_number])){
	echo json_encode($q->fetchall(PDO::FETCH_ASSOC));
}
break;

}
?>