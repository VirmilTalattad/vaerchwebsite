<?php
include("theme/index.php");
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
<title>PIXL ONE - Services</title>

<?php include("inc/essentials.php") ?>
</head>
<style>
.jumbotron{
    background-size:cover;
    background-repeat: no-repeat;
    background-attachment:fixed;
    background-position:center;
}
</style>
<body>
<?php 
include("inc/body_es.php");
 ?>
<div class="container">
<div class="jumbotron jumbotron-fluid">
	<div class="container">
		<center>
		    <h1>
		PIXL ONE
		</h1>
		<span>Computer Systems, Video/Sound/Graphics Production, Maintenance, Software/App/Games Installation</span>
		</center>
	</div>
</div>
		<div class="row">
			<div class="col-sm-9">
				<div class="form-group">
					<h4>See our offers</h4>
					<div class="jumbotron" style='background-image:url(https://images.pexels.com/photos/158826/structure-light-led-movement-158826.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260);'>
						<h1 id="p_tech">TECHNICAL & ETC</h1>
					</div>
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td><p>Computer Virus Removal & Optimization</p></td>
								<td>Includes installing an Anti-Virus software to protect your computer from viruses and computer optimization to increase performance.</td>
							</tr>
							<tr>
								<td><p>Operating System (Reformat)</p></td>
								<td>
									<h5>(Pack 1) Reformat Only</h5>
									<span>Re-install, Upgrade, Downgrade your computers Operating System to Windows XP, Vista, Windows 7, 8, 10 with Divers</span>
									<h5>(Pack 2)Essentials</h5>
									<span>Inlcudes (Pack 1) and installation of Microsoft Office and Antivirus Software, CD DVD Burning Software.</span>
								</td>
							</tr>
														<tr>
								<td><p>pfSense Setup</p></td>
								<td>pfSense is an open source firewall/router computer software distribution based on FreeBSD. It is installed on a physical computer or a virtual machine to make a dedicated firewall/router for a network. </td>
								
							</tr>
								<tr>
								<td><p>Diskless  Setup</p></td>
								<td>A diskless node (or diskless workstation) is a workstation or personal computer without disk drives, which employs network booting to load its operating system from a server. (A computer may also be said to act as a diskless node, if its disks are unused and network booting is used.)</td>
								
							</tr>
								<tr>
								<td><p>Pisonet Setup</p></td>
								<td>A pisonet is an arcade style computer unit used as an internet or gaming vending machine. </td>
								
							</tr>
								<tr>
								<td><p>Special Software/Apps/Games Installation</p></td>
								<td>
									Includes installing some 3rd party programs in your computer like (Microsoft Office, Autocad, Photoshop, After Effects, FL Studio, Maya, Autodesk 3ds Max, Blender... etc)
								</td>
							</tr>
						</tbody>
					</table>

					<div class="jumbotron" style='background-image:url(https://images.pexels.com/photos/1044989/pexels-photo-1044989.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260);'>
						<h1 id="p_graph">GRAPHICS</h1>
					</div>
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td><p>Logo Design</p></td>
								<td>Designing a logo for Business or Personal purposes.</td>
							</tr>
							<tr>
								<td><p>Product Cover, Poster, Tarpaulin, Banner, etc...</p></td>
								<td>Layout a Tarpaulin, Product Cover, Ad, Invitation, etc... for Business or Personal purposes.</td>
							</tr>
								<tr>
								<td><p>Photo Manipulation/ Restoration/ Enhance</p></td>
								<td>Photo Vectorizing, Color Manipulation, Cartoon Art, etc...</td>
							</tr>
							</tr>
								<tr>
								<td><p>Content Editor</p></td>
								<td>VLOGS, MONTAGES, etc...</td>
							</tr>
						</tbody>
					</table>
					<div class="jumbotron" style='background-image:url(https://images.pexels.com/photos/257904/pexels-photo-257904.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260);'>
						<h1 id="p_mus">MUSIC & SOUNDS</h1>
					</div>
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td><p>Audio/ Music Production</p></td>
								<td>Music for Games, Songs, Remix, Video, etc...</td>
							</tr>
						</tbody>
					</table>
					<div class="jumbotron" style='background-image:url(https://images.pexels.com/photos/2004161/pexels-photo-2004161.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260);'>
						<h1 id="p_web">Web DEVELOPMENT/ DESIGN</h1>
					</div>
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td><p>Web Design</p></td>
								<td>Design a website for your Business, Personal Profile, Blog, VLOG, Online Portfolio that has no database included.</td>
							</tr>
							<tr>
								<td><p>Web Information System</p></td>
								<td>Design an entire online system depending on your business needs.</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
					<div class="card">
					<div class="card-body">
						<h4>Our Services</h4>
				<div class="form-group">
					<a href="#p_tech"> PC Repair and etc...</a>
				</div>
				<div class="form-group">
				<a href="#p_graph"> Graphics</a>
				</div>
				<div class="form-group">
				<a href="#p_mus"> Music</a>
				</div>
				<div class="form-group">
				<a href="#p_web"> Website & Info Systems</a>
				</div>
					</div>
				</div>
				</div>
				<div class="form-group">
					<div class="card">
						<div class="card-body">
							<h4>For Inquiries</h4>
							<div class="form-group">
								<p class="lessen">Main</p>
							<span>Email:<br><strong>pixlone.services@gmail.com</strong></span>
							</div>
							<div class="form-group">
								<p class="lessen">Secondary</p>
							<span>Globe:<br><strong># 09273300519<br># 09552983543</strong></span>
							<hr>
							<span>Smart:<br><strong># 09501724031</strong></span>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
</div>
</body>
</html>