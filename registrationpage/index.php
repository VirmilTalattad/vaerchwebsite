<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Vaerch Accout | Registration Page</title>
	<?php  include("../theme/external_theme.php"); ?>
	<?php  include("../components/res.php"); ?>
	<?php include("../inc/essentials.php") ?>
  <style type="text/css">
      body{
        /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ffffff+0,aaaaaa+100 */
background-image: url(../images/v1.jpg);
background-size: cover;
background-repeat: no-repeat;
background-attachment: fixed;
  }
  </style>
</head>
<body>
  <br>
<div class="container">
	<img class="float-right" src="../images/vaerch.png" style="width: 70px;">
	<h4>Vaerch Account | Registration<br><small><?php echo $_GET['vaerchappname'] ?></small></h4>
	<hr>
	<form action="<?php weblink_uni(); ?>" method="POST" style="max-width: 350px;">
		<input type="hidden" name="creationplace" value="<?php echo $_GET['vaerchappname'] ?>">
	   <input type="hidden" name="tag" value="createvaerchaccount">
            <div class="form-group">
              <label>What kind of a performer are you?</label>
          <select class="form-control form-control-lg" required="" name="v_accounttype">
            <option selected="" disabled="">Choose who you are...</option>
            <option>Graphic Artist</option>
            <option>Web Developer</option>
            <option>Desktop Developer</option>
            <option>Game Developer</option>
            <option>Music Composer</option>
            <option>Video Editor</option>
            <option>Technician</option>
            <option>Comedian</option>
            <option>Dancer</option>
            <option>Singer</option>
            <option>Photographer</option>
            <option>Vlogger</option>
            <option>Painter</option>
            <option>Model</option>
            <option>Inspirer</option>
            <option>Gamer</option>
          </select>
          </div>
          <div class="form-group">
            <label>Username</label>
            <input class="form-control" type="text" name="v_username" required="" autocomplete="off">
          </div>
           <div class="form-group">
            <label>Email</label>
            <input class="form-control" type="email" name="v_email" required="" autocomplete="off">
          </div>
          <div class="row">
            <div class="col-sm-6">
               <div class="form-group">
            <label>Password</label>
            <input class="form-control" type="Password" name="v_pass" required="">
          </div>
            </div>
            <div class="col-sm-6">
               <div class="form-group">
            <label>Re-enter Password</label>
            <input class="form-control" type="Password" name="v_repass" required="">
          </div>
            </div>
          </div>
           <div class="form-group">
            <label>Sex</label>
           <select class="form-control" name="v_sex" required="">
              <option value="1">Male</option>
              <option value="0">Female</option>
           </select>
          </div>
          <div class="form-group">
            <label>Date of Birth:</label>
          <input type="date" class="form-control" name="v_dateofbirth" required="">
          </div>
           <div class="form-group">
            <label>Mobile Number:</label>
          <input type="text" class="form-control" name="v_contact" required="" autocomplete="off">
          </div>
          <hr>
          <div class="form-group">
          	<button type="submit" class="btn btn-primary float-right">Register for a Vaerch Account</button>
            <br>
            <br>
          </div>
          </form>

</div>
</body>
</html>