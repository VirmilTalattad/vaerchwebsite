<?php
include("inc/session_page.php");
include("theme/index.php");
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
<title>Vaerch Official Website</title>
<?php include("inc/essentials.php") ?>
</head>
<body>
<?php include("inc/body_es.php") ?>

<div class="container">
	<br>
	<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="asset_home.php"><i class="fas fa-home"></i> Super 3D Home</a></li>
    <li class="breadcrumb-item active" aria-current="page" id="cont_title_bread"></li>
  </ol>
</nav>
<div class="row">
	<div class="col-sm-3" style="height: 300px;">
		<div id="cont_image" src="" style="height: 100%; width: 100%; background-size: 100%; background-repeat: no-repeat; background-position: center; "></div>
	</div>
	<div class="col-sm-9" style="height: 300px;">
		<small style="color: rgba(255,255,255,0.5);" id="cont_author">Author name goes here...</small>
		<h1 id="cont_title">Title of content</h1>

		<pre class="perfectpre" style="font-family: san; color: white; width: 100%; height: 150px; overflow: auto; " id="cont_desc"></pre>
		<button class="btn btn-primary" data-toggle="modal" data-target="#modal_download"><i class="fas fa-download"></i> Download</button>
	</div>
	<div class="col-sm-12">
		<p style="text-align: right;"><strong>Published: </strong> <span id="cont_datepublished">April 11, 2019</span></p>
		<hr>
		<h2>Screenshots</h2>
		<div class="row">
			<div class="col-sm-6" style="height: 300px;">
				<div src="" id="img_screenshots_1" style="height: 100%; width: 100%; background-size: 100%; background-repeat: no-repeat; background-position: center; border-radius: 4px;"></div>
			</div>
			<div class="col-sm-6" style="height: 300px;">
				<div src="" id="img_screenshots_2" style="height: 100%; width: 100%; background-size: 100%; background-repeat: no-repeat; background-position: center; border-radius: 4px;"></div>
			</div>
		</div>
	</div>
	<div class="col-sm-12">
		<hr>
		<h4>Reviews</h4>
		<div class="card">
			<div class="card-body">
				<form action="" method="POST">
					<div class="form-group">
						<textarea class="form-control" placeholder="Type your review here..."></textarea>
					</div>
					<div class="form-group">
						<button class="btn btn-primary">Post Review</button>
					</div>
				</form>
				<hr>
			</div>
		</div>
	</div>
	<div class="col-sm-12">
		<br>
		<h4>Check out more content<br><small>The're more amazing things behind the corners.</small></h4>
		<div class="card">
			<div class="card-body" >
				<div class="row" id="conts_relate">
					
				</div>
			</div>
		</div>
	</div>
</div>

</div>
</body>
</html>

<?php
if(isset($_SESSION["vaerch_email"])){
	?>
<div class="modal" tabindex="-1" role="dialog" id="modal_download">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<h4><i class="fas fa-file-archive"></i> Download Content</h4>
        <div class="container">
        	<p>Download this content from Super 3D?</p>
        	<div class="form-group">
        		<div class="card">
        			<div class="card-header">
        				Content Information
        			</div>
        		<div class="card-body">
        			
        		</div>
        	</div>
        	</div>
        <a download="" href="#" id="cont_down_link"  class="btn btn-primary btn-block" ><i class="fas fa-download"></i> Get this content</a>
        </div>

      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	lod_accinf();

	function lod_accinf(){
		// cont_num
		$.ajax({
		type: "POST",
		url : "ajax/func.php",
		data: {tag:"lod_download_bttn",cont_number:<?php echo json_encode($_GET["cont_num"]); ?>},
		success: function(data){
		$("#cont_down_link").prop("href",data);
		}
	})
	}
</script>
	<?php
}else{
	?>

<div class="modal" tabindex="-1" role="dialog" id="modal_download">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<h4>Login to Download</h4>
      	<p>Vaerch</p>
      	<hr>
        <p>Already have an account?<br>
        	<a class="btn btn-primary btn-block" href="vlogin.php">Login</a>
        </p>
        <p>No account yet?<br>
        	<a class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal_vaerchacc_signin" data-dismiss="modal">Register now for free!</a>
        </p>
      </div>
    </div>
  </div>
</div>

	<?php
}
?>



<script type="text/javascript">
	LoadOnline();
	LoadContent();

	function LoadContent(){
		// cont_num
		$.ajax({
		type: "POST",
		url : "ajax/func.php",
		data: {tag:"load_content_info",cont_number:<?php echo json_encode($_GET["cont_num"]); ?>},
		success: function(data){
			data = JSON.parse(data);
			$("#cont_author").html(data[0]["special"]);
			$("#cont_title").html(data[0]["title"]);
			$("#cont_title_bread").html(data[0]["title"]);
			$("#cont_desc").html(data[0]["description"]);
			$("#cont_image").css("background-image", "url(images/homecover/" + data[0]["image"] + ")");

			$sc_array = JSON.parse(data[0]["screenshots"]);
			// alert($sc_array[0]);
			$("#img_screenshots_1").css("background-image","url(uploads_packages/screenshots/" + $sc_array[0] + ")");
			$("#img_screenshots_2").css("background-image","url(uploads_packages/screenshots/" + $sc_array[1] + ")");

			$("#cont_datepublished").html(data[0]["post_timestamp"]);

		}
	})
	}
  	function LoadOnline(){
  		$.ajax({
		type: "POST",
		url : "ajax/func.php",
		data: {tag:"load_online",size:"3"},
		success: function(data){
			$("#conts_relate").html(data);
		}
	})
  	}
</script>