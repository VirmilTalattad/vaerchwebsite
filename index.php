<?php
include("inc/session_page.php");
include("theme/index.php");
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
<title>Vaerch Official Website</title>
<?php include("inc/essentials.php") ?>
</head>
<body>
<?php include("inc/body_es.php") ?>

	<div class="jumbotron jumbotron-fluid jumbotron-max frontimg" id="cov_image" >
		<div class="fader">
			<center>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<h1 id="cov_title">Sword of Heroes</h1>
				<small id="cov_desc">An epic new RPG game for PC and Android that travels back from the origin story of Ronietite.</small>
				<br>
				<p style="font-size: 10px;"><i class="far fa-circle"></i></p>
				<p id="cov_special">Coming this year.</p>
				
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4">
						<a id="cov_link" target="_blank" href="vaerch_subs.php" class="btn btn-primary">Subscribe Now</a>
					</div>
					<div class="col-sm-4"></div>
				</div>
			</center>
		</div>
	</div>




<div class="container">

<br>
<div class="row">
	<div class="col-sm-12">
		<h1>FEATURED</h1>
	</div>
		<div class="col-sm-6">
		<div class="card">
			<div class="card-body" >
				<a href="#" class="float-right"><i class="fab fa-google-play"></i> Google Play</a>
				<h1>GAMES</h1>
				<p style="color: rgba(255,255,255,0.5)">Play our games!</p>
				<div id="games_cont" style="overflow: auto; max-height: 500px;">
	
				</div>
			</div>
		</div>
		<br>
	</div>
	<div class="col-sm-6">
		<div class="card">
			<div class="card-body" >
				<a href="asset_home.php" class="float-right"><i class="fas fa-shopping-bag"></i> Vaerch Super 3D</a>
				<h1>ASSETS</h1>
				<p style="color: rgba(255,255,255,0.5)">Use our assets to make your own games!</p>
				<div id="online_cont" class="row" style="overflow: auto; max-height: 500px;">
					
				</div>
			</div>
		</div>
		<br>
	</div>
	<div class="col-sm-12">
		<h1>Vaerch Blog</h1>
	</div>
	<div class="col-sm-12">
		<div class="card">
			<div class="card-body">

				<div id="blog_cont">
					
				</div>
			</div>
		</div>
		<br>
	</div>
	<div class="col-sm-12">
		<h1>Follow our Online Community</h1>
		<div style="font-size: 20px;">
			<div  class="grid-list">
				<a href=""><i class="fab fa-facebook"></i> Facebook</a>
				<a href=""><i class="fab fa-youtube"></i> Youtube</a>
				<a href=""><i class="fab fa-twitter"></i> Twitter</a>
			</div>
		</div>
		</div>
	</div>
</div>
</div>
</body>
</html>


	<script type="text/javascript">

	// $(document).ready(function(){
  	LoadCover();
	LoadBlog();
	LoadGames();
	LoadOnline();
	function LoadGames(){
  		$.ajax({
		type: "POST",
		url : "ajax/func.php",
		data: {tag:"load_games"},
		success: function(data){
			$("#games_cont").html(data);
		}
	})
  	}
  	function LoadOnline(){
  		$.ajax({
		type: "POST",
		url : "ajax/func.php",
		data: {tag:"load_online",size:"6"},
		success: function(data){
			$("#online_cont").html(data);
		}
	})
  	}
  	function LoadBlog(){
  		$.ajax({
		type: "POST",
		url : "ajax/func.php",
		data: {tag:"load_blogs"},
		success: function(data){
			$("#blog_cont").html(data);
		}
	})
  	}
  	function LoadCover(){
  		$.ajax({
		type: "POST",
		url : "ajax/func.php",
		data: {tag:"load_coverphoto_latest"},
		success: function(data){

			data = JSON.parse(data);
			// alert( data[0]["image"]);
			$("#cov_title").html(data[0]["title"]);
			$("#cov_desc").html(data[0]["description"]);
			$("#cov_special").html(data[0]["special"]);
			$("#cov_link").prop("href",data[0]["link"]);
			$("#cov_image").css("background-image", "url(images/homecover/" + data[0]["image"] + ")");

		
		}
	})
  	}
// });



</script>