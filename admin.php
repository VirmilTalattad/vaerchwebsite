<?php
include("theme/index.php");
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
<title>Vaerch Dot.Admin</title>
<?php include("inc/essentials.php") ?>
</head>
<body>
<?php include("admin_components/nav.php") ?>
	<div class="container">
		<div class="row">
		<div class="col-sm-4">
			
		</div>
		<div class="col-sm-4">
			<br>
			<h1>Dot.Admin</h1>
			<small>Vaerch Site Manager</small>
			<br>
			<form action="func_admin/func.php" method="POST">
				<input type="hidden" name="tag" value="login_as_admin">
				<div class="form-group">
					<label>Username</label>
					<input type="email" autocomplete="off" class="form-control" name="vv_username_admin">
				</div>
				<div class="form-group">
					<label>Password</label>
					<input type="password" autocomplete="off" class="form-control" name="vv_password_admin">
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Sign-in</button>
				</div>
			</form>
		</div>
		<div class="col-sm-4">
			
		</div>
	</div>
	</div>
</div>
</body>
</html>