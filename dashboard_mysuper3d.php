<?php
include("inc/auth.php");
include("theme/index.php");
include("func/displayer.php");
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
<title>Perfomer - My Super 3D</title>
<?php include("inc/essentials.php") ?>
</head>
<body>
<?php include("inc/body_es.php") ?>
<br>
<style type="text/css">
  .fonttocustomize{
    transition: 0.2s all;
  }
  .text_black{
    color: black;
  }
  .text_white{
    color: white;
  }
  .text_backgroundworthy{
    color: white;
    text-shadow: 0px 2px 30px rgba(0,0,0,0.7);
  }
</style>
<div class="container">
<div class="row">
  <div class="col-sm-2">
    <a href="#" title="Change Profile Picture..." data-toggle="modal" data-target="#modal_changepropic">
      <img src="<?php echo $_SESSION['profilepicture']; ?>" style="width: 60px; height: 60px; background-color: rgba(0,0,0,0.5); border-radius: 4px;">
    </a>
    <h6><?php echo $_SESSION["vaerch_name"]; ?><br><strong><?php echo $_SESSION["vaerch_acctype"]; ?></strong><br><small><?php echo $_SESSION["vaerch_email"]; ?></small></h6>
    <div class="card">
<?php include("inc/accountnav.php"); ?>
    </div>
  </div>
    <div class="col-sm-10">
      <div id="cont_s1" style="display: none;">
        <h1>Super 3D</h1>
      <p>Share your creation to the world without cost. Support out Indie Developers Community.</p>
      <div class="card">
        <div class="card-body">
          <h4>Publish your creation to Super 3D for free.</h4>
            <p>You currently not registered to Super 3D yet. If you want to publlish your creations to Super 3D, you must register your account first by clicking the "Register" button below.</p>
            <button class="btn btn-primary"  data-toggle="modal" data-target="#modal_reg_super3d">Register</button>
        </div>
      </div>
      </div>
      <div id="cont_s2" style="display: none;">
        <h1>Dashboard</h1>

        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Creation</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Merchant's Profile</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Super 3D Account Setting</a>
          </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
          <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
            <!-- CREATION PAGE -->
                   <div class="row">
         <div class="col-sm-6">
           <div class="card">
             <div class="card-body">
              <h4>Publish</h4>
              <p>Publish a new creation to Super 3D</p>
                <button data-toggle="modal" data-target="#modal_postnewacreation" class="btn btn-primary"><i class="fas fa-upload"></i> New</button>
             </div>
           </div>
         </div>
       </div>
       <br>
        <h4>Published Creations</h4>
        <table class="table table-striped table-bordered" id="dt_pubs">
          <thead>
            <tr>
              <th>Name</th>
              <th>Ratings</th>
              <th>Downloads</th>
              <th>Status</th>
            </tr>
          </thead>
        </table>
          </div>
          <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
            <div class="row">
              <div class="col-sm-12">
                <h1 id="merch_name">MERCHANT NAME</h1>
              </div>
              <div class="col-sm-3">
                 <p>Icon</p>
                  <div src="" id="merch_icon" style="height: 256px; width: 100%;" class="perfect_fit"></div>
                  <div class="form-group">
                    <button class="btn btn-primary" id="btn_change_icon" data-toggle="modal" data-target="#modal_chnew_merch">Change</button>
                  </div>
              </div>
              <div class="col-sm-9">
                <p>Cover Photo</p>
                  <div src="" id="merch_cover" style="height: 256px; width: 100%;" class="perfect_fit"></div>
                  <div class="form-group">
                    <button class="btn btn-primary" id="btn_change_cover" data-toggle="modal" data-target="#modal_chnew_merch">Change</button>
                  </div>
              </div>
            </div>
            <form action="func/server.php" method="POST" enctype="multipart/form-data">
              <div class="modal" tabindex="-1" role="dialog" id="modal_chnew_merch">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-body">
                      <h4 id="lbl_changetitle">Change Logo</h4>
                      <input type="hidden" id="uploadtype" name="uptype">
                      <div class="form-group">
                        <input type="hidden" name="tag" value="changmerchlogo">
                        <label>Choose a new logo from your computer.</label>
                        <input type="file" required="" name="newlogo">
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary" id="btn_changepro">Change Logo</button>
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            <script type="text/javascript">

              $("#btn_change_icon").click(function(){
                $("#lbl_changetitle").html("Change Merchant Icon");
                $("#btn_changepro").html("Change Icon");
                $("#uploadtype").val("merch_icon");
              })
               $("#btn_change_cover").click(function(){
                $("#lbl_changetitle").html("Change Merchant Cover");
                $("#btn_changepro").html("Change Cover");
                $("#uploadtype").val("merch_cover");
              })

              var accemail =  <?php echo json_encode($_SESSION["vaerch_email"]); ?>;
              // alert(accemail);
              $.ajax({
                type: "POST",
                url: "ajax/func.php",
                data: {tag: "getStoreInfo","acc_email": accemail},
                success: function(data){
    json_storeinfo = JSON.parse(data);
    $("#merchname_forposting").val(json_storeinfo[0]["merch_name"]);
    $("#merchwebsite_forposting").val(json_storeinfo[0]["website"]);
    $("#merch_name").html(json_storeinfo[0]["merch_name"]);
    $("#merch_icon").css("background-image","url(uploads/" + accemail + "/super3d/" + json_storeinfo[0]["merch_icon"]);
    $("#merch_cover").css("background-image","url(uploads/" + accemail + "/super3d/" + json_storeinfo[0]["merch_cover"]);
                }
              })
            </script>
            <!-- MERCHATS PAGE -->
           
          </div>
          <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
            <!-- SUPER 3D ACCOUNT SETTINGS -->
            <div class="row">
              <div class="col-sm-12">
                <div class="card">
                  <div class="card-body">
                    <h1>Account Deactivation</h1>
                <p>Account deactivation hides your Super 3D account from the Super 3D Asset store until you switch it back on.</p>
                <ul>
                  <li>Nothing gets deleted.</li>
                  <li>Hides Super 3D account from the Super 3D Asset Store.</li>
                  <li>Easy to re-activate.</li>
                </ul>
                <button class="btn btn-primary">Deactivate my Super 3D Account</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>

</div>
</div>
</body>
</html>
<?php
  include("components/mysuper3ddashboard.php");
?>
<script type="text/javascript">
  
  CheckSuper3DAcc(<?php echo json_encode($_SESSION["vaerch_email"]); ?>);
  function CheckSuper3DAcc(acc_email){
     // alert(acc_email);
    $.ajax({
      type: "POST",
      url: "ajax/func.php",
      data: {tag:"check_acc_elegi",email: acc_email},
      success: function(data){
        // alert(data);
        if(data == "0"){
            $("#cont_s1").css("display","block");
             $("#cont_s2").css("display","none");
        }else if(data == "1"){
           $("#cont_s1").css("display","none");
           $("#cont_s2").css("display","block");
        }
      }
    })
  }
  setInterval(function(){
  $(".fonttocustomize").css("font-size",$("#fontsizerange").val() + "px");
  $(".fonttocustomize").css("font-family",$("#cus_fontstyle").val());
  $(".fonttocustomize").css("text-align",$("#cus_textalign").val());
  $(".fonttocustomize").css("color",$("#cus_color").val());
  var stylecode = '[{"font-size":"' + $("#fontsizerange").val() + 'px","font-family":"' + $("#cus_fontstyle").val() +'","text-align":"' + $("#cus_textalign").val() +'","color":"' + $("#cus_color").val() + '"}]';
  $("#text_stylecode").val(stylecode);


  },500)

  function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#postpanel').css('background-image', "url(" +  e.target.result + ")");
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#id_bgimageupload").change(function() {
  readURL(this);
});
// $("#dt_pubs").DataTable();
</script>