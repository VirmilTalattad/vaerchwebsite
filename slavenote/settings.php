<?php include("inc/auth.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Slavenote | Dashboard</title>
	<?php include("theme/theme.php"); ?>
	<?php include("func/displayer.php"); ?>
	<?php include("func/updater.php"); ?>
	<?php include("inc/res.php"); ?>
	<?php include("switcher/mode.php"); ?>
</head>
<body style="background-color: #D9D9D9;">
	<?php include("components/navbar.php"); ?>
	<div class="container">

		<h1>SETTINGS</h1>
		<hr>
		<h2>DASHBOARD</h2>
		<div class="card">
			<div class="card-body">
				<h1>Show only the Note title, not it's inner text.</h1>
				<p>Provides lesser detail of the note's icon so the inner content of your note is not easily readable on the dashboard.</p>
				<label class="switch">
				<input type="checkbox" id="control_showmoredetsett">
				<span class="slider"></span>
				</label>
			</div>
		</div>
		<div class="card">
			<div class="card-body">
				<h1>Note Font</h1>
				<p>Is the default font ridiculus? is it getting in your nerves?... hold my beer.</p>
				<select class="form-control" id="control_selectedfont">
					<option value="slave_defaultfont" >Default Font</option>
					<option value="inconsolata" >Inconsolata</option>
				</select>
			</div>
		</div>

</body>
</html>

<script type="text/javascript">
	SyncSettings();
	function SyncSettings(){
		$.ajax({
			type:"POST",
			url: "<?php weblink(); ?>",
			data: {tag:"getsettingsvalues"},
			success: function(data){
				data = JSON.parse(data);
				if(data[0]["lessdetail"] == "1"){
					$("#control_showmoredetsett").prop("checked",true);
				}else{
					$("#control_showmoredetsett").prop("checked",false);
				}
				$("#control_selectedfont").val(data[0]["font"]);

				
			}
		})
	}
	$("#control_showmoredetsett").change(function(){
		
		ChangeSetting_ShowMoreDetails(this);
	})
	$("#control_selectedfont").change(function(){
		ChangeSetting_NoteFont(this);
	})
	function ChangeSetting_NoteFont(content){
		content = $(content).val();
		 $.ajax({
 			type: "POST",
 			url: "<?php weblink(); ?>",
 			data: {tag:"cprefs_notefont",newval:content}
 		})
	}
 	function ChangeSetting_ShowMoreDetails(content){
		if($(content).prop("checked")){
			content = 1;
		}else{
			content = 0;
		}
 		$.ajax({
 			type: "POST",
 			url: "<?php weblink(); ?>",
 			data: {tag:"cprefs_notelessdetail",newval:content}
 		})
 	}
</script>