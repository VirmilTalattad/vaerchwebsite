<nav class="navbar navbar-expand-lg">
  <a class="navbar-brand" href="dashboard.php" style="color: white; text-shadow: 1px 1px 0px rgba(0,0,0,1);">(⌣̩̩́_⌣̩̩̀) SLAVENOTE</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span style="color: white;">More..</span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          File
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modal_newnote"><img src="images/icons/addnote.svg" class="minicon">  New Note</a>
          <a class="dropdown-item" onclick="loadtrashnotes();" href="#" data-toggle="modal" data-target="#modal_trash"><img src="images/icons/deletenote.svg" class="minicon"> Trash</a>
           <div class="dropdown-divider"></div>
            <form>
            	<a class="dropdown-item" href="#" onclick="logout_slavenote()"><img src="images/icons/logout.svg" class="minicon"> Log-out</a>
            </form>
        </div>
      </li>
            <li class="nav-item">
        <a class="nav-link" href="settings.php">
          Settings
        </a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" id="slavesearch" type="search" placeholder="Search Slaves..." aria-label="Search">
  
    </form>
  </div>
</nav>
    <span class="searchpanel" style="display: none;">
    	<div class="searchpanel-header">
    		<span>Search Result</span>
    	</div>
    	<div class="searchpanel-body" id="searchresultpanel">
    		Loading...
    	</div>
      </span>

	<div class="modal" tabindex="-1" role="dialog" id="modal_newnote">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">New Slavenote</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      <div class="row">
      	<div class="col-sm-8">
      		<div class="form-group">
      	<label>Filename</label>
      	<input style="font-family: <?php echo $_SESSION["prefs_font"]; ?>;" type="text" autocomplete="off" id="newnote_filename" class="form-control" placeholder="optional..." name="slave_title">
      </div>
      	</div>
      	<div class="col-sm-4">
      		<div class="form-group">
      	<label>Category</label>
      	<input style="font-family: <?php echo $_SESSION["prefs_font"]; ?>;" type="text" class="form-control" id="newnote_category" name="slave_category" required="" value="Quick Notes" list="allcatnames">
      	<datalist id="allcatnames">
      		<?php LoadCatName_datalist(); ?>
      	</datalist>
      		</div>
      	</div>
      </div>
       <div class="form-group">
      	<label>Text</label>
      	<textarea style="font-family: <?php echo $_SESSION["prefs_font"]; ?>;" id="newnote_text" autocomplete="off" class="form-control" name="slave_text" required="" placeholder="insert text..." rows="6"></textarea>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="addnewnotebtn" data-dismiss="modal"><img class="minicon" src="images/icons/note.svg"> Save Note</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>



	<div class="modal" tabindex="-1" role="dialog" id="modal_editnote">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Note</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<input type="hidden" value="editnote" name="tag">
      	<input type="hidden" id="edit_contid" name="contentid">
      <div class="row">
      	<div class="col-sm-8">
      		<div class="form-group">
            <label>Filename: </label>
      	<input style="font-family: <?php echo $_SESSION["prefs_font"]; ?>;" type="text" autocomplete="off" id="toedit_filename" class="form-control" placeholder="optional..." name="slave_title">
      </div>
      	</div>
      	<div class="col-sm-4">
      		<div class="form-group">
            <label>Category: </label>
      	<input style="font-family: <?php echo $_SESSION["prefs_font"]; ?>;" type="text" id="toedit_category" required="" class="form-control" name="slave_category" value="Quick Notes" list="allcatnames">
      	<datalist id="allcatnames">
      		<?php LoadCatName_datalist(); ?>
      	</datalist>
      		</div>
      	</div>
      </div>
       <div class="form-group">
      	<textarea style="font-family: <?php echo $_SESSION["prefs_font"]; ?>;" autocomplete="off" required="" class="form-control" id="toedit_text" name="slave_text" placeholder="insert text..." rows="10"></textarea>
      </div>
      </div>
      <div class="modal-footer">
      	<button type="button"  name="edit_movetotrash" data-dismiss="modal" data-funcname="x" id="edit_delete" class="btn btn-danger btneditnote" style="display: block; position: fixed; left: 0; margin-left: 20px;"><img class="minicon" src="images/icons/deletenote.svg"></button>
        <button type="button" name="edit_update" data-dismiss="modal" data-funcname="edit_update" id="edit_modify"  class="btn btn-success btneditnote"><img class="minicon" src="images/icons/note.svg"> Save</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal_trash">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Trash</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="trashnotescontainer">
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	function opennote(component){
		$("#toedit_filename").val($(component).data("fname"));
		$("#toedit_category").val($(component).data("fcat"));
		$("#toedit_text").val($(component).data("ftext"));
		$("#edit_contid").val($(component).data("contid"));
	}
</script>


<script type="text/javascript">
  $("#addnewnotebtn").click(function(){
     var newnote_filename =  $("#newnote_filename").val();
    var newnote_category =  $("#newnote_category").val();
    var newnote_text =  $("#newnote_text").val();
    var edit_movetotrash = $("#edit_movetotrash")
    var edit_update = $("#edit_update")
    $.ajax({
      type: "POST",
      url: "<?php weblink(); ?>",
      data: {tag:"addnewnote",slave_title:newnote_filename,slave_category:newnote_category,slave_text:newnote_text},
      success: function(){
        $("#newnote_filename").val("");
        $("#newnote_text").val("");
      }
    });
  })

    $(".btneditnote").click(function(){
      var edit_contid = $("#edit_contid").val();
     var newnote_filename =  $("#toedit_filename").val();
    var newnote_category =  $("#toedit_category").val();
    var newnote_text =  $("#toedit_text").val();
actiontype_input = "";
    if($(this).data("funcname") == "edit_update"){
       actiontype_input ="edit";
    }else{
     actiontype_input ="delete";
    }
    $.ajax({
      type: "POST",
      url: "<?php weblink(); ?>",
      data: {tag:"editnote",contentid:edit_contid,slave_title:newnote_filename,slave_category:newnote_category,slave_text:newnote_text,actiontype:actiontype_input},
      success: function(data){
        $("#toedit_filename").val("");
        $("#toedit_text").val("");
        $("#toedit_category").val("");
      }
    });
  })
 loadtrashnotes();
    function loadtrashnotes(){
      $.ajax({
        type: "POST",
        url: "<?php weblink(); ?>",
        data: {tag:"loadtrashnotes"},
        success: function(data){
            $("#trashnotescontainer").html(data);
        }
      })
    }

   
    setInterval(function(){
      loadtrashnotes();
    },5000)
</script>