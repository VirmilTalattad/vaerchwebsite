<?php include("inc/auth.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Slavenote | Dashboard</title>
	<?php include("theme/theme.php"); ?>
	<?php include("func/updater.php"); ?>
	<?php include("func/displayer.php"); ?>
	<?php include("inc/res.php"); ?>
	<?php include("switcher/mode.php"); ?>

</head>
<body style="background-color: #D9D9D9;">
	<?php include("components/navbar.php"); ?>
	<div class="container">

		<h1>Categories</h1>
		<hr>
	<div class="row" id="livenotes">
			
	</div>
	</div>
</body>
</html>

<script type="text/javascript">
	loadnotes();
	CheckSearch();
	setInterval(function(){
		loadnotes();
		CheckSearch();
	},1000)

	function CheckSearch(){
			var sval = $("#slavesearch").val();
			if(sval.length != 0){
				$(".searchpanel").css("display","block");
			}else{
				$(".searchpanel").css("display","none");
			}
	}
	$("#slavesearch").keyup(function(){
			var searchvalue = $(this).val();
				$.ajax({
				type: "POST",
			url: "<?php echo weblink(); ?>",
			data: {tag:"search","searchval":searchvalue},
			success:function(data){
				$("#searchresultpanel").html(data);
			}
			})
	})
	function loadnotes(){
			$.ajax({
				type: "POST",
			url: "<?php echo weblink(); ?>",
			data: {tag:"liveslavenotedisplay"},
			success:function(data){
				$("#livenotes").html(data);
			}
			})
	}
	function delnote(comp){
			var noteid = $(comp).data("noteid");
			var component = $(comp).data("compid");
			$("#" + component).remove();
			$.ajax({
			type: "POST",
			url: "<?php echo weblink(); ?>",
			data: {tag:"delnote","id":noteid}
			})
	}
	function resnote(comp){
		
			var noteid = $(comp).data("noteid");
			var component = $(comp).data("compid");
			$("#" + component).remove();
			$.ajax({
			type: "POST",
			url: "<?php echo weblink(); ?>",
			data: {tag:"resnote","id":noteid}
			})
	}
	function logout_slavenote(){
			$.ajax({
			type: "POST",
			url: "<?php echo weblink(); ?>",
			data: {tag:"logout"},
			success: function(){
				location.href= "index.php";
			}
			})
	}
</script>