<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">

<style type="text/css">
/*FONT FACES*/
@font-face {
    font-family: slave_defaultfont;
    src: url(theme/default.ttf);
}
@font-face {
    font-family: inconsolata;
    src: url(theme/inconsolata.ttf);
}
/*EXPERIENCE*/

/* The switch - the box around the slider */
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

/* Hide default HTML checkbox */
.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #001490;
}

input:focus + .slider {
  box-shadow: 0 0 1px #001490;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
body{
	font-size: 25px;
	font-family: slave_defaultfont;
	background-color: #009688;
}
.form-control{
	font-size: 25px;
	box-shadow: 1px 1px 0px rgba(0,0,0,1);
	border-radius: 0px;
	border: 1px solid black;
}
.btn-default{
	font-size: 25px;
	padding: 5px;
	border-radius: 0px;
	border: 1px dashed black;
	background-color: #BFBFBF;
	box-shadow: 3px 3px 0px rgba(0,0,0,1);
	padding-left: 20px;
	padding-right: 20px;
}
.btn-danger{
	font-size: 25px;
	padding: 5px;
	border-radius: 0px;
	border: 1px dashed red;
		background-color: #BFBFBF;
	box-shadow: 3px 3px 0px rgba(0,0,0,1);
	padding-left: 20px;
	padding-right: 20px;
	color: black;
}
.btn-primary{
	font-size: 25px;
	padding: 5px;
	border-radius: 0px;
	border: 1px dashed blue;
		background-color: #BFBFBF;
	box-shadow: 3px 3px 0px rgba(0,0,0,1);
	padding-left: 20px;
	padding-right: 20px;
	color: black;
}
.btn-success{
	font-size: 25px;
	padding: 5px;
	border-radius: 0px;
	border: 1px dashed green;
		background-color: #BFBFBF;
	box-shadow: 3px 3px 0px rgba(0,0,0,1);
	padding-left: 20px;
	padding-right: 20px;
	color: black;
}
.btn-secondary{
	font-size: 25px;
	padding: 5px;
	border-radius: 0px;
	border: 1px solid black;
		background-color: #BFBFBF;
	box-shadow: 3px 3px 0px rgba(0,0,0,1);
	padding-left: 20px;
	padding-right: 20px;
	color: black;
}
small{
	background-color: black;
	color: white;
	color: #00FFE4;
	box-shadow: 3px 3px 0px rgba(255,255,255,1);
	font-size: 20px;
}
.card-header{
	padding: 5px;
	background-color: #001490;
	border-radius: 0px;
	border: 1px solid white;
	color: white;
	text-shadow: 1px 1px 0px rgba(0,0,0,1);
}
.card{
	border-radius: 0px;
	border: 1px solid black;
	box-shadow: 3px 3px 0px rgba(0,0,0,1);
	background-color: #D9D9D9;
	margin: 5px !important;
}
.searchpanel{
	top: 0;
	right: 0;
	width: 40%;
	border: 1px solid black;
	position: fixed;
	display: block;
	background-color: white;
	margin-top: 60px;
	margin-right: 10px;
	box-shadow: 3px 3px 0px rgba(0,0,0,1);
	z-index: 100;
	background-color: #D9D9D9;
		overflow: hidden;
}
.searchpanel-header{
	top:0;
	left: 0;
	right: 0;
	display: block;
	width: 100%;
	background-color: gray;
	max-height: 60px;
	overflow: hidden;
	padding: 5px;
	border: 1px solid white;
}
.searchpanel-body{
	background-color: white;
	display: block;
	margin: 5px;
		padding: 5px;
		border: 1px solid black;
		overflow: auto;
		max-height: 500px;
}
@media only screen and (max-width: 1000px) {
    .searchpanel{
	width: 100%;
	left: 0;
	bottom: 0;
	top: 25%;
	height: 60%;
}

}
@media only screen and (max-width: 580px) {
	.tohide{
		display: none;
	}
}
a{
	color:blue;
}
.card-note{
	background-color: #FFF0C6;
	font-family: <?php echo $_SESSION["prefs_font"] ?>;
}
.card-hoverable:hover{
color: white !important;
background-color: #001490;
}
.minicon{
	width: 20px;
}
.navbar a{
	color: white;
		text-shadow: 1px 1px 0px rgba(0,0,0,1);
}
.navbar{
	padding: 5px;
	  background-image: linear-gradient(-90deg, #9E9E9E, #3F51B5);
	border-radius: 0px;
	border: 1px solid white;
	color: white;
	text-shadow: 1px 1px 0px rgba(0,0,0,1);
	box-shadow: 3px 1px 0px rgba(0,0,0,1);
}
.dropdown-menu{
		border-radius: 0px;
	border: 1px solid black;
	box-shadow: 3px 3px 0px rgba(0,0,0,1);
	background-color: #D9D9D9;
}
.close{
	color: white !important;
	opacity: 1;
}
.dropdown-menu .dropdown-item{
	color: black;
	text-shadow: none;
	background-color: #D9D9D9;
	font-size: 20px;
}
.dropdown-menu .dropdown-item:hover{
	color: white;
	text-shadow: none;
	background-color: #D9D9D9;
	font-size: 20px;
	background-color: #001490;
}
hr{
border-top: 3px solid black;
}
.modal-header{
	padding: 5px;
	background-color: #001490;
	border-radius: 0px;
	border: 1px solid white;
	color: white;
	text-shadow: 1px 1px 0px rgba(0,0,0,1);
}
.modal-content{
	border-radius: 0px;
	border: 1px solid black;
	box-shadow: 3px 3px 0px rgba(0,0,0,1);
	background-color: #D9D9D9;
}
.modal-footer{

}
.card_text{
	white-space: pre-wrap;       /* Since CSS 2.1 */
    white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
    white-space: -pre-wrap;      /* Opera 4-6 */
    white-space: -o-pre-wrap;    /* Opera 7 */
    word-wrap: break-word;       /* Internet Explorer 5.5+ */
    font-size: 20px;
    overflow: hidden;
    display: block;
    margin-top: -60px;
}
</style>