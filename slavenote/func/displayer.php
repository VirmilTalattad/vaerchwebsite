<?php 
if (session_status() == PHP_SESSION_NONE) {
	session_start();
}
include("switcher/mode.php");
if($isonline){
	include("../conn/c.php");
}else{
	include("../vaerchwebsite/conn/c.php");
}

function loadtrashnotes(){
	global $c;
	$q= $c->prepare("SELECT * FROM slavenote_data WHERE owner=:email AND archive='1' GROUP BY category ORDER BY id DESC");
	$q->bindParam(":email",$_SESSION["vaerch_email"]);

		if($q->execute()){
		if($q->rowCount() != 0){
			$rescat = $q->fetchall(PDO::FETCH_ASSOC);
		for($icat = 0; $icat < count($rescat);$icat++){
			?>
			<div class="col-sm-12">
			<div class="row">
				<div class="col-sm-12">
					<h4><strong><?php echo $rescat[$icat]["category"]; ?></strong></h4>
			</div>
			
			<?php

	$q = $c->prepare("SELECT * FROM slavenote_data WHERE owner=:email AND archive='1' AND category=:currcat ORDER BY id DESC");
	$q->bindParam(":email",$_SESSION["vaerch_email"]);
	$q->bindParam(":currcat",$rescat[$icat]["category"]);
	if($q->execute()){
		$res = $q->fetchall(PDO::FETCH_ASSOC);
		for($i = 0; $i < count($res);$i++){
			?>
			<div id="idtrash_<?php echo $i; ?>" class="col-sm-12">

				<div class="card card-note" data-contid="<?php echo $res[$i]['id']; ?>">
				<div class="card-body">
					<h3><strong><?php echo $res[$i]["title"]; ?></strong></h3>
					<p>
						<?php echo substr($res[$i]["text_content"], 0,100) . "..."; ?>
					</p>
					<a onclick="resnote(this)" href="#" data-noteid="<?php echo $res[$i]["id"]; ?>" data-compid="idtrash_<?php echo $i; ?>">Restore</a>
					<a onclick="delnote(this)" href="#" data-noteid="<?php echo $res[$i]["id"]; ?>" data-compid="idtrash_<?php echo $i; ?>" class="float-right" style="color:red;">Ultra Delete</a>
				</div>
			</div>

			</div>
		
	
			<?php
		}
		echo "</div>	</div>";
	}
		}
	}else{
		?>
			<div class="col-sm-12">
				<center><p>Trash is empty.</p></center>

			</div>
		<?php
	}
	}
}
function LoadCatName_datalist(){
		global $c;
	$q= $c->prepare("SELECT * FROM slavenote_data WHERE owner=:email GROUP BY category");
	$q->bindParam(":email",$_SESSION["vaerch_email"]);
	if($q->execute()){
		$rescat = $q->fetchall(PDO::FETCH_ASSOC);
			for($icat = 0; $icat < count($rescat);$icat++){
					echo "<option>" . $rescat[$icat]["category"] ."</option>";
			}
	}

}
?>