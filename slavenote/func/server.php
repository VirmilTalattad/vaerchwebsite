<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
	include("../switcher/mode.php");
	if($isonline == true){
		include("../../conn/c.php");
	}else{
		include("../../vaerchwebsite/conn/c.php");
	}

		$tag = $_POST["tag"];
		switch ($tag) {
		case "cprefs_notefont":
			$newvalue = $_POST["newval"];
			$q = $c->prepare("UPDATE slavenote_preferences SET font=? WHERE owner=?");
			if($q->execute([$newvalue,$_SESSION["vaerch_email"]])){
				echo "true";
					$_SESSION["prefs_font"] = $newvalue;
			}else{
				echo "false";
			}
		break;
		case "cprefs_notelessdetail":
			$newvalue = $_POST["newval"];
			$q = $c->prepare("UPDATE slavenote_preferences SET lessdetail=? WHERE owner=?");
			if($q->execute([$newvalue,$_SESSION["vaerch_email"]])){
				echo "true";
					$_SESSION["prefs_isdetail"] = $newvalue;
			}else{
				echo "false";
			}
		break;
		case "getsettingsvalues":
			$q = $c->prepare("SELECT * FROM slavenote_preferences WHERE owner=? LIMIT 1");
			if($q->execute([$_SESSION["vaerch_email"]])){
				$res = $q->fetchall(PDO::FETCH_ASSOC);
				echo json_encode($res);
			}
		break;
			case 'login':
		$email = htmlentities($_POST["sl_email"]);
		$password = htmlentities($_POST["sl_pass"]);
		$password = md5($password);

		$q = $c->prepare("SELECT * FROM accounts where email=:email");
		$q->bindParam(":email",$email);
		if($q->execute()){

			if($q->rowCount() != 0){
						$res = $q->fetchall(PDO::FETCH_ASSOC);
		for($i = 0; $i < count($res);$i++){
			if($res[$i]["password"] === $password){
				if($res[$i]["is_genuine"] == 1){
					// CREATE DIRECTORY IF NOT EXISITING\
					if (!file_exists('../uploads/' . $res[$i]["email"])) {
					// FOR PROFILES
					mkdir('../uploads/' .$res[$i]["email"] . "/profiles", 0777, true);

					mkdir('../uploads/' .$res[$i]["email"] . "/post_uploads", 0777, true);
					}
					$_SESSION["vaerch_email"] = $res[$i]["email"];
					$_SESSION["vaerch_name"] = $res[$i]["username"];
					$_SESSION["vaerch_acctype"] = $res[$i]["performer_type"];
					$_SESSION["profilepicture"] =  "uploads/" .  $res[$i]["email"] . "/profiles/" . $res[$i]["profile_picture"];
					redirect_silent("dashboard.php");
				}else{
					redirect("index.php","Account needs activation!");
				}
			}else{
				redirect("index.php","Password do not match!");
			}
		}
		}else{
			redirect("index.php","Account not existing!");
		}
	}
				break;
			
		case "addnewnote":
			$title = htmlspecialchars($_POST["slave_title"]);
			$category = htmlspecialchars($_POST["slave_category"]);
			$text = htmlspecialchars($_POST["slave_text"]);

			$q = $c->prepare("INSERT INTO slavenote_data(title,category,text_content,owner) VALUES(:title,:category,:text_content,:owner)");
			$q->bindParam(":title",$title );
			$q->bindParam(":category",$category);
			$q->bindParam(":text_content",$text);
			$q->bindParam(":owner",$_SESSION["vaerch_email"]);

			if($q->execute()){
				redirect_silent("dashboard.php");
			}else{
				redirect_silent("dashboard.php");
			}
		break;

		case "editnote":



		if($_POST["actiontype"] === "delete"){
			$cont_id = $_POST["contentid"];
			$title = htmlspecialchars($_POST["slave_title"]);
			$category = htmlspecialchars($_POST["slave_category"]);
			$text = htmlspecialchars($_POST["slave_text"]);
			$q = $c->prepare("UPDATE slavenote_data SET archive='1' WHERE id=:contentid");
			$q->bindParam(":contentid",$cont_id);
			if($q->execute()){
				redirect_silent("dashboard.php");
			}else{
				redirect_silent("dashboard.php");
			}
		}else{
			$cont_id = $_POST["contentid"];
			$title = htmlspecialchars($_POST["slave_title"]);
			$category = htmlspecialchars($_POST["slave_category"]);
			$text = htmlspecialchars($_POST["slave_text"]);

			$q = $c->prepare("UPDATE slavenote_data SET title=:title,category=:category,text_content=:text_content,owner=:owner,datemodified=NOW() WHERE id=:contentid");
			$q->bindParam(":title",$title );
			$q->bindParam(":category",$category);
			$q->bindParam(":text_content",$text);
			$q->bindParam(":owner",$_SESSION["vaerch_email"]);
			$q->bindParam(":contentid",$cont_id);

			if($q->execute()){
				redirect_silent("dashboard.php");
			}else{
				redirect_silent("dashboard.php");
			}
		}
			
		break;
		case "liveslavenotedisplay":

	$q= $c->prepare("SELECT *, COUNT(category) as catcount FROM slavenote_data WHERE owner=:email AND archive='0' GROUP BY category ORDER BY id DESC");
	$q->bindParam(":email",$_SESSION["vaerch_email"]);

		if($q->execute()){
		$rescat = $q->fetchall(PDO::FETCH_ASSOC);
		if(count($rescat) != 0){
	for($icat = 0; $icat < count($rescat);$icat++){
			?>
			<div class="col-sm-6">
			<div class="row">
				<div class="col-sm-12">
					<h4><strong><?php echo $rescat[$icat]["category"]; ?></strong></h4>
			</div>
			
			<?php

	$q = $c->prepare("SELECT * FROM slavenote_data WHERE owner=:email AND category=:currcat AND archive='0' ORDER BY id DESC");
	$q->bindParam(":email",$_SESSION["vaerch_email"]);
	$q->bindParam(":currcat",$rescat[$icat]["category"]);
	if($q->execute()){
		$res = $q->fetchall(PDO::FETCH_ASSOC);
		for($i = 0; $i < count($res);$i++){
			?>
			<div class="col-sm-6">
				<div class="card card-hoverable card-note" data-toggle="modal" data-target="#modal_editnote" onclick="opennote(this)" data-fname="<?php echo $res[$i]['title']; ?>" data-fcat="<?php echo $res[$i]['category']; ?>" data-ftext="<?php echo $res[$i]['text_content']; ?>" data-contid="<?php echo $res[$i]['id']; ?>">
				<div class="card-body">
					<h3><strong><?php echo $res[$i]["title"]; ?></strong></h3>
					<span class="card_text">
						<?php

						if($res[$i]["title"] != ""){
							if ($_SESSION["prefs_isdetail"] == "0") {
							echo substr($res[$i]["text_content"], 0,100) . "...";
							}
						}else{
							echo substr($res[$i]["text_content"], 0,100) . "...";
						}
						
						?>
					</span>
					<span style="font-size: 2vh; color: rgba(0,0,0,0.5)"><strong>Created:</strong> <?php echo $res[$i]["datecreated"]; ?><br></span>
					<span style="font-size: 2vh; color: rgba(0,0,0,0.5)"><strong>Modified:</strong> <?php echo $res[$i]["datemodified"]; ?></span>
				</div>
			</div>
			</div>
		
	
			<?php
		}
		echo "</div>	</div>";
	}
		}
		}else{
			echo '<div class="col-sm-12"><center><p>(⌣̩̩́_⌣̩̩̀)<br>You have no notes here... </p><a href="#" data-toggle="modal" data-target="#modal_newnote">(+) Create One</a></center></div>';
		}
	
	}
		break;
		case "delnote":
			$q = $c->prepare("UPDATE slavenote_data SET archive='3' WHERE id=:idtomanage");
			$q->bindParam(":idtomanage",$_POST["id"]);
			if($q->execute()){
				echo "Success";
			}else{
				echo "Epic Fail.";
			}
		break;
		case "resnote":
			$q = $c->prepare("UPDATE slavenote_data SET archive='0' WHERE id=:idtomanage");
			$q->bindParam(":idtomanage",$_POST["id"]);
			if($q->execute()){
				echo "Success";
			}else{
				echo "Epic Fail.";
			}
		break;

		case "search":
			$svalue ="%" .  htmlspecialchars($_POST["searchval"]) . "%";
			$q = $c->prepare("SELECT * FROM slavenote_data WHERE title LIKE :ss1 OR category LIKE :ss2 OR text_content LIKE :ss3 OR datecreated LIKE :ss4 LIMIT 20");

			$q->bindParam(":ss1",$svalue);
			$q->bindParam(":ss2",$svalue);
			$q->bindParam(":ss3",$svalue);
			$q->bindParam(":ss4",$svalue);

			if($q->execute()){
				$res = $q->fetchall(PDO::FETCH_ASSOC);
				if(count($res) != 0){
					for($i = 0; $i < count($res);$i++){
					if($res[$i]["archive"] == '0' && $res[$i]["owner"] == $_SESSION["vaerch_email"]){
						?>

					<div class="card card-hoverable card-note" class="card card-hoverable card-note" data-toggle="modal" data-target="#modal_editnote" onclick="opennote(this)" data-fname="<?php echo $res[$i]['title']; ?>" data-fcat="<?php echo $res[$i]['category']; ?>" data-ftext="<?php echo $res[$i]['text_content']; ?>" data-contid="<?php echo $res[$i]['id']; ?>">
						<div class="card-body">
							<h1><?php echo $res[$i]["title"]; ?></h1>
							<span><?php echo substr($res[$i]["text_content"], 0,100) . "..."; ?></span>
						</div>
					</div>
					<?php
				}
				}
				}else{
					echo "Nothing found...";
				}
			}else{
				echo json_encode($q->errorInfo());
			}
		break;
		case "logout":
			session_destroy();
		break;
		case "loadtrashnotes":
				$q= $c->prepare("SELECT * FROM slavenote_data WHERE owner=:email AND archive='1' GROUP BY category ORDER BY id DESC");
	$q->bindParam(":email",$_SESSION["vaerch_email"]);

		if($q->execute()){
		if($q->rowCount() != 0){
			$rescat = $q->fetchall(PDO::FETCH_ASSOC);
		for($icat = 0; $icat < count($rescat);$icat++){
			?>
			<div class="col-sm-12">
			<div class="row">
				<div class="col-sm-12">
					<h4><strong><?php echo $rescat[$icat]["category"]; ?></strong></h4>
			</div>
			
			<?php

	$q = $c->prepare("SELECT * FROM slavenote_data WHERE owner=:email AND archive='1' AND category=:currcat ORDER BY id DESC");
	$q->bindParam(":email",$_SESSION["vaerch_email"]);
	$q->bindParam(":currcat",$rescat[$icat]["category"]);
	if($q->execute()){
		$res = $q->fetchall(PDO::FETCH_ASSOC);
		for($i = 0; $i < count($res);$i++){
			?>
			<div id="idtrash_<?php echo $i; ?>" class="col-sm-12">

				<div class="card card-note" data-contid="<?php echo $res[$i]['id']; ?>">
				<div class="card-body">
					<h3><strong><?php echo $res[$i]["title"]; ?></strong></h3>
					<p>
						<?php echo substr($res[$i]["text_content"], 0,100) . "..."; ?>
					</p>
					<a onclick="resnote(this)" href="#" data-noteid="<?php echo $res[$i]["id"]; ?>" data-compid="idtrash_<?php echo $i; ?>">Restore</a>
					<a onclick="delnote(this)" href="#" data-noteid="<?php echo $res[$i]["id"]; ?>" data-compid="idtrash_<?php echo $i; ?>" class="float-right" style="color:red;">Ultra Delete</a>
				</div>
			</div>

			</div>
		
	
			<?php
		}
		echo "</div>	</div>";
	}
		}
	}else{
		?>
			<div class="col-sm-12">
				<center><p>Trash is empty.</p></center>

			</div>
		<?php
	}
	}
		break;
		}

function redirect($pagename,$messgae){
	?>
	<script type="text/javascript">
		alert(<?php echo json_encode($messgae); ?>);
		window.location = "../" + <?php echo json_encode($pagename); ?>;
	</script>
	<?php
}

function redirect_silent($pagename){
	?>
	<script type="text/javascript">
		window.location = "../" + <?php echo json_encode($pagename); ?>;
	</script>
	<?php
}

function goback($messgae){
	?>
	<script type="text/javascript">
		alert(<?php echo json_encode($messgae); ?>);
		window.history.back();
	</script>
	<?php
}
?>