<?php
if (session_status() == PHP_SESSION_NONE) {
	session_start();
}
include("switcher/mode.php");
if($isonline){
	include("../conn/c.php");
}else{
	include("../vaerchwebsite/conn/c.php");
}
$useremail = $_SESSION["vaerch_email"];

// DEFAULT SETTINGS
$default_font = "slave_defaultfont";
$default_lessdetail = "0";

//UPDATE SETTINGS DATA
$q = $c->prepare("SELECT * FROM slavenote_preferences WHERE owner=?");
$q->execute([$useremail]);
if($q->rowCount() == 0){
	// SET DEFAULT SETTINGS
	$q = $c->prepare("INSERT INTO slavenote_preferences(owner,lessdetail,font) VALUES(?,?,?)");
	$q->execute([$useremail,$default_lessdetail,$default_font]);
	$_SESSION["prefs_font"] = $default_font;
	$_SESSION["prefs_isdetail"] = $default_lessdetail;
}else{
	// LOAD SETTINGS
	$res = $q->fetchall(PDO::FETCH_ASSOC);
	$_SESSION["prefs_font"] = $res[0]["font"];
	$_SESSION["prefs_isdetail"] = $res[0]["lessdetail"];
}
?>