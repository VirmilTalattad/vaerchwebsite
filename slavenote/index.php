	<?php include("inc/logcheck.php"); ?>
	<?php $_SESSION["vaerchappname"] = "Slavenote"; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Slavenote</title>
	<?php include("theme/theme.php"); ?>
	<?php include("switcher/mode.php"); ?>
	<?php
		if($isonline){
			include("../conn/c.php");
		}else{
			include("../vaerchwebsite/conn/c.php");
		}
	?>
	<?php include("inc/res.php"); ?>
</head>
<body>
	<div class="container">
	<br>
	<center>
		<h1 style="color: white; text-shadow: 1px 1px 0px rgba(0,0,0,1); font-size: 30px;" >(⌣̩̩́_⌣̩̩̀)<br>sLaV3<span style="color: red;">|\|</span><span style="color: yellow;">[]</span><span style="color: lime;">-|-</span><span style="color: blue;">e</span></small><br><small>Login with your VAERCH Account</small></h1>
		<div class="card" style="max-width: 300px;">
			<div class="card-header">
				Enter your credentials
			</div>
			<div class="card-body">
				<form action="<?php weblink(); ?>" method="POST">
					<input type="hidden" value="login" name="tag">
			<div class="row">
			<div class="col-sm-12">
				<div class="form-group">
					<label>Email Address</label>
				<input class="form-control" placeholder="insert text here..." type="text" name="sl_email">
				</div>
			</div>
			<div class="col-sm-12">
				<div class="form-group">
					<label>Password</label>
				<input class="form-control" placeholder="insert password here..." type="Password" name="sl_pass">
				</div>
			</div>
			<div class="col-sm-12">
				<button class="btn btn-default float-right">Login</button>
			</div>
		</div>
				</form>
			</div>
		</div>
		<a data-toggle="modal" data-target="#registationmodal"  style="background-color: white;" onclick="loadregistrationpage()" href="" >Don't have a Vaerch Account yet?<br>Register here!</a>
	</div>
	</center>
</body>
</html>
<div class="modal" tabindex="-1" role="dialog" id="registationmodal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Vaerch Account</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

        <div  class="modal-body" id="content" style="padding: 0px !important; margin: 0px !important; height:600px;">
        	
        </div>

    </div>
  </div>
</div>
<?php
	if($isonline){
		?>
		<!-- ONLINE -->
<script type="text/javascript">
	function loadregistrationpage() {
     document.getElementById("content").innerHTML='<object type="text/html" style="height:100%; width: 100%;" data="http://vaerch.com/registrationpage/index.php?vaerchappname=<?php echo $_SESSION['vaerchappname']; ?>" ></object>';
}
</script>
		<?php
	}else{
	?>
	<!-- OFFFLINE -->
<script type="text/javascript">
	function loadregistrationpage() {
     document.getElementById("content").innerHTML='<object type="text/html" style="height:100%; width: 100%;" data="http://localhost/vaerchwebsite/registrationpage/index.php?vaerchappname=<?php echo $_SESSION['vaerchappname']; ?>" ></object>';
}
</script>
	<?php
	}
?>