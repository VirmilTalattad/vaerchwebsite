<?php
include("theme/index.php");
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
<title>Vaerch Account Confirmation</title>
<?php include("inc/essentials.php") ?>
</head>
<body>
<?php include("inc/body_es.php") ?>
<div class="container">
<div class="jumbotron jumbotron-fluid">
	<div class="container">
		<center>
			<h1>Account Activation Code.</h1>
			<small>We just sent the account activation code to your email.<br>
			You won't be able to use this account if you skip this.</small>
				<form action="<?php weblink(); ?>" method="POST">
					<br>
					<div class="form-group">
						<input type="hidden" name="tag" value="confirmaccount">
						<input autocomplete="off" style="width: 30vh;" type="text" max="100" required="" placeholder="Paste the code here!" class="form-control" name="act">
					</div>
					<div class="form-group">
						<button class="btn btn-primary">Activate Account</button>
					</div>
				</form>
		</center>
	</div>
</body>
</html>