<?php
include("theme/index.php");
include("func_admin/auth.php");
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
<title>Vaerch Dot.Admin</title>
<?php include("inc/essentials.php") ?>
</head>
<body>
<?php include("admin_components/nav.php") ?>
	<div class="container">
		<br>
		<div class="row">
			<div class="col-sm-12">
					<h1>Dashboard & Site Manager</h1>
			</div>
			<div class="col-sm-9">
			<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
			  <li class="nav-item">
			    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#featuredasset" role="tab" aria-controls="featuredasset" aria-selected="true">Featured Asset</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#gamesfeatured" role="tab" aria-controls="gamesfeatured" aria-selected="false">Games</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#blognews" role="tab" aria-controls="blognews" aria-selected="false">Blog News</a>
			  </li>
			    <li class="nav-item">
			    <a class="nav-link" id="pills-coverpage-tab" data-toggle="pill" href="#coverpage" role="tab" aria-controls="coverpage" aria-selected="false">Cover Page</a>
			  </li>
			</ul>
			<div class="tab-content" id="pills-tabContent">
			  <div class="tab-pane fade show active" id="featuredasset" role="tabpanel" aria-labelledby="pills-home-tab">
			  	<!-- FTEAURED ASSET -->
			  	<table class="table table-striped table-bordered">
			  		<thead>
			  			<tr>
			  				<td>Preview</td>
			  				<td>Info</td>
			  				<td>Action</td>
			  			</tr>
			  		</thead>
			  		<tbody id="ffass">
			  			
			  		</tbody>
			  	</table>
			  </div>
			  <div class="tab-pane fade" id="gamesfeatured" role="tabpanel" aria-labelledby="pills-profile-tab">
			  	<!-- FTEAURED GAME -->
			  	<table class="table table-striped table-bordered">
			  		<thead>
			  			<tr>
			  				<td>Preview</td>
			  				<td>Info</td>
			  				<td>Action</td>
			  			</tr>
			  		</thead>
			  					  		<tbody id="ffgg">
			  			
			  		</tbody>
			  	</table>
			  </div>
			  <div class="tab-pane fade" id="blognews" role="tabpanel" aria-labelledby="pills-contact-tab">
			  	<!-- FTEAURED BLOG NEWS -->
			  	<table class="table table-striped table-bordered">
			  		<thead>
			  			<tr>
			  				<td>Preview</td>
			  				<td>Info</td>
			  				<td>Action</td>
			  			</tr>
			  		</thead>
			  					  		<tbody id="ffbb">
			  			
			  		</tbody>
			  	</table>
			  </div>
			   <div class="tab-pane fade" id="coverpage" role="tabpanel" aria-labelledby="pills-contact-tab">
			   	<!-- FTEAURED COVER PAGE -->
			   	<table class="table table-striped table-bordered">
			   		<thead>
			   			<tr>
			   				<td>Preview</td>
			  				<td>Info</td>
			  				<td>Action</td>
			   			</tr>
			   		</thead>
			   					  		<tbody id="ffcc">
			  			
			  		</tbody>
			   	</table>
			   </div>
			</div>
			</div>
			<div class="col-sm-3">
					<div class="form-group">
						<div class="card">
					<div class="card-body">
						<span>Dynamic Post</span><br>
						<span class="lessen">Create a dynamic post that will be displayed in different pages.</span><hr>
						<button data-toggle="modal" onclick="validate_compose_inputs()" data-target="#modal_changecover" class="btn btn-sm btn-primary">Compose</button>
					</div>
					</div>
					</div>
					<div class="form-group">
						<div class="card">
					<div class="card-body">
						<span>Preferences</span><br>
						<span class="lessen">Add preferences that is accessible across the website.</span><hr>
						<button data-toggle="modal" data-target="#modal_addnewinfodata" data-toggle="modal" data-target="#modal_changecover" class="btn btn-sm btn-primary">New Preferences</button>
					</div>
				</div>
					</div>
			</div>
		</div>
		<br>
		

	</div>
</div>
</body>
</html>

<?php
include("components/admin_dash_modals.php");
?>
<script type="text/javascript">
	LoadFeaturedAssets();
	LoadFeaturedGames();
LoadBlogNews();
LoadCoverPages();
	function LoadFeaturedAssets(){
		$.ajax({
			type:"POST",
			url: "func_admin/func.php",
			data: {tag: "getfeaturedasset"},
			success: function(data){
				// alert(data);
				$("#ffass").html(data);
			}
		})
	}
	function LoadFeaturedGames(){
		$.ajax({
			type:"POST",
			url: "func_admin/func.php",
			data: {tag: "getfeaturedgames"},
			success: function(data){
				// alert(data);
				$("#ffgg").html(data);
			}
		})
	}
		function LoadBlogNews(){
		$.ajax({
			type:"POST",
			url: "func_admin/func.php",
			data: {tag: "getblognews"},
			success: function(data){
				// alert(data);
				$("#ffbb").html(data);
			}
		})
	}
			function LoadCoverPages(){
		$.ajax({
			type:"POST",
			url: "func_admin/func.php",
			data: {tag: "getcovers"},
			success: function(data){
				// alert(data);
				$("#ffcc").html(data);
			}
		})
	}
</script>