<?php
include("theme/index.php");
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
<title>Vaerch Account Confirmation</title>
<?php include("inc/essentials.php") ?>
</head>
<body>
<?php include("inc/body_es.php") ?>
<div class="container">
<div class="jumbotron jumbotron-fluid">
	<div class="container">
		<center>
			<h1>Sign-in to Vaerch</h1>
			<small>What amazing thing are you going to do next?</small>

			<form action="<?php weblink(); ?>" method="POST" style="width:40vh;">
				<br>
				<input type="hidden" name="tag" value="login">
					<div class="form-group">
					<label>Email: </label>
					<input type="email" name="vl_email" class="form-control" required="">
				</div>
				<div class="form-group">
					<label>Password: </label>
					<input type="password" name="vl_pass" class="form-control" required="">
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block">Login</button>
				</div>

			</form>
		</center>
	</div>
</body>
</html>
