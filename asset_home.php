<?php
include("inc/session_page.php");
include("theme/index.php");
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
<title>Vaerch Official Website</title>
<?php include("inc/essentials.php") ?>
</head>
<body>
<?php include("inc/body_es.php") ?>

<div class="container">
	<br>
<div class="row">
	<div class="col-sm-12" style="height: 500px;">
		<div id="cont_image" style="height: 100%; width: 100%; background-size: 100%; background-repeat: no-repeat; background-position: center; background-image: url('images/super3dfirstcover.png');">
		</div>
	</div>
	<div class="col-sm-12">
		<br>
		<h4>Fresh from the storage<br>
		<small>Newly Uploaded Assets from our Performers</small></h4>
		<div class="row" id="conts_newly">
					
		</div>
	</div>
		<div class="col-sm-12">
		<br>
		<h4>Random Assets<br>
		<small>Who knows what you will discover.</small></h4>
		<div class="card">
			<div class="card-body" >
				<div class="row" id="conts_relate">
					
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-12">
		<br>
		<h4>Ordered by Categories<br>
		<small>Maybe you can find what you really need.</small></h4>
				<div class="row" id="conts_categories">
				
				</div>
	</div>

</div>

</div>
</body>
</html>



<script type="text/javascript">
	LoadOnline();
	LoadCategories();
	LoadNewAssets();
  	function LoadOnline(){
  		$.ajax({
		type: "POST",
		url : "ajax/func.php",
		data: {tag:"load_online",size:"3"},
		success: function(data){
			$("#conts_relate").html(data);
		}
	})
  	}

  	function LoadCategories(){
  		$.ajax({
		type: "POST",
		url : "ajax/func.php",
		data: {tag:"LoadCategories"},
		success: function(data){
			$("#conts_categories").html(data);
		}
	})
  	}
  	  	function LoadNewAssets(){
  		$.ajax({
		type: "POST",
		url : "ajax/func.php",
		data: {tag:"LoadNewAssets"},
		success: function(data){
			$("#conts_newly").html(data);
		}
	})
  	}
</script>