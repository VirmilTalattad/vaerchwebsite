<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K8ZQX6H"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php"><img src="images/vaerch.png" width="30" height="30" alt=""> Vaerch</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Entertainment
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="vaerchgames.php">Vaerch Mobile Games</a>
          <a class="dropdown-item" href="asset_home.php"><i class="fas fa-shopping-bag"></i> Super 3D</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="pixl_one.php">PIXL ONE Services</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          More
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="about.php">About Us</a>
          <a class="dropdown-item" href="contact.php">Contact</a>
          <a class="dropdown-item" href="http://privacypolicy.vaerch.com/">Vaerch Apps & Games Privacy Policy</a>
        </div>
      </li>
    </ul>

    <?php 
    if(isset($_SESSION["vaerch_email"])){
?>
<!--   <li class="nav-item"> -->
        <a class="nav-link" href="dashboard.php"><i class="far fa-user-circle"></i> My Account</a>
<!--       </li> -->
 <form action="<?php echo weblink(); ?>" method='POST' class="form-inline my-2 my-lg-0">
      <input type="hidden" name="tag" value="logout">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Sign-out</button>
    </form>
<?php
    }else{
    ?>
 <form class="form-inline my-2 my-lg-0">
      <button class="btn btn-outline-success my-2 my-sm-0" type="button" data-toggle="modal" data-target="#modal_vaerchacc_signin" style="margin-right: 5px;">Sign-up</button>
      <button class="btn btn-outline-primary my-2 my-sm-0" type="button" onclick="window.location.href='vlogin.php';">Sign-in</button>
    </form>
    <?php
    }
    ?>
   
  </div>
</nav>
<br>
<br>
  <div class="modal" tabindex="-1" role="dialog" id="modal_vaerchacc_signin">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form action="<?php weblink(); ?>" method="POST">
        <div class="modal-header">
          <h5 class="modal-title">Join Vaerch</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <input type="hidden" name="tag" value="createvaerchaccount">
            <div class="form-group">
              <label>What kind of a performer are you?</label>
          <select class="form-control form-control-lg" required="" name="v_accounttype">
            <option selected="" disabled="">Choose who you are...</option>
            <option>Graphic Artist</option>
            <option>Web Developer</option>
            <option>Desktop Developer</option>
            <option>Game Developer</option>
            <option>Music Composer</option>
            <option>Video Editor</option>
            <option>Technician</option>
            <option>Comedian</option>
            <option>Dancer</option>
            <option>Singer</option>
            <option>Photographer</option>
            <option>Vlogger</option>
            <option>Painter</option>
            <option>Model</option>
            <option>Inspirer</option>
            <option>Gamer</option>
          </select>
          </div>
          <div class="form-ground">
            <label>Username</label>
            <input class="form-control" type="text" name="v_username" required="" autocomplete="off">
          </div>
           <div class="form-ground">
            <label>Email</label>
            <input class="form-control" type="email" name="v_email" required="" autocomplete="off">
          </div>
          <div class="row">
            <div class="col-sm-6">
               <div class="form-ground">
            <label>Password</label>
            <input class="form-control" type="Password" name="v_pass" required="">
          </div>
            </div>
            <div class="col-sm-6">
               <div class="form-ground">
            <label>Re-enter Password</label>
            <input class="form-control" type="Password" name="v_repass" required="">
          </div>
            </div>
          </div>
           <div class="form-ground">
            <label>Sex</label>
           <select class="form-control" name="v_sex" required="">
              <option value="1">Male</option>
              <option value="0">Female</option>
           </select>
          </div>
          <div class="form-ground">
            <label>Date of Birth:</label>
          <input type="date" class="form-control" name="v_dateofbirth" required="">
          </div>
           <div class="form-ground">
            <label>Mobile Number:</label>
          <input type="text" class="form-control" name="v_contact" required="" autocomplete="off">
          </div>

                    <div class="form-group">
            <label>Terms and Conditions</label><br>
<textarea class="form-control" id="lod_termsandcond" readonly="" style="background-color: black;" rows="5">
</textarea>
<script type="text/javascript">
  $.ajax({
    type: "POST",
    url: "ajax/func.php",
    data: {tag: "lod_tac"},
    success: function(data){
      $("#lod_termsandcond").html(data);
    }})
</script>
<label><input required="" type="checkbox" name=""> I Agree to all Terms and Conditions</label>
          </div>

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Create Vaerch Account!</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        </div>
        </form>
      </div>
    </div>
  </div>
