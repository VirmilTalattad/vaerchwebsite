<link rel="shortcut icon" type="image/png" href="images/fav.png"/>
<meta name="description" content="Vaerch Technologies is a software development company. We develop fast, flexible, simple web solutions from small to large businesses in the Philippines. We also develop fun and immersive games for android devices like God Runner Ultra Mega (God Runner), Infinity Defenders 3D RPG, Quick Shoot That Bottle Arcade Game and more...">
<meta name="keywords" content="Web Applications, Systems, Android Games, RPG, Arcade, Strategy,Vaerch, Vaerch Technologies, Vaerch Systems, Vaerch Official Website, Virmil Talattad, Vaerch Online, Vaerch Games and Systems, Vaerch Web Development, Vaerch Tutorials, Vaerch Location">
<meta name="author" content="Virmil B. Talattad">
<meta name="google-site-verification" content="JzpqxJzwRkqh9aqQjP6z9-Evpd7V5Qm7afo5MOmp3mw" />
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K8ZQX6H');</script>
<!-- End Google Tag Manager -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-129696854-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-129696854-1');
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-XXXX');</script>
<!-- End Google Tag Manager -->


<!-- CUSTOM CSS -->

<style type="text/css">


	 .perfectpre {
            overflow-x: auto;
            white-space: pre-wrap;
            white-space: -moz-pre-wrap;
            white-space: -pre-wrap;
            white-space: -o-pre-wrap;
            word-wrap: break-word;
         }


	.alert{
		padding:5px;
		margin:2px;
		background-color: transparent;
	}
	.alert-primary{
		border-color: #0097e6;
		color: #00a8ff;
	}
	.lessen{
		opacity: 0.5;
	}
	.col_green{
		color: #4cd137;
	}
	.col_green_m{
		color: #4cd137;
	}
	.col_blue{
		color: #00a8ff;
	}
	.col_blue_m{
		color: #0097e6;
	}
	.col_yellow{
		color: #fbc531;
	}
	.col_yellow_m{
		color: #e1b12c;
	}
@font-face{
	  font-family: san;
  src: url(theme/sanfranc/san.ttf);
}
@font-face{
	  font-family: san_bold;
  src: url(theme/sanfranc/san_bold.ttf);
}
.perfect_fit{
	background-size: 100%; background-repeat: no-repeat; background-position: center; 
}
.breadcrumb{
	background-color: transparent;
}


	.text_shadow{
	color: white;
	text-shadow: 1px 2px 20px  black;
	}
	.card_bg{
	background-size: cover; background-position: center; background-repeat: no-repeat;
	}
	.jumbotron{
		background-color: rgba(0,0,0,0.1);
	}
	body{
		font-family: san;
	background-color: #1D1D1D;
	color: #CFCFCF;

	}
	.category_favicon_contentbutton{
		width: 50px;
		color: white;
		border: 1px solid rgba(255,255,255,0.1);
		background-color: rgba(255,255,255,0.1);
		height: 50px;
		border-radius: 1px;
	}
	.navbar{
		background-color: #2D2D2D !important;
		position: fixed;
		width: 100%;
		z-index: 100;

border-bottom: 1px solid #3A3A3A !important;
box-shadow: 0px 0px 10px rgba(0,0,0,0.2);

	}
.btn_cat {
border:none; background-color: rgba(255,255,255,0.5); border: 2px solid rgba(255,255,255,0.7);
border-radius: 20px 0px 0px 20px;
margin-right: -20px;
}
.btn_cat:hover{

	border:none; background-color: rgba(255,255,255,0); border: 2px solid rgba(255,255,255,0.7);
color: white;
	}
	.navbar .dropdown-menu{
		background-color: #2D2D2D !important;
		position: fixed;
		/*width: 100%;*/
		/*z-index: 100;*/


border: 1px solid #3A3A3A !important;
box-shadow: 0px 0px 10px rgba(0,0,0,0.2);
border-radius : 0px;
margin-top: 9px;
	}


	.dropdown-menu{
		background-color: #2D2D2D !important;
		position: fixed;
		/*width: 100%;*/
		/*z-index: 100;*/


border: 1px solid #3A3A3A !important;
box-shadow: 0px 0px 10px rgba(0,0,0,0.2);
border-radius : 0px;
margin-top: 9px;
	}

	.dropdown-menu .dropdown-item{
		color: white;
	}



.dropdown-menu a{
opacity: 0.4;
	}
	.dropdown-menu a:hover{
		opacity: 0.9;
background-color: transparent;
	}
	.navbar a{
		color: #E4E4E4 !important;

	}
	h1, h2, h3, h4, h5, h6{
		font-family: san_bold;
	}
	.jumbotron-max{
		margin: 0px !important;
		overflow: hidden;
	}
	.card{
		background-color: #141414;
		border-radius: 0px;
		border: 1px solid #2B2B2B;
		box-shadow: 0px 0px 10px rgba(0,0,0,0.2);
	}
	.modal{
		background-color: rgba(255,255,255,0.2);
	}
	.modal-header{
		background-color: rgba(0,0,0,0.1);
		border-radius: 0px;
		border: none;
		border-bottom: 1px solid #454545 !important;
	}
	.close{
		opacity: 1;
		color: white !important;
	}
	.modal-body{
		background-color: #212121 !important;
	}
	.modal-footer{
		background-color: rgba(0,0,0,0.1);
		border-radius: 0px;
		border: none;
		border-top: 1px solid #454545 !important;

	}
	h5 small{
		opacity: 0.5;
	}
	.frontimg{
		height: 500px;
		background-position: center;
		background-size: cover;
			padding: 0px;
	}
	.frontimg .fader{
		background-image: linear-gradient(to bottom, transparent, #1D1D1D) !important;
		height: 100%;
		width: 100%;
		color: white;
		/*background-color: black;*/
	}
	.grid-list{
		width: 100%;
	}
	.grid-list a{
		display: inline-block;
		width: 150px;
		text-align: center;
	}
	.modal-content{
		background-color: #1A1A1A;
		border-radius: 0px;
		border: 1px solid #2B2B2B;
		box-shadow: 0px 0px 20px rgba(0,0,0,0.2);
	}
	label{
		color: #737373;
	}
	.btn{
		background-image: linear-gradient(to bottom, #5E5E5E, #2F2F2F) !important;
		border-color: #454545 !important;
		border-radius: 0px;
		box-shadow: 0px 1px 2px rgba(0,0,0,0.2);
		color: rgba(255,255,255,2) !important
	}
	.btn:hover{
		color: rgba(255,255,255,0.5) !important;
	}
	.form-control{
		background-color: #2D2D2D;
		border-color: #3A3A3A !important;
		border-radius: 0px;
		box-shadow: 0px 1px 2px rgba(0,0,0,0.2);
		color: #CACACA !important
	}
	.form-control:focus{
		background-color: #2D2D2D;
		border-color: #3A3A3A !important;
		border-radius: 0px;
		box-shadow: 0px 1px 2px rgba(0,0,0,0.2);
		color: #CACACA !important
	}
	.btn_link_button{
		width: 100%;
		color: white;
		border: 1px solid rgba(255,255,255,0.1);
		background-color: rgba(255,255,255,0.1);
		height: 300px;
		border-radius: 1px;
		/*background-color: transparent;*/

	}
	.btn_link_button:hover{
		background-color: rgba(255,255,255,0.2);
		border: 1px solid rgba(255,255,255,0.1);
		text-decoration: none;
		color: white;
	}
</style>
