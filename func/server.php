<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
$tag = $_POST["tag"];
include("../conn/c.php");
switch($tag){
	case "changmerchlogo":
		$newlogo = $_FILES["newlogo"];
		$email = $_SESSION["vaerch_email"];

		$gid = $c->prepare("SELECT * FROM accounts WHERE email=?");
		if($gid->execute([$email])){
			$xres = $gid->fetchall(PDO::FETCH_ASSOC);
			$acc_ID = $xres[0]["id"];

// FILE UPLOAD proccess

$is_cancel = false;
$new_icon_name ="";
			$moveDir = "../uploads/" . $_SESSION["vaerch_email"] . "/super3d/";
if ($newlogo["name"] != "") {
// FUNCTION
$file_base = basename($newlogo["name"]);
$file_name = $newlogo["name"];
$file_extentsion = strtolower(pathinfo($file_base,PATHINFO_EXTENSION));
$temp = explode(".", $newlogo["name"]);
$newfilename = "cov_" .  round(microtime(true)) . '.' . end($temp);
// VALIDATION
if($file_extentsion == "png" || $file_extentsion == "gif" || $file_extentsion == "jpeg" || $file_extentsion == "jpg" || $file_extentsion == "bmp"){
	move_uploaded_file($newlogo["tmp_name"], $moveDir . $newfilename);
	$new_icon_name = $newfilename;
}else{
	$is_cancel = true;
}
}


if($is_cancel == false){
			$q = "";
			if($_POST["uptype"] == "merch_icon"){
				$q = $c->prepare("UPDATE super3d_accounts SET merch_icon=? WHERE account_id=?");
			}else if($_POST["uptype"] == "merch_cover"){
				$q = $c->prepare("UPDATE super3d_accounts SET merch_cover=? WHERE account_id=?");
			}
			
			if($q->execute([$new_icon_name,$acc_ID])){
				redirect("dashboard_mysuper3d.php","Icon has changed.");
			}else{
				redirect("dashboard_mysuper3d.php","Failed to change icon. Try again later");
			}
		}else{
			redirect("dashboard_mysuper3d.php","Failed to change icon. Try a different image file.");
		}
		}
	break;
	case "resgister_acc_new":
		$m_name = $_POST["m_name"];
		$m_desc = $_POST["m_desc"];
		$m_emailsupport = $_POST["m_emailsupport"];
		$m_icon = $_FILES["m_icon"];
		$m_cover = $_FILES["m_cover"];


		$q = $c->prepare("SELECT * FROM accounts WHERE email=? AND is_genuine='1' LIMIT 1");
		if($q->execute([$_SESSION["vaerch_email"]])){
			if($q->rowCount() == 1){
				$res = $q->fetchall(PDO::FETCH_ASSOC);
			$account_id = $res[0]["id"];
			$q = $c->prepare("INSERT INTO super3d_accounts(account_id,merch_name,merch_description,merch_icon,merch_cover,merch_supportemail)
			VALUES(?,?,?,?,?,?)");

$newiconname ="";
			$moveDir = "../uploads/" . $_SESSION["vaerch_email"] . "/super3d/";
if ($m_icon["name"] != "") {
// FUNCTION
$file_base = basename($m_icon["name"]);
$file_name = $m_icon["name"];
$file_extentsion = strtolower(pathinfo($file_base,PATHINFO_EXTENSION));
$temp = explode(".", $m_icon["name"]);
$newfilename = round(microtime(true)) . '.' . end($temp);
// VALIDATION
if($file_extentsion == "png" || $file_extentsion == "gif" || $file_extentsion == "jpeg" || $file_extentsion == "jpg" || $file_extentsion == "bmp"){
	move_uploaded_file($m_icon["tmp_name"], $moveDir . $newfilename);
	$newiconname = $newfilename;
}else{
	$is_cancel = true;
}
}


$new_covername ="";
			$moveDir = "../uploads/" . $_SESSION["vaerch_email"] . "/super3d/";
if ($m_cover["name"] != "") {
// FUNCTION
$file_base = basename($m_cover["name"]);
$file_name = $m_cover["name"];
$file_extentsion = strtolower(pathinfo($file_base,PATHINFO_EXTENSION));
$temp = explode(".", $m_cover["name"]);
$newfilename = "cov_" .  round(microtime(true)) . '.' . end($temp);
// VALIDATION
if($file_extentsion == "png" || $file_extentsion == "gif" || $file_extentsion == "jpeg" || $file_extentsion == "jpg" || $file_extentsion == "bmp"){
	move_uploaded_file($m_cover["tmp_name"], $moveDir . $newfilename);
	$new_covername = $newfilename;
}else{
	$is_cancel = true;
}
}


			if($q->execute([$account_id ,$m_name,$m_desc,$newiconname,$new_covername,$m_emailsupport])){
				redirect("dashboard_mysuper3d.php","Registration complete!");
			}else{
				redirect("dashboard_mysuper3d.php","Registration to Super 3D has failed.");
			}
			}else{
				redirect("dashboard_mysuper3d.php","Account is not genuine.");
			}
		}else{
			redirect("dashboard_mysuper3d.php","Failed to execute operation.");
		}
	break;
    case 'get_sub_count':
       $q = $c->prepare("SELECT * FROM subscriptions");
       $q->execute();
       echo $q->rowCount();
    break;
	case 'add_new_sub':

		$email = htmlentities(strtolower($_POST["sub_email"]));
		// $latest_date = date("Y-m-d H:i:sa");
		$q = $c->prepare("SELECT * FROM subscriptions WHERE email=:ee");
		$q->bindParam(":ee",$email);
		if($q->execute()){
			if($q->rowCount() == 0){
				$q = $c->prepare("INSERT INTO subscriptions(email,timestamp) VALUES(:ee,NOW())");
				$q->bindParam(":ee",$email);
				// $q->bindParam(":tt",$latest_date);
				if($q->execute()){
					redirect("vaerch_subs.php","Email subscribed!");
				}else{
					redirect_silent("Email registered!");
				}
			}else{
				redirect("vaerch_subs.php","Email already registered!");
			}
		}
		
		break;
	case "login":
		$email = htmlentities($_POST["vl_email"]);
		$password = htmlentities($_POST["vl_pass"]);
		$password = md5($password);

		$q = $c->prepare("SELECT * FROM accounts where email=:email");
		$q->bindParam(":email",$email);
		if($q->execute()){

			if($q->rowCount() != 0){
						$res = $q->fetchall(PDO::FETCH_ASSOC);
		for($i = 0; $i < count($res);$i++){
			if($res[$i]["password"] === $password){
				if($res[$i]["is_genuine"] == 1){
					// CREATE DIRECTORY IF NOT EXISITING\

					if (!file_exists('../uploads/' . $res[$i]["email"])) {
					// FOR PROFILES
					mkdir('../uploads/' .$res[$i]["email"] . "/profiles", 0777, true);
					mkdir('../uploads/' .$res[$i]["email"] . "/super3d", 0777, true);
					mkdir('../uploads/' .$res[$i]["email"] . "/post_uploads", 0777, true);

					}
					$_SESSION["vaerch_email"] = $res[$i]["email"];
					$_SESSION["vaerch_name"] = $res[$i]["username"];
					$_SESSION["vaerch_acctype"] = $res[$i]["performer_type"];
					$_SESSION["profilepicture"] =  "uploads/" .  $res[$i]["email"] . "/profiles/" . $res[$i]["profile_picture"];
					redirect_silent("dashboard.php");
				}else{
					redirect("accountconfirmation.php","Account needs activation!");
				}
			}else{
				redirect("vlogin.php","Password do not match!");
			}
		}
		}else{
			redirect("vlogin.php","Account not existing!");
		}
	}
	break;
	case "createvaerchaccount":
		
		if(isset($_POST["creationplace"])){
		$creationplace = $_POST["creationplace"];
		if($creationplace == "local"){
			// LOCAL IS THE CREATION PLACE
			$_SESSION["creationplace"] = "local";
		}else{
			// APP IS THE CREATION PLACE
			$_SESSION["creationplace"] = $creationplace;
		}
		}else{
			$_SESSION["creationplace"] = "local";
		}
		
		// CLEAN STRING
		$v_accounttype = htmlentities($_POST["v_accounttype"]);
		$v_username = htmlentities($_POST["v_username"]);
		$v_email =strtolower(htmlentities($_POST["v_email"]));
		$v_pass = htmlentities($_POST["v_pass"]);
		$v_repass = htmlentities($_POST["v_repass"]);
		$v_contact = htmlentities($_POST["v_contact"]);
		$v_sex = htmlentities($_POST["v_sex"]);
		$v_dateofbirth = htmlentities($_POST["v_dateofbirth"]);

		if($v_pass == $v_repass){
			// CHECK IF ACCOUNT ALREADY EXIST
		$q = $c->prepare("SELECT * FROM accounts WHERE email=:email");
		$q->bindParam(":email",$v_email);
		if($q->execute() && $q->rowCount() == 0){
			$q = $c->prepare("INSERT INTO accounts(username,email,password,contact,birthdate,gender,activation,performer_type,creationplace)
			VALUES(:username,:email,:password,:contact,:birthdate,:gender,:activation,:ptype,:crepalce)");
		// ACTIVATION CODE GENERATION
		$code = "Vaerch-(" . rand(0,1000) . ")-" . uniqid();
		// ENCRYPTINNG PASSWORD
		$v_pass = md5($v_pass);

		$q->bindParam(":username",$v_username);
		$q->bindParam(":email",$v_email);
		$q->bindParam(":password",$v_pass);
		$q->bindParam(":contact",$v_contact);
		$q->bindParam(":birthdate",$v_dateofbirth);
		$q->bindParam(":gender",$v_sex);
		$q->bindParam(":activation",$code);
		$q->bindParam(":ptype",$v_accounttype);
		$q->bindParam(":crepalce",$_SESSION["creationplace"]);
		$to = $v_email;
		$subject = "Vaerch Account - Activation Code";
		$body = "Copy this : " . $code;
		include("emailsender.php");

		if($q->execute()){
			redirect("accountconfirmation.php","A confirmation email has been sent to your email (" . $v_email . ")");
		}else{
			if($_SESSION["creationplace"] == "local"){
			redirect("index.php","Failed to create account. Try again.");
		}else{
			goback("Failed to create account. Try again.");
		}
		}
		}else{
			if($_SESSION["creationplace"] == "local"){
			redirect("index.php","Account already exist. Failed to create account.");
		}else{
			goback("index.php","Account already exist. Failed to create account.");
		}
		}
		}else{
			if($_SESSION["creationplace"] == "local"){
			redirect("index.php","Password do not match. Failed to create account. try again");
		}else{
			goback("Password do not match. Failed to create account. try again");
		}
		}
	break;
	case "confirmaccount":
	$act = $_POST["act"];
	$q = $c->prepare("SELECT * FROM accounts WHERE activation=:act AND is_genuine='0' LIMIT 1");
	$q->bindParam(":act",$act);
	if($q->execute()){
		if( $q->rowCount() != 0){
			$q = $c->prepare("UPDATE accounts SET is_genuine='1' WHERE activation=:act");
			$q->bindParam(":act",$act);
			if($q->execute()){
				if($_SESSION["creationplace"] == "local"){
					redirect("vlogin.php","Account Successfully Validated!");
				}else{
					redirect("registrationpage/successpage.php","Account Successfully Validated!");
				}
			}
		}else{
			goback("Invalid Activation Code, try again.");
		}
	}else{
		goback("Failed to activate Vaerch Account. Try again later...");
	}
	break;
	case "acc_control_changeprofilepicture":

		$target_dir = "../uploads/";
		$target_file = $target_dir . basename($_FILES["profilepic"]["name"]);
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		// Check if image file is a actual image or fake image
		if(isset($_POST["submit"])) {
		    $check = getimagesize($_FILES["profilepic"]["tmp_name"]);
		    if($check !== false) {
		        $uploadOk = 1;
		    } else {

				redirect("dashboard.php","File is not an image. Try again...");
		        $uploadOk = 0;
		    }
		}
		// Check if file already exists
		if (file_exists($target_file)) {
		    $uploadOk = 0;
		}
		// Check file size
		if ($_FILES["profilepic"]["size"] > 9000000) {
		    redirect("dashboard.php","Sorry, your file is too large. Try again...");
		    $uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {

		redirect("dashboard.php","Sorry, only JPG, JPEG, PNG & GIF files are allowed.. Try again...");
		    $uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    redirect("dashboard.php","Failed to proccess image. Try again...");

		// if everything is ok, try to upload file
		} else {

		$temp = explode(".", $_FILES["profilepic"]["name"]);
		$newfilename = round(microtime(true)) . '.' . end($temp);

		    if (move_uploaded_file($_FILES["profilepic"]["tmp_name"], $target_dir . $_SESSION["vaerch_email"] . "/profiles/" . $newfilename)) {
		        $q = $c->prepare("UPDATE accounts SET profile_picture=:profilex WHERE email=:emailx");
		        $q->bindParam(":profilex", $newfilename);
		        $q->bindParam(":emailx",$_POST["xxemail"]);
		        if($q->execute()){
		        	$_SESSION["profilepicture"] =  "uploads/" .  $_SESSION["vaerch_email"]  . "/profiles/" .  $newfilename;
		        	redirect_silent("dashboard.php");
		        }else{
		        	redirect("dashboard.php","Failed to proccess image. Try again...");
		        }
		    } else {
		        redirect("dashboard.php","Failed to proccess image. Try again...");
		    }
		}

	break;
	case "add_post_account":

	break;
	case "add_post":
		$description = htmlentities($_POST["boastdescription"]);
		$textstyle = $_POST["styletext"];
		$target_dir = "../uploads/";
		$target_file = $target_dir . basename($_FILES["postbgimage"]["name"]);
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		// Check if image file is a actual image or fake image
		if(isset($_POST["submit"])) {
		    $check = getimagesize($_FILES["postbgimage"]["tmp_name"]);
		    if($check !== false) {
		        $uploadOk = 1;
		    } else {

				redirect("dashboard.php","File is not an image. Try again...");
		        $uploadOk = 0;
		    }
		}
		// Check if file already exists
		if (file_exists($target_file)) {
		    $uploadOk = 0;
		}
		// Check file size
		if ($_FILES["postbgimage"]["size"] > 9000000) {
		    redirect("dashboard.php","Sorry, your file is too large. Try again...");
		    $uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {

		redirect("dashboard.php","Sorry, only JPG, JPEG, PNG & GIF files are allowed.. Try again...");
		    $uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    redirect("dashboard.php","Failed to proccess image. Try again...");

		// if everything is ok, try to upload file
		} else {

		$temp = explode(".", $_FILES["postbgimage"]["name"]);
		$newfilename = round(microtime(true)) . '.' . end($temp);

		    if (move_uploaded_file($_FILES["postbgimage"]["tmp_name"], $target_dir . $_SESSION["vaerch_email"] . "/post_uploads/" . $newfilename)) {
		        
		$q = $c->prepare("INSERT INTO boasts(description,owner,text_style,picture) VALUES(:x_desc,:x_owner,:styledtext,:bgimage)");
			$q->bindParam(":x_desc",$description);
			$q->bindParam(":x_owner",$_SESSION["vaerch_email"]);
			$q->bindParam(":styledtext",$textstyle);
			$q->bindParam(":bgimage",$newfilename);
			if($q->execute()){
				redirect_silent("dashboard.php");
			}else{
				redirect("dashboard.php","Failed to proccess post");
			}
		    } else {
		        redirect("dashboard.php","Failed to proccess image. Try again...");
		    }
		}



	break;
	case "logout":
		session_destroy();
		redirect_silent("vlogin.php");
	break;

}

function redirect($pagename,$messgae){
	?>
	<script type="text/javascript">
		alert(<?php echo json_encode($messgae); ?>);
		window.location = "../" + <?php echo json_encode($pagename); ?>;
	</script>
	<?php
}

function redirect_silent($pagename){
	?>
	<script type="text/javascript">
		window.location = "../" + <?php echo json_encode($pagename); ?>;
	</script>
	<?php
}

function goback($messgae){
	?>
	<script type="text/javascript">
		alert(<?php echo json_encode($messgae); ?>);
		window.history.back();
	</script>
	<?php
}
?>