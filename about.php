<?php
include("inc/session_page.php");
include("theme/index.php");
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
<title>About Vaerch</title>
<style type="text/css">
	.profilepicture{
		height: 200px;
		width: 200px;
		background-color: gray;
		margin: auto;
	}
</style>
<?php include("inc/essentials.php") ?>
</head>
<body>
<?php include("inc/body_es.php") ?>
<div class="container">
<div class="jumbotron jumbotron-fluid">
	<div class="container">
		<h1>
		About Vaerch
		</h1>
	</div>
</div>
<h4>Meet our team!</h4>
<div class="row">
	<div class="col-sm-4">
		<div class="card">
			<div class="card-body">
				<div class="profilepicture">
					
				</div>
			</div>
			<div class="card-footer">
				<center><h4>Edward</h4>
				<p>System Analyst</p></center>
			</div>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="card">
			<div class="card-body">
				<div class="profilepicture">
					
				</div>
			</div>
			<div class="card-footer">
				<center><h4>Virmil</h4>
				<p>Programmer</p></center>
			</div>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="card">
			<div class="card-body">
				<div class="profilepicture">
					
				</div>
			</div>
			<div class="card-footer">
				<center><h4>Renz Steven</h4>
				<p>Idk yet</p></center>
			</div>
		</div>
	</div>
</div>
</div>
</body>
</html>