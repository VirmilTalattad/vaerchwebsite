<?php
include("inc/session_page.php");
include("theme/index.php");
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
<title>Vaerch Contacts</title>
<?php include("inc/essentials.php") ?>
</head>
<body>
<?php include("inc/body_es.php") ?>
<div class="container">
<div class="jumbotron jumbotron-fluid">
	<div class="container">
		<h1>
		Contact us
		</h1>
	</div>
	<ul>
		<li><i class="fas fa-phone"></i> Mobile Number: 09273300519</li>
		<li><i class="fas fa-envelope"></i> Email: Vaerch.Tech@Gmail.com</li>
		<li><i class="fab fa-facebook"></i> Facebook Page: <a href="https://web.facebook.com/VaerchGames/">https://web.facebook.com/VaerchGames/</a></li>
	</ul>
</div>
</div>
</body>
</html>