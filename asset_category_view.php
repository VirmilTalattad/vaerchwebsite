<?php
include("inc/session_page.php");
include("theme/index.php");
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
<title>Vaerch Official Website</title>
<?php include("inc/essentials.php") ?>
</head>
<body>
<?php include("inc/body_es.php") ?>

<div class="container">
	<br>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="asset_home.php"><i class="fas fa-home"></i> Super 3D Home</a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo $_GET["catname"]; ?></li>
  </ol>
</nav>

<div class="row">
	<div class="col-sm-12" style="height: 300px;">
			<center>
				<h4>CATEGORY</h4>
				<div style="border: 2px solid white; padding: 20px; display: block; border-radius: 50px;">
					<h1 ><i class="fas fa-arrow-circle-right"></i> <?php echo $_GET["catname"]; ?></h1>
				</div>
				
			</center>
	</div>
	<div class="col-sm-12">
		<h4>All related to this category<br>
	</div>
		<div class="col-sm-12">
		<div id="category_contents" class="row">
			
		</div>
	</div>
		<div class="col-sm-12">
		<br>
		<h4>See from other Category<br>
		<small>Maybe you can find what you really need.</small></h4>
				<div class="row" id="conts_categories">
				
				</div>
	</div>
</div>

</div>
</body>
</html>

<script type="text/javascript">
	load_all_single_category();
  	function load_all_single_category(){
  		$.ajax({
		type: "POST",
		url : "ajax/func.php",
		data: {tag:"load_all_single_category",cat_name:<?php echo json_encode($_GET["catname"]); ?>},
		success: function(data){
			$("#category_contents").html(data);
		}
	})
  	}
  		LoadCategories();
  	function LoadCategories(){
  		$.ajax({
		type: "POST",
		url : "ajax/func.php",
		data: {tag:"LoadCategories"},
		success: function(data){
			$("#conts_categories").html(data);
		}
	})
  	}
</script>