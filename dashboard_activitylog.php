<?php
include("inc/auth.php");
include("theme/index.php");
include("func/displayer.php");
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
<title>Perfomer - Activity Log</title>
<?php include("inc/essentials.php") ?>
</head>
<body>
<?php include("inc/body_es.php") ?>
<br>
<style type="text/css">
  .fonttocustomize{
    transition: 0.2s all;
  }
  .text_black{
    color: black;
  }
  .text_white{
    color: white;
  }
  .text_backgroundworthy{
    color: white;
    text-shadow: 0px 2px 30px rgba(0,0,0,0.7);
  }
</style>
<div class="container">
<div class="row">
  <div class="col-sm-2">
    <a href="#" title="Change Profile Picture..." data-toggle="modal" data-target="#modal_changepropic">
      <img src="<?php echo $_SESSION['profilepicture']; ?>" style="width: 60px; height: 60px; background-color: rgba(0,0,0,0.5); border-radius: 4px;">
    </a>
    <h6><?php echo $_SESSION["vaerch_name"]; ?><br><strong><?php echo $_SESSION["vaerch_acctype"]; ?></strong><br><small><?php echo $_SESSION["vaerch_email"]; ?></small></h6>
    <div class="card">
<?php include("inc/accountnav.php"); ?>
    </div>
  </div>
    <div class="col-sm-10">
      <h1>Activity Log</h1>
    </div>

</div>
</div>
</body>
</html>

<form action="<?php echo weblink(); ?>" method="POST" enctype="multipart/form-data">
  <div class="modal" tabindex="-1" role="dialog" id="modal_changepropic">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Change Profile Picture</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="tag" value="acc_control_changeprofilepicture">
        <input type="hidden" value="<?php echo $_SESSION['vaerch_email']; ?>" name="xxemail">

        <div class="form-group">
          <label>Choose image file</label>
          <input type="file" name="profilepic">
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Begin Upload</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>



<div class="modal" tabindex="-1" role="dialog" id="modal_fontpicker">
  <div class="modal-dialog modal-lg" role="document" >
    <div class="modal-content">
      <div class="modal-body">
        <div class="form-group" style="height: 60px; background-color: gray; padding:10px; border-radius: 4px;">
          <p class="fonttocustomize" >Hello, World!</p>
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-sm-3">
              <label>Font</label>
              <select class="form-control" id="cus_fontstyle">
                <option value="robotofont">Roboto</option>
                <option value="ranga">Ranga</option>
                <option value="bebas">Bebeas Neue</option>
                <option value="playfair">Playfair</option>
              </select>
            </div>
            <div class="col-sm-3">
              <label>Alignment</label>
              <select class="form-control" id="cus_textalign">
                <option value="left">Left</option>
                <option value="center">Center</option>
                <option value="right">Right</option>
              </select>
            </div>
            <div class="col-sm-3">
              <label>Color</label>
              <select class="form-control" id="cus_color">
                <option value="black">Black</option>
                <option value="white">White</option>
              </select>
            </div>
            <div class="col-sm-3">
              <label>Size</label>
              <input type="range" id="fontsizerange" min="16" max="30" class="form-control" value="12" name="">
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>


<script type="text/javascript">
  setInterval(function(){
  $(".fonttocustomize").css("font-size",$("#fontsizerange").val() + "px");
  $(".fonttocustomize").css("font-family",$("#cus_fontstyle").val());
  $(".fonttocustomize").css("text-align",$("#cus_textalign").val());
  $(".fonttocustomize").css("color",$("#cus_color").val());
  var stylecode = '[{"font-size":"' + $("#fontsizerange").val() + 'px","font-family":"' + $("#cus_fontstyle").val() +'","text-align":"' + $("#cus_textalign").val() +'","color":"' + $("#cus_color").val() + '"}]';
  $("#text_stylecode").val(stylecode);


  },500)

  function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#postpanel').css('background-image', "url(" +  e.target.result + ")");
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#id_bgimageupload").change(function() {
  readURL(this);
});
</script>