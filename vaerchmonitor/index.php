<!DOCTYPE html>
<html>
<head>
	<title>Vaerch Monitor</title>
	<?php include("theme/theme.php");
		  include("inc/res.php");
	 ?>
</head>
<body>
<div class="container">
	<div class="jumbotron">
		<h1>Vaerch Monitoring</h1>
	</div>
	<div class="row"> 
		<div class="col-sm-3">
			<div class="card">
			<div class="card-header">
				All Registered Accounts
			</div>
			<div class="card-body">
				<p><strong>All</strong>: <span id="disp_allus"></span></p>
				<p><strong>Verified</strong>: <span id="disp_userver"></span></p>
				<p><strong>Unverified</strong>: <span id="disp_userunver"></span></p>
				<p><strong>Today</strong>: <span id="disp_regtoday"></span></p>
			</div>
		</div>
		</div>
		<div class="col-sm-3">
			<div class="card">
			<div class="card-header">
				Verified Performers
			</div>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Type</th>
							<th>Count</th>
						</tr>
					</thead>
					<tbody id="performerssum">
						
					</tbody>
				</table>
		</div>
		</div>
	</div>
</div>
</body>
</html>
<script type="text/javascript">
	setInterval(function(){
		alluserscount();
		alluserscount_verified();
		alluserscount_notverified();
		dispallperformercountbycat();
		displayregisteredtoday();
	},1000)
	
	function alluserscount(){
		$.ajax({
			type: "POST",
			url: "<?php weblink(); ?>",
			data:{tag:"disp_allusers"},
			success: function(data){
				$("#disp_allus").html(data);
			}
		})
	}
		function alluserscount_verified(){
		$.ajax({
			type: "POST",
			url: "<?php weblink(); ?>",
			data:{tag:"disp_allusers_verified"},
			success: function(data){
				$("#disp_userver").html(data);
			}
		})
	}
		function alluserscount_notverified(){
		$.ajax({
			type: "POST",
			url: "<?php weblink(); ?>",
			data:{tag:"disp_allusers_unverified"},
			success: function(data){
				$("#disp_userunver").html(data);
			}
		})
	}
		function displayregisteredtoday(){
		$.ajax({
			type: "POST",
			url: "<?php weblink(); ?>",
			data:{tag:"disp_registered_today"},
			success: function(data){
				$("#disp_regtoday").html(data);
			}
		})
	}
		function dispallperformercountbycat(){
		$.ajax({
			type: "POST",
			url: "<?php weblink(); ?>",
			data:{tag:"disp_performerscount_verified"},
			success: function(data){
				$("#performerssum").html(data);
			}
		})
	}
</script>