<?php include("inc/auth.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Rice Mill Inventory - User Management</title>
	<?php
	include("theme/index.php");
	include("inc/central.php");
	include("func/displayer.php");
	?>
</head>
<body>

<div class="container">
	<?php include("comp/index.php") ;?>
	<div class='jumbotron jumbotron-fluid'>
	<div class='container'>
		<h4>User Management</h4>
	</div>
	</div>


		<?php 

if($_SESSION["usertype"] == 0){
?>
<div class="row">
	<div class="col-sm-3">
            <div class="alert alert-info" role="alert">
  <i class="fas fa-info-circle"></i> <strong>User Manager</strong> Lets you create secondary user account to help you manage transactions and print data.
</div>
		<div class="list-group">
		  <a href="#" class="list-group-item list-group-item-action" data-toggle="modal" data-target="#NewUserAccountModal"><i class="fas fa-plus-circle"></i> New User Account</a>
		</div>
	</div>
	<div class="col-sm-9">
<table id="usertable" class="table table-striped table-bordered">
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Type</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <tbody>
<?php ShowUsers(); ?>
  </tbody>
</table>
	</div>
</div>
<?php
}else{
	echo "<center>Access not available for this current user type.</center>";
}
		?>
</div>
</body>
</html>
<script type="text/javascript">
  $("#usertable").DataTable({
            rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true
  });
</script>
<form action='<?php weblink(); ?>' method='POST'>
<div class="modal" tabindex="-1" role="dialog" id="modal_deleteuser">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <!-- ESSENTIALS - START -->
 		<input type="hidden" name="tag" value="DeleteUserAccount">
    <input type="hidden" name="user" value="<?php useremail(); ?>">
    <input type="hidden" id="userid_todelte" name="id">
              <!-- ESSENTIALS - END -->
           <!-- FORM VISIBLE PART - START -->
 		<p>Do you want to delete this user account?</p>
     <!-- FORM VISIBLE PART - END -->
      </div>
      <div class="modal-footer">
        	<button class='btn btn-danger' type='submit'>Yes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>

<script type="text/javascript">
	$("#usertable").on("click",".deluseracc", function(){
		$("#userid_todelte").val($(this).data("id"));
	})
</script>
<form action='<?php weblink(); ?>' method='POST'>
<div class="modal" tabindex="-1" role="dialog" id="NewUserAccountModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">New User Account</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <!-- ESSENTIALS - START -->
 		<input type="hidden" name="tag" value="CreateUserAccount">
    <input type="hidden" name="user" value="<?php useremail(); ?>">
              <!-- ESSENTIALS - END -->
           <!-- FORM VISIBLE PART - START -->
 		<div class='form-group'>
 			<label>Username:</label>
 			<input placeholder="Example: Virmil Talattad" autocomplete="off" required="" type='text' class='form-control' name="username" maxlength="100">
 		</div>
 		<div class='form-group'>
 			<label>Email Address:</label>
 			<input placeholder="Example: Sample@sample.com" autocomplete="off" required="" type='email' class='form-control' name="email" maxlength="100">
 		</div>
 		<div class="row">
 			<div class="col-sm-6">
 				 		 <div class='form-group'>
 			<label>Password:</label>
 			<input placeholder="type password here..." id="pass1" autocomplete="off" required="" type='password' class='form-control' name="password" maxlength="60">
 		</div>
 			</div>
 			<div class="col-sm-6">
 				 		 		<div class='form-group'>
 			<label>Re-enter Password:</label>
 			<input placeholder="re-enter password here..." id="pass2" autocomplete="off" required="" type='password' class='form-control' name="repassword" maxlength="60">
 		</div>
 			</div>
 		</div>


     <!-- FORM VISIBLE PART - END -->
      </div>
      <div class="modal-footer">
        	<button class='btn btn-primary' id="creatuseraccbtn" type='submit'>Create User Account</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>

<script type="text/javascript">
	
setInterval(function(){
	var p1 = $("#pass1").val();
	var p2 = $("#pass2").val();

	if(p1.length >= 1){
			if(p1 != p2){
			$("#creatuseraccbtn").prop("disabled",true);
			$("#creatuseraccbtn").html("Password do not match.");
	}else{
			$("#creatuseraccbtn").prop("disabled",false);
			$("#creatuseraccbtn").html("Create New Account");
	}
	}else{
		$("#creatuseraccbtn").prop("disabled",true);
	}
	
},300)
</script>

