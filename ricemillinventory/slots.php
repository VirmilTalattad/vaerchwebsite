<?php include("inc/auth.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Rice Mill Inventory - Slot Management</title>
	<?php
	include("theme/index.php");
	include("inc/central.php");
	include("func/displayer.php");
	?>
</head>
<body>

<div class="container">
	<?php include("comp/index.php") ;?>
	<div class='jumbotron jumbotron-fluid'>
	<div class='container'>
		<h4>Slot Management</h4>
	</div>
	</div>
<div class="row">
	<div class="col-sm-3">
            <div class="alert alert-info" role="alert">
  <i class="fas fa-info-circle"></i> <strong>Slot Managment</strong> Lets you create slots for storing items.
</div>
		<div class="list-group">
		  <a href="#" class="list-group-item list-group-item-action" data-toggle="modal" data-target="#addnewslotmodal"><i class="fas fa-plus-circle"></i> New Slot</a>

		</div>
	</div>
	<div class="col-sm-9">
<table id="tanstable" class="table table-striped table-bordered">
  <thead>
    <tr>
      <th scope="col">Slot Name</th>
      <th scope="col">Current</th>
      <th scope="col">Max</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
<?php ShowSlots(); ?>
  </tbody>
</table>

	</div>
</div>
</div>
</body>
</html>
<form action='<?php weblink(); ?>' method='POST'>
<div class="modal" tabindex="-1" role="dialog" id="addnewslotmodal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add New Slot</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="user" value="<?php useremail(); ?>">
 		<input type="hidden" name="tag" value="addslot">
 		<div class='form-group'>
 			<label>Slot Name:</label>
 			<input autocomplete="off" required="" type='text' class='form-control' name="name">
 		</div>
 		 <div class='form-group'>
 			<label>Max Quantity:</label>
 			<input autocomplete="off" required="" type='number' class='form-control' name="max_cap">
 		</div>

    <div class="row">
      <div class="col-sm-6">
             <div class='form-group'>
      <label>Slot Code:</label>
      <input autocomplete="off" required="" type='text' class='form-control' name="slot_code">
    </div>
      </div>
            <div class="col-sm-6">
             <div class='form-group'>
      <label>Slot Color:</label>
      <input autocomplete="off" required="" type='text' class='form-control' name="slot_color">
    </div>
      </div>
    </div>
      </div>
      <div class="modal-footer">
        	<button class='btn btn-primary' type='submit'>Add New Slot</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

</form>
<form action='<?php weblink(); ?>' method='POST'>
<div class="modal" tabindex="-1" role="dialog"  id="deleteslotmodal">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Delete Slot?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<p>Come again?</p>
        <input type="hidden" name="user" value="<?php useremail(); ?>">
 		<input type="hidden" name="tag" value="deleteslot">
 		<input id="slotdelid" type="hidden" name="id">
      </div>
      <div class="modal-footer">
        <button class='btn btn-danger' type='submit'>Delete Slot</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>

<script type="text/javascript">
 function slot_function_delete(controlled){
 	$("#slotdelid").val($(controlled).data("id"));
 }
</script>
<form action='<?php weblink(); ?>' method='POST'>
<div class="modal" tabindex="-1" role="dialog"  id="editslotmodal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Slot</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
 		<input type="hidden" name="tag" value="editslot">
 		<div class='form-group'>
 			<input id="edit_id" type="hidden" name="id">
      <input type="hidden" name="user" value="<?php useremail(); ?>">
        <input id="edit_oldslotnamereserve" autocomplete="off" required="" type='hidden' class='form-control' name="oldname">
 			<label>Slot Name:</label>
 			<input id="edit_slotname" autocomplete="off" required="" type='text' class='form-control' name="name">
 			<label>Slot Max Quantity:</label>
 			<input id="edit_slotmaxquantity" autocomplete="off" required="" type='text' class='form-control' name="max_cap">
 		</div>
      </div>
      <div class="modal-footer">
        	<button class='btn btn-success' type='submit'>Save Changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form>
<script type="text/javascript">
	 function slot_function_edit(controlled){
	 	$("#edit_id").val($(controlled).data("id"));
		$("#edit_slotname").val($(controlled).data("slotname"));
      $("#edit_oldslotnamereserve").val($(controlled).data("slotname"));
		$("#edit_slotmaxquantity").val($(controlled).data("maxquantity"));
	 }
</script>

<script type="text/javascript">
  
  $("#tanstable").DataTable({
            rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true
  });
</script>