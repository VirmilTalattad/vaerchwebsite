<?php include("inc/auth.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Rice Mill Inventory - Item Management</title>
	<?php
	include("theme/index.php");
	include("inc/central.php");
	include("func/displayer.php");
	?>
</head>
<body>

<div class="container">
	<?php include("comp/index.php") ;?>
	<div class='jumbotron jumbotron-fluid'>
	<div class='container'>
		<h4>Item Management</h4>
	</div>
	</div>

<div class="row">
	<div class="col-sm-3">
        <div class="alert alert-info" role="alert">
  <i class="fas fa-info-circle"></i> <strong>Item Management</strong> Lets you create and store items to your slots.
</div>
		<div class="list-group">
		  <a href="#" class="list-group-item list-group-item-action" data-toggle="modal" data-target="#addnewitemmodal"><i class="fas fa-plus-circle"></i> New Item</a>
		</div>
	</div>
	<div class="col-sm-9">
<table id="itemtable" class="table table-striped table-bordered">
  <thead>
    <tr>
      <th scope="col">Code</th>
      <th scope="col">Color</th>
      <th scope="col">Kilo</th>
      <th scope="col">Price per kg</th>
      <th scope="col">Slot</th>
      <th scope="col">Qty</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
<?php ShowItems(); ?>
  </tbody>
</table>

	</div>
</div>
</div>
</body>
</html>
<form action='<?php weblink(); ?>' method='POST'>
<div class="modal" tabindex="-1" role="dialog" id="addnewitemmodal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add New Item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
 		<input type="hidden" name="tag" value="additem">
         <input type="hidden" name="user" value="<?php useremail(); ?>">
              <div class='form-group'>
      <label>Slot:</label>
      <select class="form-control" id="selectedslot" required="" name="slot">
        <?php ShowSlots_Dropdown(); ?>
      </select>
    </div>
 		 <div class="row">
 		 	<div class="col-sm-9">
 		 		<div class='form-group'>
 			<label>Code Name:</label>
 			<input autocomplete="off" readonly="" required="" type='text' class='form-control' id="additem_cn" name="code">
 		</div>
 		 	</div>
 		 	<div class="col-sm-3">
 		 	<div class='form-group'>
 			<label>Color:</label>
 			<input autocomplete="off" readonly="" required="" type='text' class='form-control' id="additem_c" name="color" list="colors_data">

 		</div>	
 		 	</div>
 		 </div>
 		<div class="row">
 			<div class="col-sm-4">
 		<div class='form-group'>
 			<label>Kilo:</label>
 			<input autocomplete="off" required="" type='number' class='form-control' name="kilo">
 		</div>
 			</div>
 			<div class="col-sm-4">
	 		<div class='form-group'>
	 			<label>Price per Kilo:</label>
	 			<input autocomplete="off" required="" type='number' class='form-control' name="price">
	 		</div>
 			</div>
 				<div class="col-sm-4">
	 		<div class='form-group'>
	 			<label>Qty - <small>(number of cavan)</small></label>
	 			<input autocomplete="off" id="qtyvaltogive"  required="" min="1" type='number' class='form-control' name="qty">
	 		</div>
 			</div>
 		</div>

      </div>
      <div class="modal-footer">
        	<button class='btn btn-primary' type='submit'>Add New Item</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

</form>
<script type="text/javascript">
  $("#selectedslot").change(function(){
    var slname = $(this).val();
    $.ajax({
      type: "POST",
      url: "<?php weblink(); ?>",
      data: {"tag":"getslotnameavquantity","slotname":slname},
      success: function(data){
        var availableQuantity = parseInt(data);
          $("#qtyvaltogive").prop("max",availableQuantity);
          
      }
    })

      // GET SLOT PROPERTIES
    $.ajax({
      type: "POST",
      url: "<?php weblink(); ?>",
      data: {"tag":"GetSlotProperties","slotname":slname},
      success: function(data){
        data = JSON.parse(data);
     $("#additem_cn").val(data["slot_code"]);
          $("#additem_c").val(data["slot_color"]);
      }
    })
  })


	$("#itemtable").DataTable({
            rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true
  });
</script>
<form action='<?php weblink(); ?>' method='POST'>
<div class="modal" tabindex="-1" role="dialog" id="modaldeleteitem">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Delete Item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
 		<input type="hidden" name="tag" value="addslot">
 		<div class='form-group'>
 			<p>Come again?</p>
 			<input type="hidden" name="tag" value="deleteitem">
 			<input type="hidden" name="id" id="itemidtodelete">
         	<input type="hidden" name="user" value="<?php useremail(); ?>">
 		</div>
      </div>
      <div class="modal-footer">
        	<button class='btn btn-danger' type='submit'>Delete Item</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>
<script type="text/javascript">
	function DeleteItem(controlobject){
		$("#itemidtodelete").val($(controlobject).data("id"));
	}
</script>

<form action='<?php weblink(); ?>' method='POST'>
<div class="modal" tabindex="-1" role="dialog" id="modaledititem">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
            <div class="modal-body">
 		<input type="hidden" name="tag" value="edititem">
         <input type="hidden" name="user" value="<?php useremail(); ?>">
         <input type="hidden" name="id" id="editinput_id">
 		 <div class="row">
 		 	<div class="col-sm-9">
 		 		<div class='form-group'>
 			<label>Code Name:</label>
 			<input id="editinput_codename" autocomplete="off" required="" type='text' class='form-control' name="code">
 		</div>
 		 	</div>
 		 	<div class="col-sm-3">
 		 	<div class='form-group'>
 			<label>Color:</label>
 			<input id="editinput_color" autocomplete="off" required="" type='text' class='form-control' name="color" list="colors_data">

 		</div>	
 		 	</div>
 		 </div>
 		<div class="row">
 			<div class="col-sm-4">
 		<div class='form-group'>
 			<label>Kilo:</label>
 			<input id="editinput_kilo" autocomplete="off" required="" type='number' class='form-control' name="kilo">
 		</div>
 			</div>
 			<div class="col-sm-4">
	 		<div class='form-group'>
	 			<label>Price per Kilo:</label>
	 			<input id="editinput_priceperkilo" autocomplete="off" required="" type='number' class='form-control' name="price">
	 		</div>
 			</div>
 			<div class="col-sm-4">
	 		<div class='form-group'>
	 			<label>Quantity:</label>
	 			<input id="editinput_qty" autocomplete="off" required="" type='number' class='form-control' name="qty">
	 		</div>
 			</div>
 		</div>
 		<div class='form-group'>
 			<label>Slot:</label>
 			<select id="editinput_slotname" class="form-control" required="" name="slot">
 				<?php ShowSlots_Dropdown(); ?>
 			</select>
 		</div>
      </div>
      <div class="modal-footer">
        	<button class='btn btn-primary' type='submit'>Save Changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form>

<script type="text/javascript">
	function EditItem(controlobject){
		$("#editinput_id").val($(controlobject).data("id"));
		$("#editinput_codename").val($(controlobject).data("code"));
		$("#editinput_kilo").val($(controlobject).data("kilo"));
		$("#editinput_priceperkilo").val($(controlobject).data("price"));
		$("#editinput_slotname").val($(controlobject).data("slot"));
		$("#editinput_color").val($(controlobject).data("color"));
		$("#editinput_qty").val($(controlobject).data("qty"));
		$("#editinput_qty").prop("max",$(controlobject).data("max_qty"));
	}

    $("#editinput_slotname").change(function(){
    var slname = $(this).val();
    $.ajax({
      type: "POST",
      url: "<?php weblink(); ?>",
      data: {"tag":"getslotnameavquantity","slotname":slname},
      success: function(data){
        var availableQuantity = parseInt(data);
          $("#editinput_qty").prop("max",availableQuantity);
          
      }
    })
     })
</script>

 			<datalist id="colors_data">
 				<option>Blue</option>
 				<option>Red</option>
 				<option>Orange</option>
 				<option>Purple</option>
 				<option>Pink</option>
 				<option>Green</option>
 				<option>Yelloww</option>
 				<option>Brown</option>
 			</datalist>