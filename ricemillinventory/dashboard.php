<?php include("inc/auth.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>INV Sys - Home Screen</title>
	<?php
	include("theme/index.php");
	include("inc/central.php");
	include("func/displayer.php");
	?>
</head>
<body>
<style type="text/css">
/*	#mapcont{
		width: 1000px !important;
	}*/
	.laymap .btn-secondary{
		border: 2px solid rgba(0,0,0,0.2);
		background-color: rgba(0,0,0,0.07);
		border-radius: 1px;
		color: black;
		margin: 1px;
		width: 98%;
		height: 70px;
		font-size: 25px;
	}
		.laymap .col-sm-1{
		margin-top: 0px;
		padding: 0px;
	}
		.laymap .col-sm-2{
		margin-top: 0px;
		padding: 0px;
	}
		.laymap .col-sm-3{
		margin-top: 0px;
		padding: 0px;
	}
		.laymap .col-sm-4{
		margin-top: 0px;
		padding: 0px;
	}
		.laymap .col-sm-5{
		margin-top: 0px;
		padding: 0px;
	}
		.laymap .col-sm-6{
		margin-top: 0px;
		padding: 0px;
	}
		.laymap .col-sm-7{
		margin-top: 0px;
		padding: 0px;
	}
		.laymap .col-sm-8{
		margin-top: 0px;
		padding: 0px;
	}
		.laymap .col-sm-9{
		margin-top: 0px;
		padding: 0px;
	}
		.laymap .col-sm-10{
		margin-top: 0px;
		padding: 0px;
	}
		.laymap .col-sm-11{
		margin-top: 0px;
		padding: 0px;
	}
		.laymap .col-sm-12{
		margin-top: 0px;
		padding: 0px;
	}
</style>
<div class="container">
	<?php include("comp/index.php") ;?>
	<div class='jumbotron jumbotron-fluid'>
	<div class='container'>
		<h4>Dashboard</h4>
	</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="card">
				<div class="card-body">
					<a href="trans.php" class="float-right btn btn-primary"><i class="fas fa-arrow-right"></i></a>
					<h4>Transactions</h4>
					<h1 class="display-4"><?php Count_Transaction(); ?></h1>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="card">
				<div class="card-body">
					<a href="logs.php" class="float-right btn btn-primary"><i class="fas fa-arrow-right"></i></a>
					<h4>logs</h4>
					<h1 class="display-4"><?php Count_Logs(); ?></h1>
				</div>
			</div>
		</div>
	</div>
<hr>


<div id="mapcont">
	<div class="laymap">
		<div class="row">
		<div class="col-sm-11">
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-1">
						<button class="btn btn-secondary btn-block slotbtnitem" id="slot_55">55</button>
					</div>
					<div class="col-sm-3">
						<button class="btn btn-secondary btn-block slotbtnitem" id="slot_63">Supplies</button>
					</div>
					<div class="col-sm-8">
						
					</div>
				</div>
			</div>
			<!-- SEPARATION -->
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_56">56</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_57">57</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_58">58</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_59">59</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_60">60</button></div>
					<div class="col-sm-1"></div>
				</div>
			</div>
			<!-- SEPARATION -->
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_54">54</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_45">45</button></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_36">36</button></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_61">61</button></div>
				</div>
			</div>
		<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_53">53</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_44">44</button></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_35">35</button></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_62">62</button></div>
				</div>
			</div>
		<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_52">52</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_43">43</button></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_34">34</button></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
				</div>
			</div>
		<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_51">51</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_42">42</button></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_33">33</button></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
				</div>
			</div>
		<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_50">50</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_41">41</button></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_32">32</button></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
				</div>
			</div>
			<!-- SEPARATION -->
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_49">49</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_40">40</button></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_31">31</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_30">30</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_29">29</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_28">28</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_27">27</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_26">26</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_25">25</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_24">24</button></div>
					<div class="col-sm-1"></div>
				</div>
			</div>
						<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_48">48</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_39">39</button></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
				</div>
			</div>
									<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_47">47</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_38">38</button></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_46">46</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_37">37</button></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_15">15</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_16">16</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_17">17</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_18">18</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_19">19</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_20">20</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_21">21</button></div>
					<div class="col-sm-1"></div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-1"><button style="transform: scale(0);" class="btn btn-secondary btn-block slotbtnitem">x</button></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-1"><button style="transform: scale(0);" class="btn btn-secondary btn-block slotbtnitem">x</button></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					
					<div class="col-sm-1"></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_9">9</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_10">10</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_11">11</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_12">12</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_13">13</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_14">14</button></div>
					<div class="col-sm-1"></div>
				</div>
			</div>
						<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-1"><button style="transform: scale(0);" class="btn btn-secondary btn-block slotbtnitem">x</button></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					<div class="col-sm-1"></div>
					
					<div class="col-sm-1"></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_1">1</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_2">2</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_3">3</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_4">4</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_5">5</button></div>
					<div class="col-sm-1"><button class="btn btn-secondary btn-block slotbtnitem" id="slot_6">6</button></div>
					<div class="col-sm-1"></div>
				</div>
			</div>
		</div>

		<div class="col-sm-1">
			<div class="row">
				<div class="col-sm-12">
					<button style="transform: scale(0);" class="btn btn-secondary btn-block slotbtnitem">24</button>
				</div>
				<div  style="transform: scale(0);" class="col-sm-12">
					<button class="btn btn-secondary btn-block slotbtnitem">24</button>
				</div>
				<div  style="transform: scale(0);" class="col-sm-12">
					<button class="btn btn-secondary btn-block slotbtnitem">24</button>
				</div>
				<div  style="transform: scale(0);" class="col-sm-12">
					<button class="btn btn-secondary btn-block slotbtnitem">24</button>
				</div>
				<div  style="transform: scale(0);" class="col-sm-12">
					<button class="btn btn-secondary btn-block slotbtnitem">24</button>
				</div>
				<div  style="transform: scale(0);" class="col-sm-12">
					<button class="btn btn-secondary btn-block slotbtnitem">24</button>
				</div>
				<div  style="transform: scale(0);" class="col-sm-12">
					<button class="btn btn-secondary btn-block slotbtnitem">24</button>
				</div>
				<div  style="transform: scale(0);" class="col-sm-12">
					<button class="btn btn-secondary btn-block slotbtnitem">24</button>
				</div>
				<div class="col-sm-12">
					<button style="transform: scale(0);" class="btn btn-secondary btn-block slotbtnitem">24</button>
				</div>
				<div class="col-sm-12">
					<button  class="btn btn-secondary btn-block slotbtnitem" id="slot_23">23</button>
				</div>
				<div class="col-sm-12">
					<button class="btn btn-secondary btn-block slotbtnitem" id="slot_22">22</button>
				</div>
				<div class="col-sm-12">
					<button style="transform: scale(0);" class="btn btn-secondary btn-block slotbtnitem">24</button>
				</div>
				<div class="col-sm-12">
					<button class="btn btn-secondary btn-block slotbtnitem" id="slot_8">8</button>
				</div>
				<div class="col-sm-12">
					<button class="btn btn-secondary btn-block slotbtnitem" id="slot_7">7</button>
				</div>

			</div>
		</div>
	</div>
</div>
</div>
</div>
</body>
</html>


<script type="text/javascript">
	for(var i=1; i <= 63;i++){
		docheck(i);
		
	}

	function docheck(i){
	$.ajax({
		type:"POST",
		url:"func/server.php",
		data:{"tag":"slotchecker","slotnumber":i},
		success: function(data){
			if(data =="true"){
				$("#slot_" + i).prop("disabled",false);
				$("#slot_" + i).css("background-color","#1565C0");
				$("#slot_" + i).css("color","#64B5F6");
				$("#slot_" + i).css("border-color","#64B5F6");
				$("#slot_" + i).val(i);
			}else{
				$("#slot_" + i).prop("disabled",true);
			}
		}
	})
	}

			
</script>

<div class="modal" tabindex="-1" role="dialog" id="moreinfomodal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Slot: <span id="slot_name"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="row">
      	<div class="col-sm-3">
      		 <div class="form-group">
       	<label>Code</label>
       	<input type="text" readonly="" id="slot_code" class="form-control" name="">
       </div>
        <div class="form-group">
       	<label>Color</label>
       	<input type="text" readonly="" id="slot_color" class="form-control" name="">
       </div>
        <div class="form-group">
       	<label># of Bag</label>
       	<input type="text" readonly="" id="slot_numberofbag" class="form-control" name="">
       </div>
        
      	</div>
      	<div class="col-sm-9">
      		<div class="form-group">
       <button class="btn btn-primary float-right btn-sm" id="AddNewRemarks">Add</button>
       	<label>Remarks</label>
       	<input type="text" id="slot_remarkstoadd" placeholder="Type new remarks here..." class="form-control" name="">
       </div>
       <div class="card">
       	<div class="card-header">
       		<button class="btn btn-sm float-right" id="slot_loadallremarks"><i class="fas fa-redo"></i> Load All</button>
       		Recent
       	</div>
       	<table id="remdt" class="table more_space_table">
       		<thead>
       			<tr>
       				<th>Time</th>
       				<th>Remarks</th>
       			</tr>
       		</thead>
       		<tbody id="slot_detail">
       			
       		</tbody>
       	</table>
       </div>
      	</div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- JS code -->
<script src="https://code.jquery.com/jquery-3.1.1.min.js">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js">
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js">
</script>
<!--JS below-->
<script type="text/javascript">

	var slot_code = $("#slot_code");
	var slot_color = $("#slot_color");
	var slot_numberofbag = $("#slot_numberofbag");
	var slot_remarkstoadd = $("#slot_remarkstoadd");
	var slot_detail = $("#slot_detail");
	var slot_name = $("#slot_name");
	$("#slot_loadallremarks").click(function(){
		var theslotname = slot_name.html();
	// LOAD REMARKS
		$.ajax({
			type : "POST",
			url: "<?php weblink(); ?>",
			data: {tag: "LoadSlotRemarks_All",slotname: theslotname},
			success: function(data){
			slot_detail.html(data);
			// $("#remdt").DataTable();
			}
		})
	})
	$("#AddNewRemarks").click(function(){
		var theslotname = slot_name.html();
		slot_remarkstoadd = $("#slot_remarkstoadd").val();
		$.ajax({
			type : "POST",
			url: "<?php weblink(); ?>",
			data: {tag: "AddSlotNewRemarks",slotname: theslotname,remarkstoadd: slot_remarkstoadd},
			success: function(data){
				// alert(data);
				LoadRemarks(theslotname);
				 $("#slot_remarkstoadd").val("");
			}
		})

	})

	$(".slotbtnitem").click(function(){
		var myid = $(this).val();
		$("#moreinfomodal").modal("show");
		// LOAD DATA
		$.ajax({
			type : "POST",
			url: "<?php weblink(); ?>",
			data: {tag: "slotslotdetail",slotid: myid},
			success: function(data){
				// alert(data);
				data = JSON.parse(data);
				slot_code.val(data["slot_code"]);
				slot_color.val(data["slot_color"]);
				slot_numberofbag.val(data["max_qty"]);
				slot_detail.val(myid);
				slot_name.html(data["name"]);
			}
		})
	
		LoadRemarks(myid);

	})

	function LoadRemarks(remarksname){
			// LOAD REMARKS
		$.ajax({
			type : "POST",
			url: "<?php weblink(); ?>",
			data: {tag: "LoadSlotRemarks",slotname: remarksname},
			success: function(data){
			slot_detail.html(data);
			// $("#remdt").DataTable();
			}
		})

	}

	
</script>
