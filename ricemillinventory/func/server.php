<?php
include("../conn/conn.php");
$tag = $_POST["tag"];

//Operations
switch($tag){
	case "AddSlotNewRemarks":
		    $actualdate = date("Y-m-d"). " " . date("h:i:sa");
		    $slotname = htmlentities($_POST["slotname"]);
		    $remarkstoadd = htmlentities($_POST["remarkstoadd"]);

		    $q = "INSERT INTO remarks(text,timestamp,slot) VALUES('" .  $remarkstoadd . "','" . $actualdate . "','" .  $slotname. "')";
		    if(mysqli_query($c,$q)){
		    	echo "true";
		    }else{
		    	echo "false";
		    }
	break;
	case "LoadSlotRemarks_All":
		$slotname = htmlentities($_POST["slotname"]);
		$q = "SELECT * FROM remarks WHERE slot='" . $slotname . "' ORDER BY id DESC";
		$res = mysqli_query($c,$q);
		while ($row = mysqli_fetch_array($res)) {
			echo "<tr>
					<td>" . $row["timestamp"] . "</td>
					<td>" . $row["text"] . "</td>
				  </tr>";
		}
	break;
	case "LoadSlotRemarks":
		$slotname = htmlentities($_POST["slotname"]);
		$q = "SELECT * FROM remarks WHERE slot='" . $slotname . "' ORDER BY id DESC LIMIT 5";
		$res = mysqli_query($c,$q);
		while ($row = mysqli_fetch_array($res)) {
			echo "<tr>
					<td>" . $row["timestamp"] . "</td>
					<td>" . $row["text"] . "</td>
				  </tr>";
		}
	break;
	case "slotslotdetail":
		$slotid = mysqli_real_escape_string($c,$_POST["slotid"]);
		$slotid = htmlspecialchars($slotid);
		$q = "SELECT * FROM slots WHERE name='"  . $slotid . "'";
		echo json_encode(mysqli_fetch_array(mysqli_query($c,$q)));
	break;
	case "CreateUserAccount":
		$username = strtoupper(mysqli_real_escape_string($c,$_POST["username"]));
		$email =strtolower( mysqli_real_escape_string($c,$_POST["email"]));
		$password = mysqli_real_escape_string($c,$_POST["password"]);

		// CHECK IF ACCOUNT ALREADY EXIST

		$q = "SELECT * FROM users WHERE email ='" . $email . "' LIMIT 1";
		if(mysqli_num_rows(mysqli_query($c,$q)) == 0){
			// CREATE NEW ACCOUNT OPERATION
		$q = "INSERT INTO users(email,name,password,type) VALUES('" . $email . "','" . $username . "','" . $password . "','1')";

		if(mysqli_query($c,$q) or die(mysqli_error($c))){
		AddToLogs("Created a new user account.");
		Goto_message("New User Account Successfully Created!","../user.php");
		}else{
		AddToLogs("Failed to create a new user account.");
		Goto_message("Failed to create a new user account.","../user.php");
		}
		}else{
			// ALREADY EXISTING ACCOUNT DETECTED
		AddToLogs("Failed to create a new user account because email already exist.");
		Goto_message("Failed to create a new user account because email already exist.","../user.php");
		}
		
	break;
	case "slotchecker":
		$slotnumber = $_POST["slotnumber"];
		$q = "SELECT * FROM slots WHERE name='" . $slotnumber . "'";
		$res = mysqli_query($c,$q);
		if(mysqli_num_rows($res) == 0){
			echo "false";
		}else{
			echo "true";
		}
	break;
	case "DeleteUserAccount":
		$id = $_POST["id"];
		$q = "DELETE FROM users WHERE id='" . $_POST["id"] . "'";

		if(mysqli_query($c,$q) or die(mysqli_error($c))){
			AddToLogs("Deleted a user account.");
			Goto_message("User Account Successfully deleted!","../user.php");
		}else{
			AddToLogs("Failed to delete a user account.");
			Goto_message("Failed to delete user account.","../user.php");
		}
	break;
	case "UpdateTransaction":
		$id = $_POST["id"];
		$added_cash = $_POST["added_cash"];
		$added_check = $_POST["added_check"];
		if($added_cash == ""){
		$added_cash = 0;
		}
		if($added_check == ""){
		$added_check = 0;
		}
		$q = "SELECT * FROM transactions WHERE id='" . $id . "'";
		$row = mysqli_fetch_array(mysqli_query($c,$q));
		$old_cash = $row["cash_payment"];
		$old_check = $row["check_payment"];
		$old_cash += $added_cash;
		$old_check += $added_check;

		$q = "UPDATE transactions SET cash_payment='" . $old_cash . "',check_payment='" . $old_check . "' WHERE id='" . $id . "'";

		if(mysqli_query($c,$q) or die(mysqli_error($c))){
			AddToLogs("Successfully updated a new payment information.");
			Goto_message("Saved!","../trans.php");
		}else{
			AddToLogs("Failed to update new payment modifications.");
			Goto_message("Something went wrong.","../trans.php");
		}

	break;
	case "GetTransactionItems":
		$q = "SELECT * FROM transactions where id='" . $_POST["id"] . "' LIMIT 1";
		echo json_encode(mysqli_fetch_array(mysqli_query($c,$q)));
	break;
	case "getiteminfo":
		$q = "SELECT * FROM inventory WHERE id='" . $_POST["id"] . "' LIMIT 1";
		echo json_encode(mysqli_fetch_array(mysqli_query($c,$q)));
	break;
	case "login":
		$email = strtolower($_POST["email"]);
		$email = mysqli_real_escape_string($c,$email);
		$password = mysqli_real_escape_string($c,$_POST["password"]);
		$email = htmlentities($email);
		$password = htmlentities($password);
		$q = "SELECT * FROM users WHERE email='" . $email . "' LIMIT 1";
        $res = mysqli_query($c,$q);

		if(mysqli_num_rows($res) != 0){
		while($row = mysqli_fetch_array($res)){
		if($password == $row["password"]){
		session_start();
		$_SESSION["email"] = $email;
		$_SESSION["usertype"] = $row["type"];
		Goto_message("Success","../dashboard.php");
		}else{
			Goto_message("Password do not match.","../index.html");
			}
		}
		}else{
			Goto_message("Account not found.","../index.html");
		}	
	break;
	case "logout":
		session_start();
		session_destroy();
		Goto_message("Logged-out","../index.html");
	break;

	// SLOT MANAGEMENT
	// SLOT MANAGEMENT
	// SLOT MANAGEMENT
	case "addslot":
	$name = mysqli_real_escape_string($c,$_POST["name"]);
	$name = htmlentities($name);
	$max_cap = $_POST["max_cap"];
	$slot_code = mysqli_real_escape_string($c,$_POST["slot_code"]);
	$slot_color = mysqli_real_escape_string($c,$_POST["slot_color"]);
	
	// CHECKING
	$q = "SELECT * FROM slots WHERE name='" . $name ."'";
	if(mysqli_num_rows(mysqli_query($c,$q)) == 0){
		$q = "INSERT INTO slots(name,max_qty,slot_code,slot_color) VALUES('" . $name  . "','" . $max_cap . "','" . $slot_code . "','" . $slot_color . "')";
		if(mysqli_query($c,$q)){
			AddToLogs("Added new slot.");
			Goto_message("Slot added successfully!","../slots.php");
		}else{
			AddToLogs("Failed to add new slot.");
			Goto_message("Failed to add slot.","../slots.php");
		}
	}else{
		AddToLogs("Failed to add new slot because slot name already exist.");
			Goto_message("Failed to add new slot because it already exist in the system!","../slots.php");
	}
	break;
		case "editslot":
	$name = mysqli_real_escape_string($c,$_POST["name"]);
	$name = htmlentities($name);

		// CHECKING
	$q = "SELECT * FROM slots WHERE name='" . $name ."' AND max_qty='" . $_POST["max_cap"] ."'";
	if(mysqli_num_rows(mysqli_query($c,$q)) == 0){
				$q = "UPDATE slots SET name='" . $name . "', max_qty='" . $_POST["max_cap"] . "' WHERE id='" . $_POST["id"] . "'";
		if(mysqli_query($c,$q) or die(mysqli_error($c))){
			$q = "UPDATE inventory SET slot='" . $name . "' WHERE slot='" . $_POST["oldname"] . "'";
			mysqli_query($c,$q);
			AddToLogs("Edited a slot information.");
			Goto_message("Changes has been saved!","../slots.php");
		}else{
			AddToLogs("Failed to edit a slot information.");
			Goto_message("Failed to save changes.","../slots.php");
		}
	}else{
		AddToLogs("Failed to edit slot information because given information already exist.");
			Goto_message("Failed to edit slot information because it already exist in the system!","../slots.php");
	}

	break;
		case "deleteslot":
		$q = "DELETE FROM slots WHERE id='" . $_POST["id"] ."'";
		if(mysqli_query($c,$q)){
			AddToLogs("Deleted a slot.");
			Goto_message("Slot deleted successfully!","../slots.php");
		}else{
			AddToLogs("Failed to delete a slot.");
			Goto_message("Failed to delete slot.","../slots.php");
		}
	break;
	// ITEM MANAGEMENT
	// ITEM MANAGEMENT
	// ITEM MANAGEMENT
	case "additem":
		$code = mysqli_real_escape_string($c,$_POST["code"]);
		$kilo = mysqli_real_escape_string($c,$_POST["kilo"]);
		$price = mysqli_real_escape_string($c,$_POST["price"]);
		$slot = mysqli_real_escape_string($c,$_POST["slot"]);
		$qty = mysqli_real_escape_string($c,$_POST["qty"]);
		$color = mysqli_real_escape_string($c,$_POST["color"]);

		$code = htmlentities($code);
		$kilo = htmlentities($kilo);
		$slot = htmlentities($slot);
		$color = htmlentities($color);
		$qty =  htmlentities($qty);

		// CHECKING
		$q = "SELECT * FROM inventory WHERE code='" . $code . "' AND slot='" . $slot . "' AND color_code='" . $color . "'";
		if(mysqli_num_rows(mysqli_query($c,$q)) == 0){
		$q = "INSERT INTO inventory(code,kilo,price,slot,color_code,qty) VALUES('" . $code . "','" . $kilo . "','" . $price . "','" . $slot . "','" . $color . "','" . $qty . "')";

		if(mysqli_query($c,$q) or die(mysqli_error($c))){
			AddToLogs("Added a new item.");
			Goto_message("Item added successfully!","../item.php");
		}else{
			AddToLogs("Failed to add a new item.");
			Goto_message("Failed to add item.","../item.php");
		}
		}else{
			AddToLogs("Item already exist.");
			Goto_message("Failed to add item because it already exist.","../item.php");
		}
	break;
		case "deleteitem":
		$q = "DELETE FROM inventory WHERE id='" . $_POST["id"] ."'";
		if(mysqli_query($c,$q)){
			AddToLogs("Deleted an item.");
			Goto_message("Item deleted successfully!","../item.php");
		}else{
			AddToLogs("Failed to delete an item.");
			Goto_message("Failed to delete item.","../item.php");
		}
			break;
	case "edititem":
		$code = mysqli_real_escape_string($c,$_POST["code"]);
		$code = htmlentities($code);
		$kilo = $_POST["kilo"];
		$price = $_POST["price"];
		$slot = $_POST["slot"];
		$qty = mysqli_real_escape_string($c,$_POST["qty"]);
		$color = mysqli_real_escape_string($c,$_POST["color"]);
		$color = htmlentities($color);
		$qty = htmlentities($qty);
		// EXECUTE MAIN FUNCTION
		$q = "UPDATE inventory SET code='" . $code  . "',kilo='" . $kilo . "',price='" . $price . "',slot='" . $slot . "',color_code='" . $color .  "',qty='" . $qty . "' WHERE id='" . $_POST["id"] . "'";
		if(mysqli_query($c,$q) or die(mysqli_error($c))){
			AddToLogs("Item information has been updated.");
			Goto_message("Updated an item information!","../item.php");
			
		}else{
			AddToLogs("Failed to update Item's information.");
			Goto_message("Failed to update item information.","../item.php");
		}
			break;
			case "addnewtransaction":
				$buyername = strtoupper(mysqli_real_escape_string($c,$_POST["buyername"]));
				$contactnum = mysqli_real_escape_string($c,$_POST["contactnum"]);
				$approver = strtoupper(mysqli_real_escape_string($c,$_POST["approver"]));
				$items_Json = $_POST["items_Json"];

				$buyername = htmlentities($buyername);
				$contactnum = htmlentities($contactnum);
				$approver = htmlentities($approver);

				$cashpayment = (int)$_POST["cashpayment"];
				$checkpayment =(int) $_POST["checkpayment"];

				// DECREASE ALL ITEMS ORIGINAL QUANTITY
				$itm_j = json_decode($items_Json, true);
				for($i = 0; $i < count($itm_j);$i++){
					$q = "SELECT * FROM inventory WHERE id='" . $itm_j[$i]["item_id"] . "'";
					$s_itemrow = mysqli_fetch_array(mysqli_query($c, $q));
					$updated_qty = $s_itemrow["qty"] - $itm_j[$i]["item_qty"];
					$q = "UPDATE inventory SET qty='" . $updated_qty . "' WHERE id='" . $itm_j[$i]["item_id"] . "'";
					mysqli_query($c,$q) or die (mysqli_error($c));
				}

				$q = "INSERT INTO transactions(buyername,contact,approver,buyeditems,cash_payment,check_payment) VALUES('" . $buyername . "','" . $contactnum . "','" . $approver . "','" . $items_Json . "','" . $cashpayment . "','" . $checkpayment . "')";

				if(mysqli_query($c,$q) or die(mysqli_error($c))){
				AddToLogs("Added a new transaction.");
				Goto_message("Transaction recorded successfully!","../trans.php");
				}else{
				AddToLogs("Failed to add new transaction.");
				Goto_message("Failed to record transaction.","../trans.php");
				}
			break;
			case "deletetrans":
				$q = "DELETE FROM transactions WHERE id='" . $_POST["id"] . "'";
				if(mysqli_query($c,$q) or die(mysqli_error($c))){
				AddToLogs("Deleted a new transaction.");
				Goto_message("Transaction deleted successfully!","../trans.php");
				}else{
				AddToLogs("Failed to deleted a transaction data.");
				Goto_message("Failed to delete a transaction.","../trans.php");
				}
			break;
			case "getslotnameavquantity":
				$slotname = htmlentities($_POST["slotname"]);
				$q = "SELECT * FROM slots WHERE name='" . $slotname ."'";
				$row = mysqli_fetch_array(mysqli_query($c,$q));
				$max_qty = $row["max_qty"];
				// GET CURRENT QTY
				$q = "SELECT *, SUM(qty) as currqty FROM inventory WHERE slot='" . $slotname ."' GROUP BY slot";
				$row = mysqli_fetch_array(mysqli_query($c,$q));
				$current_qty = $row["currqty"];
				$sumqty = $max_qty - $current_qty;
				echo $sumqty;
			break;
			case 'GetSlotProperties':
				$slotname = htmlentities($_POST["slotname"]);
				$q = "SELECT * FROM slots WHERE name='" . $slotname ."' LIMIT 1";
				echo json_encode(mysqli_fetch_array(mysqli_query($c,$q)));
				break;
		}

function Goto_silent($location){
	?>
	<script type="text/javascript">
		location.href= <?php echo json_encode($location); ?>;
	</script>
	<?php
}
function Goto_message($message,$location){
	?>
	<script type="text/javascript">
		alert(<?php echo json_encode($message); ?>);
		location.href= <?php  echo json_encode($location); ?>;
	</script>
	<?php
}

function AddToLogs($actionperformed){
	global $c;
	$user = $_POST["user"];
	$actionperformed = mysqli_real_escape_string($c,$actionperformed);
	$actionperformed = htmlentities($actionperformed);
    $actualdate = date("Y-m-d"). " " . date("h:i:sa");
	$q = "INSERT INTO logs(user,action,timestamp) VALUES('" . $user . "','" . $actionperformed . "','" . $actualdate ."')";
	mysqli_query($c,$q);

}
?>