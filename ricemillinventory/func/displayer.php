<?php
include("conn/conn.php");
//Funtions
function Count_Transaction(){
	global $c;
	$q="SELECT * FROM transactions";
	echo mysqli_num_rows(mysqli_query($c,$q));
}
function Count_Logs(){
	global $c;
	$q="SELECT * FROM logs";
	echo mysqli_num_rows(mysqli_query($c,$q));
}
function ShowLogs(){
	global $c;
	$q="SELECT * FROM logs ORDER BY id DESC";
	$res = mysqli_query($c,$q);
	while($row = mysqli_fetch_array($res)){
		echo "<tr>
			<td>" . $row["timestamp"] . "</td>
			<td>" . $row["user"] . "</td>
			<td>" . $row["action"] . "</td>
		</tr>";
	}
}
function ShowSlots(){
	global $c;
	$q="SELECT * FROM slots";
	$res = mysqli_query($c,$q);
	while($row = mysqli_fetch_array($res)){


		// COUNT CURRENT SLOT QUANITTIY
		$q = "SELECT id, SUM(qty) as overall_qty FROM inventory WHERE slot='" . $row["name"] ."' GROUP BY slot";
		$items_row = mysqli_fetch_array(mysqli_query($c, $q));
		$itemscount = 0;
		if($items_row["overall_qty"] != ""){
			$itemscount = $items_row["overall_qty"];
		}
		echo "<tr>
		<td>" . $row["name"] . "</td>
		<td>" . $itemscount . "</td>
		<td>" . $row["max_qty"] . "</td>
		<td>
" .'
  	<div class="dropdown">
  	  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  	  </button>
  	  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
  	    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#editslotmodal"
  	    data-id="' . $row["id"] . '"
  	    data-slotname="' . $row["name"] . '"
  	    data-maxquantity="' . $row["max_qty"] . '"
  	    onclick="slot_function_edit(this)"
  	    >Edit</a>
  	    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#deleteslotmodal"
  	    data-id="'. $row["id"] .'"
  	    onclick="slot_function_delete(this)"
  	    >Delete</a>
  	  </div>
  	</div>
  	'."
		</td>
		</tr>";
	}
}
function ShowSlots_Dropdown(){
	global $c;
	$q="SELECT * FROM slots";
	$res = mysqli_query($c,$q);
	echo "<option value='" . $row["name"] . "' disabled selected>...</option>";
	while($row = mysqli_fetch_array($res)){

		// COUNT CURRENT SLOT QUANITTIY
		$q = "SELECT id, SUM(qty) as overall_qty FROM inventory WHERE slot='" . $row["name"] ."' GROUP BY slot";
		$items_row = mysqli_fetch_array(mysqli_query($c, $q));
		$itemscount = 0;
		if($items_row["overall_qty"] != ""){
			$itemscount = $items_row["overall_qty"];
		}
		if($items_row["overall_qty"] == $row["max_qty"]){
			echo "<option value='" . $row["name"] . "'>" . $row["name"] ." (<small>" . $itemscount . "/" . $row["max_qty"] . "</small>) Full</option>";
		}else{
			echo "<option value='" . $row["name"] . "'>" . $row["name"] ." (<small>" . $itemscount . "/" . $row["max_qty"] . "</small>)</option>";
		}
	}
}
function ShowItems(){
	global $c;
	$q="SELECT * FROM inventory";
	$res = mysqli_query($c,$q);
	while($row = mysqli_fetch_array($res)){

		//GET MAX SLOT QUANTITY
		$q="SELECT * FROM slots WHERE name='" . $row["slot"] . "' LIMIT 1";
		$slot_data = mysqli_fetch_array(mysqli_query($c,$q));
		$max_slot_qty = $slot_data["max_qty"];

		// COUNT CURRENT SLOT QUANITTIY
		$q = "SELECT id, SUM(qty) as overall_qty FROM inventory WHERE slot='" . $row["slot"] ."' GROUP BY slot";
		$items_row = mysqli_fetch_array(mysqli_query($c, $q));
		$itemscount = 0;
		if($items_row["overall_qty"] != ""){
			$itemscount = $items_row["overall_qty"];
		}
		$max_slot_qty += $row["qty"];
		$max_slot_qty -= $itemscount;
		echo "<tr>
			<td>" . $row["code"] . "</td>
			<td>" . $row["color_code"] . "</td>
			<td>" . $row["kilo"] . "</td>
			<td>" . $row["price"] . "</td>
			<td>" . $row["slot"] . "</td>
			<td>" . $row["qty"] . "</td>
			<td>

			<div class='dropdown'>
			  <button class='btn btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
			  </button>
			  <div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>
			    <a class='dropdown-item' href='#' data-toggle='modal' data-target='#modaledititem'
			    	data-id = '" . $row["id"] . "'
			    	data-code = '" . $row["code"] . "'
			    	data-kilo = '" . $row["kilo"] . "'
			    	data-price = '" . $row["price"] . "'
			    	data-slot = '" . $row["slot"] . "'
			    	data-color = '" . $row["color_code"] . "'
			    	data-qty= '" . $row["qty"] . "'
			    	data-max_qty='" . $max_slot_qty . "'
			    	onclick= 'EditItem(this)'
			    >Edit</a>
			    <a class='dropdown-item' href='#' data-toggle='modal' data-target='#modaldeleteitem' data-id='" . $row["id"] . "' onclick='DeleteItem(this)'>Delete</a>
			  </div>
			</div>

			</td>
		</tr>";
	}
}
function ShowUsers(){
	global $c;
	$q="SELECT * FROM users";
	$res = mysqli_query($c,$q);
	while($row = mysqli_fetch_array($res)){
		$typearray = ["Administrator","Secondary User"];
		echo "<tr>
			<td>" . $row["name"] . "</td>
			<td>" . $row["email"] . "</td>
			<td>" . $typearray[$row["type"]] . "</td>
			<td>";

			if($_SESSION["email"] ==  $row["email"]){
		echo "(Current User)";
			}else{
				
						echo "
			<div class='dropdown'>
			  <button class='btn btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
			  </button>
			  <div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>
			    <a class='dropdown-item deluseracc' data-id='" .$row["id"] . "' href='#' data-toggle='modal' data-target='#modal_deleteuser' data-id='" . $row["id"] . "' onclick='DeleteItem(this)'>Delete</a>
			  </div>
			</div>
					";
			}

			echo "</td>
		</tr>";
	}
}
function ShowTransations(){
	global $c;
	$q="SELECT * FROM transactions";
	$res = mysqli_query($c,$q);
	while($row = mysqli_fetch_array($res)){


		$paidBalance = $row["cash_payment"] + $row["check_payment"];


		$buyed = json_decode($row["buyeditems"],true);
		$totalbuyed = 0;
		for($i = 0; $i < count($buyed);$i++){
			$q = "SELECT * FROM inventory WHERE id='" . $buyed[$i]["item_id"] . "'";
			$xrow = mysqli_fetch_array(mysqli_query($c,$q));
			$totalbuyed += ($xrow["price"] * $xrow["kilo"]) * $buyed[$i]["item_qty"];
		}
		$totalbalance = $totalbuyed - $paidBalance;
		echo "<tr>
		<td>" . str_replace(" ", "<br>", $row["timestamp"]) . "</td>
		<td>" . $row["buyername"] . "</td>
		<td>" . $row["contact"] . "</td>
		<td>" . $row["approver"] . "</td>
		<td>";
		echo "<ul style='font-size: 15px;'>";

		for($i = 0; $i < count($buyed);$i++){
			$q = "SELECT * FROM inventory WHERE id='" . $buyed[$i]["item_id"] . "'";
			$xrow = mysqli_fetch_array(mysqli_query($c,$q));
			echo "<li>"  . $xrow["code"] . " (" . $xrow["color_code"] . ")<strong> <span style='color:#3F51B5;'>₱ " . number_format(($xrow["price"] * $xrow["kilo"])) ."</span></strong>, <span >Qty: <strong style='color:#FF9800;'>" . $buyed[$i]["item_qty"] . "</strong></span></li>";
		}
		echo "</ul>";
		echo "</td>
		<td>₱ " . number_format($totalbuyed) . "</td>
		<td>₱ " . number_format($row["cash_payment"]) . "</td>
		<td>₱ " . number_format($row["check_payment"]) . "</td>
		<td>₱ " . number_format($totalbalance ) ."</td>
		<td><div class='dropdown'>
			  <button class='btn btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
			  </button>
			  <div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>
			    <a class='dropdown-item manageTransModalShow' href='#' data-toggle='modal' data-target='#trans_editmodal'
			    	data-paid='" . ($row["cash_payment"] + $row["check_payment"]). "'
			    	data-currentbal='" . $totalbalance. "'
			    	data-id='" . $row["id"] . "'
			    ><i class='fas fa-cog'></i> Manage</a>
			   <form action='comp/print_receipt.php' method='POST' target='_blank'>
<input type='hidden' name='id' value='" . $row["id"] . "'>

 <button type='submit' class='dropdown-item TransEditButton' href='#' data-toggle='modal' data-target='#'><i class='fas fa-print'></i> Print</button>

			   </form>
			    <a class='dropdown-item TransDelButton' data-id='" . $row["id"] . "' href='#' data-toggle='modal' data-target='#delete_transactionModalFom'><i class='fas fa-trash'></i> Delete</a>
			  </div>
			</div></td>
		</tr>";
	}
}
function LoadItemNames_Option(){
	global $c;
	$q="SELECT * FROM inventory";
	$res = mysqli_query($c,$q);
	while($row = mysqli_fetch_array($res)){
		echo "<option value='" . $row["id"] . "'>" . $row["code"] . "(" . $row["color_code"] .") - " . $row["slot"] . "</option>";
	}
}
?>