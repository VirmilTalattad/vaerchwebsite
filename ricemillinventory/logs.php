<?php include("inc/auth.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Rice Mill Inventory - Logs</title>
	<?php
	include("theme/index.php");
	include("inc/central.php");
	include("func/displayer.php");
	?>
</head>
<body>

<div class="container">
	<?php include("comp/index.php") ;?>
	<div class='jumbotron jumbotron-fluid'>
	<div class='container'>
		<h4>Most Recent Logs</h4>
	</div>
	</div>
          <div class="alert alert-info" role="alert">
  <i class="fas fa-info-circle"></i> <strong>Logs</strong> Lets you keep in track of all the operations done in the system.
</div>
<div class="row">

	<div class="col-sm-12">
<table id="logtable" class="table table-striped table-bordered">
  <thead>
    <tr>
      <th scope="col">Timestamp</th>
      <th scope="col">User</th>
      <th scope="col">Action Performed</th>
    </tr>
  </thead>
  <tbody>
<?php ShowLogs(); ?>
  </tbody>
</table>
	</div>
</div>
</div>
</body>
</html>
<script type="text/javascript">
	$("#logtable").DataTable({
            rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true,
        dom: 'Bfrtip',
        buttons: [
            'print'
        ],
          "ordering": false
  });
</script>

