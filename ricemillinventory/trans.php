<?php include("inc/auth.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Rice Mill Inventory - Transaction Management</title>
	<?php
	include("theme/index.php");
	include("inc/central.php");
	include("func/displayer.php");
	?>

</head>
<body>

<div class="container">
	<?php include("comp/index.php") ;?>
	<div class='jumbotron jumbotron-fluid'>
	<div class='container'>
		<h4>Transactions    <a href="#" class="float-right btn btn-primary" data-toggle="modal" data-target="#newtransmodal"><i class="fas fa-plus"></i> New</a></h4>
	</div>
	</div>
<div class="row">

	<div class="col-sm-12">
<table id="tanstable" class="table table-striped table-bordered more_space_table">
  <thead>
    <tr>
      <th>Time</th>
      <th>Buyer</th>
      <th>Contact</th>
      <th>Approver</th>
      <th>Purchased Item(s)</th>
      <th>Total</th>
      <th>Cash</th>
      <th>Check</th>
      <th>Balance</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
<?php ShowTransations(); ?>
  </tbody>
</table>

	</div>
</div>
</div>
</body>
</html>
<?php
include("comp/transfunctions.php");
?>

<form action='<?php weblink(); ?>' method='POST'>
<div class="modal" tabindex="-1" role="dialog" id="trans_editmodal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
          <!-- ESSENTIALS - START -->
     <input type="hidden" name="tag" value="UpdateTransaction">
    <input type="hidden" name="user" value="<?php useremail(); ?>">
    <input type="hidden" id="idtomanage" name="id">
              <!-- ESSENTIALS - END -->
           <!-- FORM VISIBLE PART - START -->

  <?php 
    if($_SESSION["usertype"] == 0){
  ?>
    <a href="#" class="float-right" data-toggle="modal" data-target="#newtransmodal" onclick="passinfotrans()" data-dismiss="modal"><i class="fas fa-shield-alt"></i> Fabricate</a>
  <?php 
    }else{
      ?>
    <div class="alert alert-primary" role="alert">
      <i class="fas fa-shield-alt"></i> Fabrication not available for this current user type.
    </div>
      <?php
    }
  ?>
<h4>Buyed Items</h4>
<table class="table table-striped table-bordered more_space_table">
  <thead>
    <tr>
      <th>Item Name</th>
      <th>Price</th>
      <th>Quantity</th>
    </tr>
  </thead>
  <tbody id="itemsbuyed">
    
  </tbody>
</table>
<div class="row">
  <div class="col-sm-6">
    <div class="form-group">
      <label>Paid Amount</label>
      <input readonly="" id="man_paidamount" class="form-control form-control-lg" type="number" placeholder="0" name="">
    </div>
  </div>
   <div class="col-sm-6">
    <div class="form-group">
      <label>Current Balance</label>
      <input readonly="" id="man_currentbal" class="form-control form-control-lg" type="number" placeholder="0" name="">
    </div>
  </div>
  <div class="col-sm-12">
    <h4>Payment</h4>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
      <label>Add Cash</label>
      <input class="form-control" id="man_cashadded" type="number" placeholder="Enter amount here.." name="added_cash">
    </div>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
      <label>Add Check</label>
      <input class="form-control" id="man_checkadded" type="number" placeholder="Enter amount here.." name="added_check">
    </div>
  </div>
</div>
     <!-- FORM VISIBLE PART - END -->
      </div>
      <div class="modal-footer">
          <button class='btn btn-primary' type='submit' id="applypaymentbutton">Apply Payment</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>

<script type="text/javascript">
  // FABRICATE INFORMATION
  function passinfotrans(){
    alert("alert");
  }
  // CURRENTS
  var currentbal = 0;
  setInterval(function(){
   if(currentbal != 0){
     var check = $("#man_cashadded").val();
    var cash = $("#man_checkadded").val();
    var totalpaycurrent = check + cash;

    // DISPLAY CURRENT BALANCE
    var computedbal = currentbal - totalpaycurrent;
    $("#man_currentbal").val(computedbal);

    if(computedbal < 0){
      $("#applypaymentbutton").prop("disabled",true);
       $("#applypaymentbutton").html("Please pay exact amount only");
    }else{
       $("#applypaymentbutton").prop("disabled",false);
        $("#applypaymentbutton").html("Apply Payment");
    }
   }
  },300)


  $("#tanstable").on("click",".manageTransModalShow",function(){
    currentbal = $(this).data("currentbal");
    $("#man_cashadded").val("");
    $("#man_checkadded").val("");
    $("#idtomanage").val($(this).data("id"));

    if($(this).data("currentbal") == 0){
        $("#applypaymentbutton").prop("disabled",true);
        $("#man_cashadded").prop("disabled",true);
        $("#man_checkadded").prop("disabled",true);
    }else{
        $("#applypaymentbutton").prop("disabled",false);
                $("#man_cashadded").prop("disabled",false);
        $("#man_checkadded").prop("disabled",false);
    }

    $("#itemsbuyed").html("");
    $("#man_paidamount").val($(this).data("paid"));
    $("#man_currentbal").val($(this).data("currentbal"));

     $("#man_currentbal").prop("max",$(this).data("currentbal"));
    $("#man_cashadded").prop("max",$(this).data("currentbal"));

 var idofTrans = $(this).data("id");

 $.ajax({
  type : "POST",
  url: "<?php weblink(); ?>",
  data: {"tag":"GetTransactionItems","id":idofTrans},
  success: function(data){
    data = JSON.parse(data);
    dataitems = JSON.parse(data["buyeditems"]);
    for(var i=0; i < dataitems.length;i++){

      // GET ITEM INFORMATION
      !function(i){
          $.ajax({
              type : "POST",
              url: "<?php weblink(); ?>",
              data: {"tag":"getiteminfo","id":dataitems[i]["item_id"]},
              success: function(datax){
                // alert(datax);
                datax = JSON.parse(datax);
                var toappeninitemsbuyed = "";
                toappeninitemsbuyed += "<tr>";
                toappeninitemsbuyed += "<td>" + datax["code"] + " (" + datax["color_code"] +")</td>";
                toappeninitemsbuyed += "<td>" + (datax["price"] * datax["kilo"]) + "</td>";
                toappeninitemsbuyed += "<td>" + dataitems[i]["item_qty"] + "</td>";
                toappeninitemsbuyed += "</tr>";

                 $("#itemsbuyed").append(toappeninitemsbuyed);
              }
          })

      }(i)

    }
         
  }
 })
  })
</script>