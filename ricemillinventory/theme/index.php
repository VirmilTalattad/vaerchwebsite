

<link rel="shortcut icon" type="image/png" href="img/fav.png"/>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
<script type="text/javascript" src="https://cdn.datatables.net/rowreorder/1.2.5/js/dataTables.rowReorder.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.2.5/css/rowReorder.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

<!-- ROBOTO -->
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

  <style type="text/css">
      /*CUSTOM CSS*/
    .more_space_table tr td{
      padding: 10px !important;
      height: 20px !important;
      font-size: 12px;
    }
    .more_space_table tr th{
      padding: 10px !important;
      height: 20px !important;
      font-size: 12px;
    }
    .more_space_table tr{
      padding: 10px !important;
      height: 20px !important;
      font-size: 12px;
    }
    .more_space_table tr td .form-control{
      font-size: 12px !important;
      padding: 5px !important;
      margin: 0px !important;
    }
    .jumbotron{
      color: white;
       background-image: linear-gradient(to right, #3F51B5 , #00BCD4);
       border-radius: 4px;
    }
    .navbar {
       background-color: transparent !important;
    }
    body{
      font-family: roboto;
      /*background-image: linear-gradient(#F5F5F5, white);*/
      background-size: cover;
      background-attachment: fixed;
      animation: soft_appear;
      animation-duration: 1s;
    }
    .modal{
      animation: soft_fadein;
      animation-duration: 0.5s;
    }
    .modal-content{
      border: 0px !important;
    }
    .modal-dialog{
      animation: soft_appear_modal;
      animation-duration: 0.5s;
      box-shadow: 0px 25px 50px rgba(0,0,0,0.3);
      border-radius: 3px !important;
      border: 0px !important;
    }
    .modal-dialog .modal-header{
      border: 0px !important;
      background-image: linear-gradient(to right, #3F51B5 , #00BCD4);
      color: white;
    }
    .modal-dialog .modal-footer{
      border: 0px !important;
    }
    .modal-sec-modal{
      margin-top: 10px !important;
    }
    .modal-sec-content{
      background-image: linear-gradient(to right, #3F51B5 , #00BCD4);
      color: white;
    }

    /*ANIMATIONS*/
    @keyframes soft_appear{
      0%{
        opacity: 0.5;
      }
      100%{
        opacity: 1;
      }
    }
    @keyframes soft_fadein{
      0%{
        opacity: 0 !important;
      }
      100%{
        opacity: 1 !important;
      }
    }
        @keyframes soft_appear_modal{
      0%{
        opacity: 0;
        margin-top: 100px;
      }
      100%{
        opacity: 1;
      }
    }
    /*CUSTOM CSS*/
  </style>