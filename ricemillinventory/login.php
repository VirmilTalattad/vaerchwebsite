<?php 	include("inc/logcheck.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Rice Mill Inventory - Login</title>
	<?php
	include("theme/index.php");
	include("inc/central.php");

	?>
</head>
<body style="
      color: white;
       background-image: linear-gradient(to right, #3F51B5 , #00BCD4);
       ">

<div class="container">
<center>
	<div style="max-width: 400px;">
			<div class="row">
	<div class="col-sm-12">
<br>
<br>
			

	</div>
	<div class="col-sm-12">
		<div class="container">
			<br>
			<br>
			<form action="<?php weblink(); ?>" method="post" style="text-align:left;">
				<h1><img src="img/icon.png" class="float-right" style="width: 70px;"><strong>RICE MILL</strong></h1>
				<h4>Transaction Center Login</h4><br>
				<input type="hidden" name="tag" value="login">
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
					<label>Email</label>
					<input type="email" style="background-color: rgba(255,255,255,0.7); border: 3px solid white;" autocomplete="off" class="form-control" required="" placeholder="Type Email..." maxlength="222" name="email">
				</div>
					</div>
					<div class="col-sm-12">
					<div class="form-group">
					<label>Password</label>
					<input type="password" style="background-color: rgba(255,255,255,0.7); border: 3px solid white;" class="form-control" required="" placeholder="Enter password..." maxlength="222" name="password">
				</div>
					</div>
				</div>
				
				<div class="form-group">
					<button class="btn btn-light btn-lg btn-block"><i class="fas fa-sign-in-alt"></i> Sign-in</button>
				</div>
			</form>
			
		</div>
	</div>
</div>
	</div>
</center>
</div>
</body>
</html>

