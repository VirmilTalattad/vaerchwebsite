<script type="text/javascript">
	$(document).ready(function() {
    $('#tanstable').DataTable({
            rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true
  });
} );
</script>
<form action='<?php weblink(); ?>' method='POST'>
<div class="modal" tabindex="-1" role="dialog" id="delete_transactionModalFom">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Delete Transaction</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <!-- ESSENTIALS - START -->
     <input type="hidden" name="tag" value="deletetrans">
    <input type="hidden" name="user" value="<?php useremail(); ?>">
              <!-- ESSENTIALS - END -->
           <!-- FORM VISIBLE PART - START -->
           <p>Come again?</p>
      <input type="hidden" name="id" value="" id="TransToDeleteId">
     <!-- FORM VISIBLE PART - END -->
      </div>
      <div class="modal-footer">
          <button class='btn btn-danger' type='submit'>Delete Transaction</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form>


<script type="text/javascript">
  $(".TransDelButton").click(function(){
    $("#TransToDeleteId").val($(this).data("id"));
  })
</script>



<form action='<?php weblink(); ?>' method='POST'>
<div class="modal" tabindex="-1" role="dialog" id="newtransmodal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">New Transaction</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="row">
      		<div class="col-sm-4">
      			        <!-- ESSENTIALS - START -->
 		<input type="hidden" name="tag" value="addnewtransaction">
    	<input type="hidden" name="user" value="<?php useremail(); ?>">
        <!-- ESSENTIALS - END -->
      <!-- FORM VISIBLE PART - START -->

      <div class="row">
      	<div class="col-sm-12">
		<div class='form-group'>
 			<label>1. Buyer's Name:</label>
 			<input autocomplete="off" placeholder="ex: Virmil Talattad" required="" type='text' class='form-control' name="buyername">
 		</div>
      	</div>
      	<div class="col-sm-12">
		<div class='form-group'>
 			<label>2. Contact Number:</label>
 			<input autocomplete="off" required="" placeholder="ex: 09xxxxxxxxx" type='text' class='form-control' name="contactnum">
 		</div>
      	</div>
      	<div class="col-sm-12">
		<div class='form-group'>
 			<label>3. Approver:</label>
 			<input autocomplete="off" required="" placeholder="ex: Name of approver" type='text' class='form-control' name="approver">
 		</div>
      	</div>
      </div>

      		</div>
      		<div class="col-sm-8">
      			 		<div class='form-group'>
 			<a href="#" class="float-right" data-toggle="modal" id="additembtnx" data-target="#additemmodal"><i class="fas fa-plus"></i> Add Item</a>
 			<label>4. Items:</label>
 			<table class="table table-striped table-bordered more_space_table">
 				<thead>
 					<tr>
 						<th>Qty</th>
 						<th>Particulars</th>
 						<th>Price</th>
 						<th>Total</th>
            <th>Action</th>
 					</tr>
 				</thead>
 				<tbody id="items_table">

 				</tbody>
 				<tfoot>
 					 <tr>
 						<th colspan="3">Grand Total</th>
 						<td id="totalofallitems">0</td>
 					</tr>
 					<tr>
 						<th colspan="3">Cash</th>
 						<td>
 								<input type="number" id="inp_cash" class="form-control" name="cashpayment">
 						</td>
 					</tr>
 					<tr>
 						<th colspan="3">Check</th>
 						<td>
 								<input type="number" id="in_check" class="form-control" name="checkpayment">
 						</td>
 					</tr>
 					 <tr>
 						<th colspan="3">Balance</th>
 						<td id="totalofall">
 							0
 						</td>
 					</tr>
 				</tfoot>
 			</table>
      <textarea  id="json_code_text" name="items_Json" rows="3" style="width: 100%; display: none;">
        
      </textarea>
 		</div>
      		</div>
      	</div>
     <!-- FORM VISIBLE PART - END -->
      </div>
      <div class="modal-footer">
        	<button class='btn btn-primary' type='submit' id="createtrans" name="subtrans_print">Create</button>
      </div>
    </div>
  </div>
</div>
</form>
<script type="text/javascript">
  setInterval(function(){
    if($("#json_code_text").val() == ""){
      $("#createtrans").prop("disabled",true);
       $("#createtrans").html("Add item(s) first!");
    }else{
      $("#createtrans").prop("disabled",false);
       $("#createtrans").html("Create Transaction");
    }
  },500)
</script>
<div class="modal" tabindex="-1" role="dialog" id="additemmodal">
  <div class="modal-dialog modal-sec-modal" role="document">
    <div class="modal-content modal-sec-content">
      <div class="modal-header">
        <h5 class="modal-title">Add an Item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
            <label>Search Item Name: </label>
            <select class="form-control" id="item_name_option">
              <option disabled="" selected="">...</option>
              <?php LoadItemNames_Option(); ?>
            </select>
          </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Max Quantity</label>
                <input type="number" class="form-control" id="maxqinput" value="0" readonly="" name="">
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
            <label>Quantity: </label>
            <input class="form-control" value="1" type="number" name="" id="item_qty">
          </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="additmbtn_purchase" onclick="Trans_AddItems()"  data-dismiss="modal" >Add Item</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>



<script type="text/javascript">
  $("#json_code_text").val("");

  // $("#item_name_option").change(function(){
  //   alert($(this).val());
  // })

  $("#additembtnx").click(function(){
    $("#additmbtn_purchase").prop("disabled",true);
      $("#item_name_option").val("");
        $("#item_qty").val("0");
  })
  $("#item_name_option").change(function(){
    CheckItemQty(); 
    $("#item_qty").val("0");
  })
  $("#item_qty").change(function(){
CheckItemQty(); 
  })
   $("#item_qty").keyup(function(){
    CheckItemQty(); 
  })

  function CheckItemQty(){

          var id = $("#item_name_option").val();
    $.ajax({
      type : "POST",
      url : "<?php weblink(); ?>",
      data: {"tag":"getiteminfo","id":id},
      success: function(data){
             data = JSON.parse(data);
       $("#maxqinput").val(data["qty"]);
         if($("#item_qty").val() == "" || $("#item_qty").val() < 0  || $("#item_qty").val() == 0){
      $("#additmbtn_purchase").prop("disabled",true);
     }else{
      if(parseInt(data["qty"] )< parseInt( $("#item_qty").val())){
      $("#additmbtn_purchase").prop("disabled",true);
     }else{
      $("#additmbtn_purchase").prop("disabled",false);
     }
     }



      }
    })


  }

  function Trans_AddItems(){
         var itemID = $('#item_name_option').val();
    var itemQTY = $('#item_qty').val();
 if($("#json_code_text").val().includes('"item_id":"' + itemID + '"') == false){

    $("#items_table").html("");
    var toaddval = "";
    var jsontextarea = $("#json_code_text");
  // CHECK IF FIRST TIME
  if($("#json_code_text").val() == ""){
     toaddval =  $('#json_code_text').val() + '[{"item_id":"' + itemID + '","item_qty":"' + itemQTY + '"}]';
    jsontextarea.val( $('#json_code_text').val() + '[{"item_id":"' + itemID + '","item_qty":"' + itemQTY + '"}]');
  }else{
    var currentvalue = jsontextarea.val();
    currentvalue = currentvalue.replace("}]","},");
    toaddval = '{"item_id":"' + itemID + '","item_qty":"' + itemQTY + '"}]';
    currentvalue += toaddval;
    jsontextarea.val(currentvalue);
  }

  // CONVERT TEXTAREA VALUE TO TABLE
  var textjson = JSON.parse(jsontextarea.val());
  // alert(textjson.length);
  for(var i =0; i < textjson.length; i++){
    !function(i){
$.ajax({
      type : "POST",
      url: "<?php weblink() ?>",
      data: {"tag":"getiteminfo","id":textjson[i]["item_id"]},
      success: function(data){
        data = JSON.parse(data);

    $("#items_table").append('<tr id="row_' + data["id"] + '"><td>' + textjson[i]["item_qty"] + '</td><td>' + data["code"] +  " (" + data["color_code"]+ ")" + '</td><td>' + (data["price"] * data["kilo"]) + '</td><td class="singleitemtotal">' + ((data["price"] * data["kilo"]) *textjson[i]["item_qty"]) + '</td><td><center><a onclick="RemoveTable(this)" data-idofelement="' + "row_" + data["id"]  +  '" data-qty="' + textjson[i]["item_qty"] + '" data-id="' + data["id"] + '" href="#"><i class="fas fa-times-circle"></i></a></center></td></tr>');
      }
    })
    }(i)
  }

 }else{
  alert("Picked item already exist in the 4. Item(s) Table.");
 }

  }

    function RemoveTable(tocontrol){
        var jsontextarea = $("#json_code_text");

     $("#json_code_text").val($("#json_code_text").val().replace('[{"item_id":"' + $(tocontrol).data("id") + '","item_qty":"' + $(tocontrol).data("qty") + '"}]',''));

     $("#json_code_text").val($("#json_code_text").val().replace(',{"item_id":"' + $(tocontrol).data("id") + '","item_qty":"' + $(tocontrol).data("qty") + '"}]',']'));


    $("#json_code_text").val($("#json_code_text").val().replace('[{"item_id":"' + $(tocontrol).data("id") + '","item_qty":"' + $(tocontrol).data("qty") + '"},','['));

        $("#json_code_text").val($("#json_code_text").val().replace(',{"item_id":"' + $(tocontrol).data("id") + '","item_qty":"' + $(tocontrol).data("qty") + '"},',','));


      $("#" + $(tocontrol).data("idofelement")).remove();

   $("#items_table").html("");

  // CONVERT TEXTAREA VALUE TO TABLE


if(jsontextarea.val().length > 3){
  var textjson = JSON.parse(jsontextarea.val());
  // alert(textjson.length);
  for(var i =0; i < textjson.length; i++){
    !function(i){
$.ajax({
      type : "POST",
      url: "<?php weblink() ?>",
      data: {"tag":"getiteminfo","id":textjson[i]["item_id"]},
      success: function(data){
        data = JSON.parse(data);
        
    $("#items_table").append('<tr id="row_' + data["id"] + '"><td>' + textjson[i]["item_qty"] + '</td><td>' + data["code"]  +  " (" + data["color_code"]+ ")" + '</td><td>' + (data["price"] * data["kilo"]) + '</td><td class="singleitemtotal">' + ((data["price"] * data["kilo"]) *textjson[i]["item_qty"]) + '</td><td><center><a onclick="RemoveTable(this)" data-idofelement="' + "row_" + data["id"]  +  '" data-qty="' + textjson[i]["item_qty"] + '" data-id="' + data["id"] + '" href="#"><i class="fas fa-times-circle"></i></a></center></td></tr>');
      }
    })
    }(i)
  }

}


  }


  setInterval(function(){
    var total = 0;
    $(".singleitemtotal").each(function() {
        total += parseInt($(this).html());
    });
    $("#totalofallitems").html(total);
    if($("#inp_cash").val() == ""){
      $("#inp_cash").val(0);
    }
    if($("#in_check").val() == ""){
      $("#in_check").val(0);
    }
    var p1 = parseInt($("#inp_cash").val());
    var p2 = parseInt($("#in_check").val());

    var allitems = parseInt($("#totalofallitems").html());
    var paymenttotal = allitems;
    paymenttotal -= p1;
    paymenttotal -= p2;

    if(paymenttotal < 0){
      paymenttotal =0;
    }
    $("#totalofall").html(paymenttotal);


  },300)


</script>