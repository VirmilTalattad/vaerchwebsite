<?php
include("../conn/conn.php");
$id = $_POST["id"];
$q = "SELECT * FROM transactions WHERE id='" . $id . "' LIMIT 1";
$row = mysqli_fetch_array(mysqli_query($c,$q));
?>
<!DOCTYPE html>
<html>
<head>
	<?php include("../theme/index.php"); ?>
	<style type="text/css">
		@media print{
			.jumbotron{
				display: none;
			}
		}
		label{
			padding: 0px !important;
			margin: 0px !important;
		}
		p{
			padding: 0px !important;
			margin: 0px !important;
		}
		small{
			padding: 0px !important;
			margin: 0px !important;
		}
		br{
			padding: 0px !important;
			margin: 0px !important;
		}
		.linerlabel{
			border-bottom: 1px solid black;
			width: 100%;
			font-size: 12px;
		}
		.corderedline{
			border-bottom: 1px solid black;
			width: 100%;
		}
	</style>
	<title>Printing of Receipt</title>
</head>
<body>
<div class="container">
	<div class="jumbotron">
		<h4>Receipt Printing - <?php echo $row["buyername"] ?> <button class="btn btn-defualt float-right" onclick="window.print()">Print</button></h4>
	</div>
	<div class="card" style="margin-top: 50px;">
		<div class="card-body">
			<div class="row">

				<div class="col-sm-12">
					<center><p><strong>STAR FOR ALL SEASONS GRAIN CORP.</strong><br><small>U-2 Jhosz Ricemill Bldg., Mc Arthur Highway,<br>San Juan, Balagtas, Bulacaan<br>Owned & Operated by: START FOR ALL SEASONS GRAINS CORP.<br>Non-VAT Reg. TIN: 009-879-230-000</small></p></center>
					<p><strong>DRAFT RECIPT</strong></p>
					<div class="row">
						<div class="col-sm-8">
							<div class="form-group">
								<label class="linerlabel">Recieved From:</label>
							</div>
						</div>
							<div class="col-sm-4">
							<div class="form-group">
								<label class="linerlabel">Date:</label>
							</div>
						</div>
						<div class="col-sm-8">
							<div class="form-group">
								<label class="linerlabel">TIN/SC-TIN:</label>
							</div>
						</div>
							<div class="col-sm-4">
							<div class="form-group">
								<label class="linerlabel">Terms:</label>
							</div>
						</div>
						<div class="col-sm-8">
							<div class="form-group">
								<label class="linerlabel">OSCA/PWD ID No:</label>
							</div>
						</div>
							<div class="col-sm-4">
							<div class="form-group">
								<label class="linerlabel">SC/PWD Signature:</label>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<label class="linerlabel">Business Style:</label>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<label class="linerlabel">Address:</label>
							</div>
						</div>
					</div>
				</div>
<!-- 				<div class="col-sm-6">
					<label>Time Date</label>: 
					<strong><u><?php echo date("Y-m-d") ?></u></strong>
				</div>
				<div class="col-sm-6">
					<label>Buyer's Name</label>: 
					<strong><u><?php echo $row["buyername"]; ?></u></strong>
				</div>
 -->

				<div class="col-sm-12">

			<table class="table table-striped table-bordered more_space_table">
 				<thead>
 					<tr>
 						<th>Qty</th>
 						<th>Particulars</th>
 						<th>Price</th>
 						<th>Total</th>
 					</tr>
 				</thead>
 				<tbody>
 					<?php
 					$PaidAmount = $row["cash_payment"] + $row["check_payment"];
 					$GrandTotal = 0;
 					$items_json = json_decode($row["buyeditems"],true);
 					for($i = 0; $i < count($items_json);$i++){

					$q = "SELECT * FROM inventory WHERE id='" . $items_json[$i]["item_id"]. "' LIMIT 1";
					$xrow = mysqli_fetch_array(mysqli_query($c,$q));
					$GrandTotal += (($xrow["price"] * $xrow["kilo"]) * $items_json[$i]["item_qty"]);
 						echo "<tr>
 							<td>" . number_format($items_json[$i]["item_qty"] ). "</td>
 							<td>" . $xrow["code"] . " - " .  $xrow["color_code"] . "</td>
 							<td>₱ " . number_format(($xrow["price"] * $xrow["kilo"])) . "</td>
 							<td>₱ " . number_format((($xrow["price"] * $xrow["kilo"]) * $items_json[$i]["item_qty"]) ). "</td>
 						</tr>";
 					}
 					?>
 				</tbody>
 				<tfoot>
 					 <tr>
 						<th colspan="3" style="text-align: right;">Grand Total</th>
 						<td>₱ 
 							<?php echo number_format($GrandTotal); ?>
 						</td>
 					</tr>
 					<tr>
 						<th colspan="3" style="text-align: right;">Cash</th>
 						<td>₱ 
 							<?php echo number_format($row["cash_payment"]); ?>
 						</td>
 					</tr>
 					<tr>
 						<th colspan="3" style="text-align: right;">Check</th>
 						<td>₱ 
 							<?php echo number_format($row["check_payment"]); ?>
 						</td>
 					</tr>
 					 <tr>
 						<th colspan="3" style="text-align: right;">Balance</th>
 						<td>₱ 
 							<?php echo number_format($GrandTotal - $PaidAmount); ?>
 						</td>
 					</tr>
 				</tfoot>
 			</table>
				</div>
				<div class="col-sm-6">
					
				</div>
				<div class="col-sm-6">
					<hr class="corderedline">
					<center><small>Cashier/Authorized Representative</small></center>
				</div>
<!-- 
				<div class="col-sm-4">
					<label>Approver</label>:<br>
					<strong><u><?php echo $row["approver"]; ?></u></strong>
					<br>
					<br>
					<hr>
					<center><small>Signature over printed name</small></center>
				</div> -->

			</div>
		</div>
	</div>
</div>
</body>
</html>