<nav class="navbar navbar-expand-lg navbar-light bg-light" >

  <a class="navbar-brand" href="dashboard.php"><img src="img/icon.png" width="30" height="30" alt=""> <strong>RiceMill</strong> Inv</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

 <?php
if(isset($_SESSION["email"]) != ""){
	?>

 <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-dharmachakra"></i> Manage
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="slots.php"><i class="fas fa-arrow-right"></i> Slots</a>
          <a class="dropdown-item" href="item.php"><i class="fas fa-arrow-right"></i> Items</a>
        </div>
      </li>


      <li class="nav-item">
        <a class="nav-link" href="trans.php"><i class="fas fa-globe"></i> Transactions</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="user.php"><i class="fas fa-users"></i> Users</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="logs.php"><i class="fas fa-shield-alt"></i> Logs</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0" action="<?php weblink(); ?>" method="POST">
      <input type="hidden" name="tag" value="logout">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fas fa-sign-in-alt"></i> Sign-out</button>
    </form>
  </div>


	<?php
	}
 ?>

</nav>
