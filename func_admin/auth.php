<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if(!isset($_SESSION["vaerch_admin_email"])){
redirect_silent("admin.php");
}
function redirect_silent($pagename){
	?>
	<script type="text/javascript">
		window.location = "" + <?php echo json_encode($pagename); ?>;
	</script>
	<?php
}
?>