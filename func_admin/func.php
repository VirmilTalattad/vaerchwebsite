<?php
include("../conn/c.php");
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
$tag = $_POST["tag"];
switch($tag){
	case "updateassnow":
		$idofass = $_POST["idofass"];
		$inp_title = htmlentities($_POST["inp_title"]);
		$inp_category = htmlentities($_POST["inp_category"]);
		$inp_downloadlink = htmlentities($_POST["inp_downloadlink"]);
		$inp_description = htmlentities($_POST["inp_description"]);
		$inp_downloadlinkname = htmlentities($_POST["inp_downloadlinkname"]);
		$inp_specialmessage = htmlentities($_POST["inp_specialmessage"]);
		$q = $c->prepare("UPDATE homepage_settings SET
			title=?,
			cat=?,
			link=?,
			description=?,
			link_title=?,
			special=?
			WHERE id=?");

		if($q->execute([$inp_title,$inp_category,$inp_downloadlink,$inp_description,$inp_downloadlinkname,$inp_specialmessage,$idofass])){
			echo "true";
		}else{
			echo "false";
		}

	break;
	case 'deleteassetnow':

	$idofass = $_POST["idofass"];
		$q= $c->prepare("DELETE FROM homepage_settings WHERE id=?");
		if($q->execute([$idofass])){
			echo "true";
		}else{
			echo "false";
		}
		break;
	case 'getassetinformationbyid':
		$asset_identification = $_POST["asset_identification"];
		$q =$c->prepare("SELECT * FROM homepage_settings WHERE id=? LIMIT 1");
		if($q->execute([$asset_identification])){
			echo json_encode($q->fetchall(PDO::FETCH_ASSOC));
		}
		break;
	case 'getfeaturedasset':
		$q = $c->prepare("SELECT * FROM homepage_settings WHERE type='online'");
		if ($q->execute()) {
			$row = $q->fetchall(PDO::FETCH_ASSOC);
			for ($i=0; $i < count($row); $i++) { 
				echo "<tr>
					<td><img src='images/homecover/" . $row[$i]["image"] . "' style='width:50px;'></td>
					<td><span>" . $row[$i]["title"] ."</span><br><span class='lessen'>" . $row[$i]["description"] ."</span></td>
					<td>
						<div class='dropdown'>
						  <button class='btn btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
						    Action
						  </button>
						  <div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>
						    <a class='dropdown-item' onclick='load_editfeaturedasset(this)' data-assetid='" . $row[$i]["id"] . "' data-toggle='modal' data-target='#modal_edit_featuredasset' href='#'>Edit Information</a>
						    <a class='dropdown-item' onclick='load_deletefeaturedassed(this)' data-assetid='" . $row[$i]["id"] . "' data-toggle='modal' data-target='#modal_delete_featued_asset_confirmation' href='#'>Delete</a>
						  </div>
						</div>
					</td>
				</tr>";
			}
		}
	break;


	
case 'getcovers':
		$q = $c->prepare("SELECT * FROM homepage_settings WHERE type='cover'");
		if ($q->execute()) {
			$row = $q->fetchall(PDO::FETCH_ASSOC);
			for ($i=0; $i < count($row); $i++) { 
				echo "<tr>
					<td><img src='images/homecover/" . $row[$i]["image"] . "' style='width:50px;'></td>
					<td><span>" . $row[$i]["title"] ."</span><br><span class='lessen'>" . $row[$i]["description"] ."</span></td>
					<td>
						<div class='dropdown'>
						  <button class='btn btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
						    Action
						  </button>
						  <div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>
						    <a class='dropdown-item' onclick='load_editfeaturedasset(this)' data-assetid='" . $row[$i]["id"] . "' data-toggle='modal' data-target='#modal_edit_featuredasset' href='#'>Edit Information</a>
						    <a class='dropdown-item' onclick='load_deletefeaturedassed(this)' data-assetid='" . $row[$i]["id"] . "' data-toggle='modal' data-target='#modal_delete_featued_asset_confirmation' href='#'>Delete</a>
						  </div>
						</div>
					</td>
				</tr>";
			}
		}
	break;




case 'getblognews':
		$q = $c->prepare("SELECT * FROM homepage_settings WHERE type='blog'");
		if ($q->execute()) {
			$row = $q->fetchall(PDO::FETCH_ASSOC);
			for ($i=0; $i < count($row); $i++) { 
				echo "<tr>
					<td><img src='images/homecover/" . $row[$i]["image"] . "' style='width:50px;'></td>
					<td><span>" . $row[$i]["title"] ."</span><br><span class='lessen'>" . $row[$i]["description"] ."</span></td>
					<td>
						<div class='dropdown'>
						  <button class='btn btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
						    Action
						  </button>
						  <div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>
						    <a class='dropdown-item' onclick='load_editfeaturedasset(this)' data-assetid='" . $row[$i]["id"] . "' data-toggle='modal' data-target='#modal_edit_featuredasset' href='#'>Edit Information</a>
						    <a class='dropdown-item' onclick='load_deletefeaturedassed(this)' data-assetid='" . $row[$i]["id"] . "' data-toggle='modal' data-target='#modal_delete_featued_asset_confirmation' href='#'>Delete</a>
						  </div>
						</div>
					</td>
				</tr>";
			}
		}
	break;


	case 'getfeaturedgames':
		$q = $c->prepare("SELECT * FROM homepage_settings WHERE type='game'");
		if ($q->execute()) {
			$row = $q->fetchall(PDO::FETCH_ASSOC);
			for ($i=0; $i < count($row); $i++) { 
				echo "<tr>
					<td><img src='images/homecover/" . $row[$i]["image"] . "' style='width:50px;'></td>
					<td><span>" . $row[$i]["title"] ."</span><br><span class='lessen'>" . $row[$i]["description"] ."</span></td>
					<td>
						<div class='dropdown'>
						  <button class='btn btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
						    Action
						  </button>
						  <div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>
						    <a class='dropdown-item' onclick='load_editfeaturedasset(this)' data-assetid='" . $row[$i]["id"] . "' data-toggle='modal' data-target='#modal_edit_featuredasset' href='#'>Edit Information</a>
						    <a class='dropdown-item' onclick='load_deletefeaturedassed(this)' data-assetid='" . $row[$i]["id"] . "' data-toggle='modal' data-target='#modal_delete_featued_asset_confirmation' href='#'>Delete</a>
						  </div>
						</div>
					</td>
				</tr>";
			}
		}
	break;
	case 'edit_document':
		$inp_id = $_POST["inp_id"];
		$inp_title = $_POST["inp_title"];
		$inp_cat = $_POST["inp_cat"];
		$inp_text = $_POST["inp_text"];
		$date_modif = date("Y-m-d H:i:s");
		$q = $c->prepare("UPDATE slavenote_data SET title=?,category=?,text_content=?,datemodified=? WHERE id=?");

		if($q->execute([$inp_title,$inp_cat,$inp_text,$date_modif,$inp_id])){
			echo "true";
		}else{
			echo "false";
		}
	break;
	case "get_document_by_id":
		$id = $_POST["idofdocument"];
		$q = $c->prepare("SELECT * FROM slavenote_data WHERE id=?");
		if($q->execute([$id])){
			echo json_encode($q->fetchall(PDO::FETCH_ASSOC));
		}
	break;
	case 'insert_new_document':
		$inp_title = htmlentities($_POST["inp_title"]);
		$inp_cat = htmlentities($_POST["inp_cat"]);
		$inp_text = htmlentities($_POST["inp_text"]);

		$date_created = date("Y-m-d H:i:s");
		$date_modified = date("Y-m-d H:i:s");
		$owner = "vaerch@admin.admin";
		$archive = "0";
		$q = $c->prepare("INSERT INTO slavenote_data SET 
		title='" . $inp_title . "',
		category='" . $inp_cat . "',
		text_content='" . $inp_text . "',
		owner='" . $owner . "',
		datecreated='" . $date_created . "',
		datemodified='" . $date_modified . "',
		archive='" . $archive . "'
		");

		if ($q->execute()) {
			echo "Document Created!";
		}else{
			echo "Error Creating Document...";
		}
	break;
	case "LoadAllCategories":
	$q = $c->prepare("SELECT category FROM slavenote_data GROUP BY category");
	if ($q->execute()) {
		$row = $q->fetchall(PDO::FETCH_ASSOC);
		for ($i=0; $i < count($row); $i++) { 
			if($row[$i]["category"] != null && $row[$i]["category"] != "")
			echo "<option>" . $row[$i]["category"] . "</option>";
		}
	}
	break;
	case "get_all_document":
		$q = $c->prepare("SELECT * FROM slavenote_data WHERE archive='0' ORDER BY datecreated DESC");
		if ($q->execute()) {
			$row = $q->fetchall(PDO::FETCH_ASSOC);
			for ($i=0; $i < count($row); $i++) { 
				echo "
					<tr>

					<td>" . date("M d, y",strtotime($row[$i]["datecreated"])) ."<br>" . 
					date("h:i",strtotime($row[$i]["datecreated"]))
					 . "</td>
					 <td>" . $row[$i]["category"] . "</td>
						<td>" ;
						if(strlen($row[$i]["title"]) > 100){
							echo substr($row[$i]["title"], 0,100) . "...";
						}else{	
							echo $row[$i]["title"];
						}
						echo "</td>
						<td>";
						if(strlen($row[$i]["text_content"]) > 400){
							echo substr($row[$i]["text_content"], 0,400) . "...";
						}else{	
							echo $row[$i]["text_content"];
						}
						echo "";


						echo '

</td>
						<td>
<div class="dropdown">
  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	Action
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

   <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modal_edit_doc" onclick="LoadSingleDocument(this)" data-idofdoc="' . $row[$i]["id"] . '">Open and Edit</a>
       <a class="dropdown-item" href="#">Archive</a>
  </div>
</div>
</td></tr>



						';
			}
		}
	break;
	// case "get_all_docus_data":
	// 	$q = 
	// break;
	case 'AddNewPerdayRecord':
	$acc_id = GetMyAccountID($_SESSION["vaerch_admin_email"]);
		$funding_source = htmlentities($_POST["funding_source"]);
		$expense_amount = $_POST["expense_amount"];
		$expense_description = htmlentities($_POST["expense_description"]);
		$dou = date("Y-m-d");
		$dr = date("Y-m-d H:i:s");

		$q = $c->prepare("INSERT INTO vaerch_finance SET
		account_id=?,
		amount=?,
		remarks=?,
		type_of_use=?,
		date_of_use=?,
		date_recorded=?");

		if ($q->execute([$acc_id,$expense_amount,$expense_description,$funding_source,$dou,$dr])) {
			echo "true";
		}else{
			echo "false";
		}

	break;
	case 'getMydailyexpenses':
		$acc_id = GetMyAccountID($_SESSION["vaerch_admin_email"]);
		$q = $c->prepare("SELECT date_of_use,type_of_use FROM vaerch_finance WHERE account_id=? AND archived='0' GROUP BY date_of_use ORDER BY date_of_use DESC LIMIT 7");
		if($q->execute([$acc_id])){
			$row = $q->fetchall(PDO::FETCH_ASSOC);
			for ($i=0; $i < count($row); $i++) { 
			if($row[$i]["type_of_use"] == "Pocket Expense" || $row[$i]["type_of_use"] =="Savings Expense"){
				$curr_date =  $row[$i]["date_of_use"];

				if($curr_date == date("Y-m-d")){
					$curr_date = "Today";
				}
				echo
				"<tr class='row'>
				<td class='col-sm-3'>
				" . $curr_date . "
				</td>
				<td class='col-sm-9'>";

				$x = $c->prepare("SELECT * FROM vaerch_finance  WHERE account_id=? AND date_of_use=? AND archived='0'");
				$x->execute([$acc_id,$row[$i]["date_of_use"]]);
				$xrow = $x->fetchall(PDO::FETCH_ASSOC);
				for ($xc=0; $xc < count($xrow); $xc++) { 
						if($xrow[$xc]["type_of_use"] == "Pocket Expense" || $xrow[$xc]["type_of_use"] =="Savings Expense"){
							?>
								<div class="alert alert-primary" role="alert">
                    <span><small class="lessen float-right"><?php echo date("h:i",strtotime( $xrow[$xc]["date_recorded"])); ?></small><?php echo $xrow[$xc]["remarks"]; ?><br>
                      <strong><?php echo number_format($xrow[$xc]["amount"]); ?></strong></span>
                    </div>
							<?php
						}
				}
				echo "
				</td><tr>";
			}
			}
		}
	break;
	case 'GetTotalAllowancePerDay':
		$acc_id = GetMyAccountID($_SESSION["vaerch_admin_email"]);
		$q = $c->prepare("SELECT * FROM vaerch_finance WHERE account_id=? AND archived='0'");
		$q->execute([$acc_id]);
		$q_json = $q->fetchall(PDO::FETCH_ASSOC);
		$overall_daily = 0;
		$array_toadd = array("Daily Expense from Pocket","Daily Expense from Savings");
		for ($i=0; $i < count($q_json); $i++) { 
			if(in_array($q_json[$i]["type_of_use"], $array_toadd)){
				$overall_daily += $q_json[$i]["amount"];
			}
		}
		echo number_format($overall_daily,2);
		break;
	case 'getTotalPocket':

	$acc_id = GetMyAccountID($_SESSION["vaerch_admin_email"]);
		$q = $c->prepare("SELECT *,SUM(amount) as overall_savings FROM vaerch_finance WHERE account_id=? AND archived='0' AND type_of_use='Add to Pocket'");
		$q->execute([$acc_id]);
		$res = $q->fetchall(PDO::FETCH_ASSOC);
		$overall_savings = $res[0]["overall_savings"];

		// ADD 
		// ------->   Savings tsansfer to wallet
		$aa = $c->prepare("SELECT * FROM vaerch_finance WHERE account_id=? AND archived='0'");
		$aa->execute([$acc_id]);
		$aa_json = $aa->fetchall(PDO::FETCH_ASSOC);
		$array_toadd = array("Savings Transfer to Wallet");
		$array_tominus = array("Wallet Transfer to Saving","Pocket Expense");
		for ($i=0; $i < count($aa_json); $i++) { 

			if(strtotime($aa_json[$i]["date_of_use"]) <= strtotime(date("Y-m-d"))){
				if(   in_array($aa_json[$i]["type_of_use"], $array_toadd )){
				$overall_savings += $aa_json[$i]["amount"];
				}else if(in_array($aa_json[$i]["type_of_use"], $array_tominus )){
				$overall_savings -= $aa_json[$i]["amount"];
				}
			}
			
		}
		// MINUS
		// ------->   Wallet transfer to savings
		// ------->   Wallet expense
		// ------->   Daily Wallet expense


		echo number_format($overall_savings,2);
		break;
	case 'getTotalSavings':

	$acc_id = GetMyAccountID($_SESSION["vaerch_admin_email"]);
		$q = $c->prepare("SELECT *,SUM(amount) as overall_savings FROM vaerch_finance WHERE account_id=? AND archived='0' AND type_of_use='Add to Saving'");
		$q->execute([$acc_id]);
		$res = $q->fetchall(PDO::FETCH_ASSOC);
		$overall_savings = $res[0]["overall_savings"];

		// ADD 
		// ------->   Wallet transfer to savings
		$aa = $c->prepare("SELECT * FROM vaerch_finance WHERE account_id=? AND archived='0'");
		$aa->execute([$acc_id]);
		$aa_json = $aa->fetchall(PDO::FETCH_ASSOC);
		$array_toadd = array("Wallet Transfer to Saving");
		$array_tominus = array("Savings Transfer to Wallet","Savings Expense");
		for ($i=0; $i < count($aa_json); $i++) { 
if(strtotime($aa_json[$i]["date_of_use"]) <= strtotime(date("Y-m-d"))){
			if(   in_array($aa_json[$i]["type_of_use"], $array_toadd )){
				$overall_savings += $aa_json[$i]["amount"];
			}else if(in_array($aa_json[$i]["type_of_use"], $array_tominus )){
				$overall_savings -= $aa_json[$i]["amount"];
			}
			}
		}
		// MINUS
		// ------->   Savings tsansfer to wallet
		// ------->   Saving expense
		// ------->   Daily Saving Expense


		echo number_format($overall_savings,2);
		break;
	case 'get_sum_of_finance':
		$q=$c->prepare("SELECT *,SUM(amount) as overall_sum_of_type FROM vaerch_finance WHERE account_id=? AND archived='0' GROUP BY type_of_use");
		$q->execute([GetMyAccountID($_SESSION["vaerch_admin_email"])]);

$json_data = $q->fetchall(PDO::FETCH_ASSOC);
		for ($i=0; $i < count($json_data); $i++) { 
					echo "<tr>
					<td><h4>" . $json_data[$i]["type_of_use"] . "</h4></td>
					<td><p style='font-size:20px;'>" . number_format($json_data[$i]["overall_sum_of_type"],2) . "</p></td>";
				}
		break;
	case "deletethis_expense":
	$id_of_finance = $_POST["finance_id"];
		$q = $c->prepare("UPDATE vaerch_finance SET archived='1' WHERE id=?");
		if($q->execute([$id_of_finance])){
			backtoPage("Record Archived!","../admin_finance.php");
		}else{
			backtoPage("Failed to Archive Record.","../admin_finance.php");
		}
	break;
	case "getallofmyfinancerecords":

		$output_type = $_POST["output_type"];
		$q=$c->prepare("SELECT * FROM vaerch_finance WHERE account_id=? AND archived='0'");
		$q->execute([GetMyAccountID($_SESSION["vaerch_admin_email"])]);
		$json_data = $q->fetchall(PDO::FETCH_ASSOC);
		switch ($output_type) {
			case '0':
			// JSON
				echo json_encode($json_data);
				break;
					case '1':
			// HTML TABLE
				for ($i=0; $i < count($json_data); $i++) { 
					echo "<tr>
					<td>" . number_format($json_data[$i]["amount"],2) . "</td>
					<td>" . $json_data[$i]["remarks"] . "</td>
					<td>" . $json_data[$i]["date_of_use"] . "</td>
					<td>" . $json_data[$i]["date_recorded"] . "</td>
					<td>" . $json_data[$i]["type_of_use"] . "</td>
					<td><button class='btn btn-primary btn-sm' onclick='ShowArchiveFinanceModal(this)' data-finrid='" . $json_data[$i]["id"]  ."' data-toggle='modal' data-target='#modal_delexpense'>Delete</button></td>
					</tr>";
				}
				break;
		}
		

	break;
	case 'addnewfinancevalue':
	$finance_remarks = htmlentities($_POST["finance_remarks"]);
	$finance_amount = htmlentities($_POST["finance_amount"]);
	$finance_type = htmlentities($_POST["finance_type"]);
	$finance_dateofuse = htmlentities($_POST["finance_dateofuse"]);
	$finance_dateofrecord = date("Y-m-d H:i:s");
	// GET ACCOUNT ID
	$q = $c->prepare("SELECT * FROM admin WHERE username=? LIMIT 1");

	$q->execute([$_SESSION["vaerch_admin_email"]]);

	if($q->rowCount() == 1){
		// ACCOUNT IS EXISTING

		$row = $q->fetchall(PDO::FETCH_ASSOC);
		$finance_id = $row[0]["id"];

		$q = $c->prepare("INSERT INTO vaerch_finance SET
			account_id=?,
			amount=?,
			remarks=?,
			type_of_use=?,
			date_of_use=?,
			date_recorded=?");

		if($q->execute([$finance_id,$finance_amount,$finance_remarks,$finance_type,$finance_dateofuse,$finance_dateofrecord])){
			backtoPage("Finance Added!","../admin_finance.php");
		}else{
			// backtoPage("Finance adding failed.","../admin_finance.php");
		}
	}else{
		backtoPage("Failed to add finance.","../admin_finance.php");
	}

		break;
	case 'sign_out_as_admin':
		session_destroy();
		backtoPage("Signed-out","../admin_sitedashboard.php");
		break;
	case "login_as_admin":
		$username = htmlentities( $_POST["vv_username_admin"]);
		$password = md5($_POST["vv_password_admin"]);
		$q = $c->prepare("SELECT * FROM admin WHERE username=? AND password=? LIMIT 1");
		$q->execute([$username,$password]);
		if($q->rowCount() == 1){
	
			$_SESSION["vaerch_admin_email"] = $username;
			backtoPage("Welcome admin of Vaerch!","../admin_sitedashboard.php");
		}else{
			backtoPage("Failed to login!","../admin.php");
		}
	break;
	case "add_new_prefs":
		$info_name = $_POST["info_name"];
		$info_val = htmlentities($_POST["info_val"]);

		$q = $c->prepare("INSERT INTO infos(info_name, value) VALUES(?,?)");
		if($q->execute([$info_name,$info_val])){
			backtoPage("New Preferences Added!","../admin_sitedashboard.php");
		}else{
			backtoPage("There's a problem adding your preferences.","../admin_sitedashboard.php");
		}
	break;
	case "changecover":
		$target_dir = "../images/homecover/";
		$target_file = $target_dir . basename($_FILES["cov_photo"]["name"]);
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		// Check if image file is a actual image or fake image
		if(isset($_POST["submit"])) {
		    $check = getimagesize($_FILES["cov_photo"]["tmp_name"]);
		    if($check !== false) {
		        $uploadOk = 1;
		    } else {

				backtoPage("File is not an image. Try again...","../admin_sitedashboard.php");
				// redirect("dashboard.php","File is not an image. Try again...");
		        $uploadOk = 0;
		    }
		}
		// Check if file already exists
		if (file_exists($target_file)) {
		    $uploadOk = 0;
		}
		// Check file size
		if ($_FILES["cov_photo"]["size"] > 9000000) {
		    backtoPage("Sorry, your file is too large. Try again...","../admin_sitedashboard.php");
		    // redirect("dashboard.php","Sorry, your file is too large. Try again...");
		    $uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {

		backtoPage("Sorry, only JPG, JPEG, PNG & GIF files are allowed.. Try again...","../admin_sitedashboard.php");
		// redirect("dashboard.php","Sorry, only JPG, JPEG, PNG & GIF files are allowed.. Try again...");
		    $uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    backtoPage("Failed to proccess image. Try again...","../admin_sitedashboard.php");
		    // redirect("dashboard.php","Failed to proccess image. Try again...");

		// if everything is ok, try to upload file
		} else {

		$temp = explode(".", $_FILES["cov_photo"]["name"]);
		$newfilename = round(microtime(true)) . '.' . end($temp);
		$coverfilename = $newfilename;
		    if (move_uploaded_file($_FILES["cov_photo"]["tmp_name"], $target_dir . $coverfilename)) {
		        
			
	$type = $_POST["type"];
			$title = $_POST["title"];
			$description = $_POST["description"];
			$btnlink = $_POST["btnlink"];
			$special = $_POST["special"];
			$btnlink_title = $_POST["btnlink_title"];

$secondary_file = "";
$is_cancel = false;
// PREFERENCES
$moveDir = "../uploads_packages/assets/";
$moveDir_screenshots = "../uploads_packages/screenshots/";
if ($_FILES["fileupload"]["name"] != "") {
// FUNCTION
$file_base = basename($_FILES["fileupload"]["name"]);
$file_name = $_FILES["fileupload"]["name"];
$file_extentsion = strtolower(pathinfo($file_base,PATHINFO_EXTENSION));
$temp = explode(".", $_FILES["fileupload"]["name"]);
$newfilename = round(microtime(true)) . '.' . end($temp);
// VALIDATION
if($file_extentsion == "zip"){
	move_uploaded_file($_FILES["fileupload"]["tmp_name"], $moveDir . $newfilename);
	$secondary_file = $newfilename;
}else{
	$is_cancel = true;
}
}

$screenshot_array = array("file_sc1","file_sc2");
$sc_filenames = array("","");
for ($i=0; $i < count($screenshot_array); $i++) { 
	if ($_FILES[$screenshot_array[$i]]["name"] != "") {
// FUNCTION
$file_base = basename($_FILES[$screenshot_array[$i]]["name"]);
$file_name = $_FILES[$screenshot_array[$i]]["name"];
$file_extentsion = strtolower(pathinfo($file_base,PATHINFO_EXTENSION));
$temp = explode(".", $_FILES[$screenshot_array[$i]]["name"]);
$newfilename = round(microtime(true)) . '.' . end($temp);
// VALIDATION
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif"){
	 $is_cancel = true;
}else{
	$sc_filenames[$i] = "screenshot_"  . $i .  "_". $newfilename;
	move_uploaded_file($_FILES[$screenshot_array[$i]]["tmp_name"], $moveDir_screenshots . "screenshot_" . $i .  "_" . $newfilename);
}
}
}
$sc_filenames = json_encode($sc_filenames);
$q = $c->prepare("INSERT INTO homepage_settings(type,title,description,link,post_timestamp,special,image,link_title,secondary_file,screenshots,owner_id,cat) VALUES(?,?,?,?,?,?,?,?,?,?,'1',?)");

if($is_cancel == false){

	$cat = "unknown";
	if (isset($_POST["category"])) {
		$cat = $_POST["category"];
	}
if($q->execute([$type ,$title ,$description,$btnlink,date("Y-m-d H:i:s"),$special,$coverfilename,$btnlink_title,$secondary_file,$sc_filenames,$cat,])){
backtoPage("Successfully Updated.","../admin_sitedashboard.php");
}else{
echo json_encode($q->errorInfo());
// backtoPage("Failed to Update.","../admin_sitedashboard.php");
}
}else{
backtoPage("Not a valie File To Download file.","../admin_sitedashboard.php");
}

} else {
backtoPage("Failed to upload File or Screenshot.","../admin_sitedashboard.php");
}
}
	break;
}
function GetMyAccountID($username){
	global $c;
	$q = $c->prepare("SELECT * FROM admin WHERE username=? LIMIT 1");
	$q->execute([$username]);
	$row = $q->fetchall(PDO::FETCH_ASSOC);
	return $row[0]["id"];
}

function backtoPage($message,$pagename){
?>
<script type="text/javascript">
	alert(<?php echo json_encode($message); ?>);
	location.href= <?php echo json_encode($pagename); ?>;
</script>
<?php
}
?>