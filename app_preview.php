<?php
include("theme/index.php");
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
<title>Vaerch App</title>
<?php include("inc/essentials.php") ?>
</head>
<body>
<?php include("inc/body_es.php") ?>
<div class="container">
<div class="jumbotron jumbotron-fluid">
	<div class="container">
		<h1>
		Vaerch | <strong>System Solutions</strong>
		</h1>
		<p>Let's make work simple, fast and less irritating.</p>
		<a class="btn btn-info" href="">Email us!</a>
	</div>
</div>
</div>
</body>
</html>