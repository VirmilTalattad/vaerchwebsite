<div class="modal" tabindex="-1" role="dialog" id="modal_edit_featuredasset">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Homepage Content Editor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <input type="hidden" id="idofasstoedit" name="">
       <div class="form-group">
         <label>Title</label>
         <input id="ass_title" type="text" autocomplete="off" class="form-control" name="">
       </div>
       <div class="form-group">
         <label>Category</label>
         <input id="ass_category" type="text" autocomplete="off" class="form-control" name="">
       </div>
       <div class="form-group">
         <label>Link Name</label>
         <input id="ass_dlname" type="text" autocomplete="off" class="form-control" name="">
       </div>
       <div class="form-group">
         <label>Download Link</label>
         <input id="ass_dl" type="text" autocomplete="off" class="form-control" name="">
       </div>
       <div class="form-group">
         <label>Description</label>
         <textarea id="ass_desc" class="form-control" rows="6"></textarea>
       </div>
        <div class="form-group">
         <label>Special Text</label>
         <input id="ass_sptxt" type="text" autocomplete="off" class="form-control" name="">
       </div>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="act_edit()" data-dismiss="modal" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="modal" tabindex="-1" role="dialog" id="modal_delete_featued_asset_confirmation">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Deleted Featured Asset</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" id="idofasstodelete" name="">
        <p>Are you sure you want to delete this Featured Asset?</p>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" onclick="act_delete()" class="btn btn-primary">Yes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function load_editfeaturedasset(controlOBJ){
    var assID = $(controlOBJ).data("assetid");
    $.ajax({
      type: "POST",
      url: "func_admin/func.php",
      data:{tag: "getassetinformationbyid",asset_identification:assID},
      success: function(data){
        data = JSON.parse(data);
        $("#idofasstoedit").val(data[0]["id"]);
$("#ass_title").val(data[0]["title"]);
$("#ass_category").val(data[0]["cat"]);
$("#ass_dl").val(data[0]["link"]);
$("#ass_dlname").val(data[0]["link_title"]);
$("#ass_desc").val(data[0]["description"]);
$("#ass_sptxt").val(data[0]["special"]);

      }
    })
  }
  function load_deletefeaturedassed(controlOBJ){
    var assID = $(controlOBJ).data("assetid");
    $("#idofasstodelete").val(assID);
  }

  function act_edit(){
    var inp_ii = $("#idofasstoedit").val();
    var inp_tt = $("#ass_title").val();
    var inp_cc = $("#ass_category").val();
    var inp_dl = $("#ass_dl").val();
    var inp_dn = $("#ass_desc").val();
    var inp_dlname = $("#ass_dlname").val();
    var inp_special = $("#ass_sptxt").val();

    $.ajax({
      type:"POST",
      url: "func_admin/func.php",
      data: {tag:"updateassnow",idofass:inp_ii
        ,inp_title:inp_tt
        ,inp_category:inp_cc
        ,inp_downloadlink:inp_dl
        ,inp_description:inp_dn
        ,inp_downloadlinkname:inp_dlname
        ,inp_specialmessage:inp_special
    },
      success: function(data){
          // alert(data);
        LoadFeaturedAssets();
        LoadFeaturedGames();
        LoadBlogNews();
        LoadCoverPages();
      }
    })
  }
  function act_delete(){
    var idofass = $("#idofasstodelete").val();
    $.ajax({
      type: "POST",
      url: "func_admin/func.php",
      data: {tag:"deleteassetnow",idofass},
      success:function(data){
        // alert(data);
          LoadFeaturedAssets();
      }
    })
  }
</script>


<form action="func_admin/func.php" method="POST" enctype="multipart/form-data">
	<div class="modal " tabindex="-1" role="dialog" id="modal_changecover">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<input type="hidden" value="changecover" name="tag">
      		
      	  <h4 class="modal-title">Compose a new Dynamic Post</h4>
      	  <span>Vaerch Dynamic Posting</span>
<div class="row">
	<div class="col-sm-4">
		<div class="form-group">
	<label>Type</label>
	<select name="type" id="input_post_type" class="form-control" >
      			<option  value="cover">Cover Image</option>
      			<option  value="blog">Blog News</option>
      			<option  value="game">Featured Game</option>
      			<option  value="online">Featured Asset</option>
      		</select>
</div>

	</div>
	<div class="col-sm-8">
		<div class="form-group">
        	<label>Title</label>
        	<input type="text" autocomplete="off" class="form-control" placeholder="Type here..." name="title">
        </div>
	</div>
  <div class="col-sm-12" id="categoryinput">
    <div class="form-group">
      <label>Choose Category</label>
      <input type="text" class="form-control" list="inp_cats" name="category">
      <datalist id="inp_cats">
        <option>Environment & Decoration</option>
        <option>Character, Creatures</option>
        <option>Weapons & Accessories</option>
        <option>Clothings and Wearables</option>
        <option>Vehicles</option>
        <option>Buildings</option>
        <option>Gadgets and Tech</option>
        <option>Effects & Sound</option>
      </datalist>
    </div>
  </div>
</div>
             <div class="row">
               <div class="col-sm-6">
                    <div class="form-group">
          <label>Description</label>
        <textarea class="form-control" autocomplete="off"  placeholder="Add description here..." name="description"></textarea>
        </div>
            <div class="row">
              <div class="col-sm-6">
                    <div class="form-group">
          <label>Button Link Title</label>
          <input type="text" id="input_button_link" autocomplete="off" class="form-control"  name="btnlink_title">
        </div>
              </div>
              <div class="col-sm-6">
                    <div class="form-group">
          <label>Button Link</label>
          <input type="text" autocomplete="off" class="form-control"  name="btnlink">
        </div>
              </div>
            </div>
                <div class="form-group">
          <label id="lbl_special">Special Message</label>
          <input type="text" autocomplete="off" class="form-control"  name="special">
        </div>
      
               </div>
               <div class="col-sm-6">
                   <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
          <label>Cover Photo</label><br>
          <input type="file" name="cov_photo" >
        </div>
          </div>
           <div class="col-sm-12" id="file_uploader">
             <div class="form-group">
          <label id="lbl_fileupload">File</label><br>
          <input type="file" name="fileupload">
        </div>
          </div>
          <div class="col-sm-12" id="inp_additional_screenshots">
            <label>2 Additional Screenshots</label><br>
            <input type="file" name="file_sc1">
             <input type="file" name="file_sc2">
          </div>
        </div>
               </div>
             </div>
        
       
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
</form>




<form action="func_admin/func.php" method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="modal_addnewinfodata">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">New Preferences</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <input type="hidden" value="add_new_prefs" name="tag">
         <div class="form-group">
          <label>Information Name</label>
          <input type="text" name="info_name" required="" class="form-control" placeholder="Type here...">
         </div>
         <div class="form-group">
          <label>Information Value</label><br>
          <textarea class="form-control" name="info_val" required="" placeholder="Type here..."></textarea>
         </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit Preferences</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
</form>


<script type="text/javascript">
  $("#input_post_type").change(function(){

validate_compose_inputs(this);
  });

  function validate_compose_inputs(controlOBJ){
    
    $("#file_uploader").css("display","none");
   $("#inp_additional_screenshots").css("display","none");
   $("#categoryinput").css("display","none");
  var current_value = $(controlOBJ).val();
    switch(current_value){
      case "cover":
        $("#input_button_link").val("Learn More");
        $("#lbl_special").html("Remarks");
        $("#lbl_fileupload").html("Secondary File");
      break;
      case "blog":
        $("#input_button_link").val("More");
          $("#lbl_special").html("Hastag or Remarks");
        $("#lbl_fileupload").html("Secondary File");
      break;
      case "game":
        $("#input_button_link").val("Download on Playstore");
          $("#lbl_special").html("Company Name");
        $("#lbl_fileupload").html("Secondary File");

      break;
      case "online":
        $("#input_button_link").val("Download");
          $("#lbl_special").html("Company Name.");
        $("#lbl_fileupload").html("File To Be Downloaded");
      $("#categoryinput").css("display","block");
      $("#inp_additional_screenshots").css("display","block");
      $("#file_uploader").css("display","block");
      break;
    }
  }
</script>