<form action="func/server.php" method="POST">
  <div class="modal" tabindex="-1" role="dialog" id="modal_postnewacreation">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Publish new Creation</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <!-- POSTING -->
      <input type="hidden" value="add_post" name="tag">
      <input type="text" id="merchname_forposting" name="pos_companyname">
      <input type="text" id="merchwebsite_forposting" name="pos_websitename">
<div class="row">
  <div class="col-sm-4">
  </div>
  <div class="col-sm-12">
    <div class="form-group">
          <label>Content Name</label>
          <input type="text" autocomplete="off" class="form-control"  placeholder="Type here..." name="post_contentname">
        </div>
  </div>
</div>
             <div class="row">
               <div class="col-sm-6">
                    <div class="form-group">
          <label>Description</label>
        <textarea class="form-control"  placeholder="Add description here..." name="pos_description"></textarea>
        </div>
            <div class="row">
              <div class="col-sm-6">
                    <div class="form-group">
          <label>Button Link Title</label>
          <input type="text" value="Visit Creators Website" id="input_button_link" class="form-control"  name="btnlink_title">
        </div>
              </div>
              <div class="col-sm-6">
                    <div class="form-group">
          <label>Button Link</label>
          <input type="text"  class="form-control" value="" id="bwebsite" name="btnlink">
          <script type="text/javascript">
            $.ajax({
              type: "POST",
              url: "ajax/func.php",
              data: {tag: "getweblink",email_v:<?php echo json_encode($_SESSION["vaerch_email"]); ?>},
              success: function(data){
                // alert(data);
                $("#bwebsite").val(data);
              }
            })
          </script>
        </div>
              </div>
            </div>
                <div class="form-group">
          <label id="lbl_special">Category</label>
          <select class="form-control" required="">
            <option disabled="" selected="">Choose Content Type...</option>
            <option>Character Texture</option>
            <option>Terrain Texture</option>
            <option>Textures</option>
            <option>Material</option>
            <option>3D Character</option>
            <option>3D Objects</option>
            <option>3D Building</option>
            <option>3D Environment</option>
              <option>3D Animation</option>
            <option>2D Animation</option>
            <option>2D Character</option>
            <option>2D Objects</option>
            <option>2D Building</option>
            <option>2D Environment</option>
            <option>2D</option>
            <option>3D</option>
            <option>Animated Texture</option>
            <option>Fantacy</option>
            <option>Sci-Fi</option>
            <option>Strategy</option>
            <option>Music</option>
            <option>Background Music</option>
            <option>FX Music</option>
            <option>Stereo</option>
            <option>2D Sprite</option>
            <option>2D Spritesheet</option>
            <option>3D + 2D</option>
            <option>Others</option>
          </select>
        </div>
      
               </div>
               <div class="col-sm-6">
                   <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
          <label>Cover Photo</label><br>
          <input type="file" name="cov_photo" >
        </div>
          </div>
           <div class="col-sm-12">
             <div class="form-group">
          <label id="lbl_fileupload">File</label><br>
          <input type="file" name="fileupload">
        </div>
          </div>
          <div class="col-sm-12">
            <label>2 Additional Screenshots</label><br>
            <input type="file" name="file_sc1">
             <input type="file" name="file_sc2">
          </div>
        </div>
               </div>
             </div>
        
          <!-- POSTING -->
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Save changes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</form>


<form  action="<?php echo weblink(); ?>" method="POST" enctype="multipart/form-data">
<div class="modal" tabindex="-1" role="dialog" id="modal_reg_super3d">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Super 3D Registration</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <input type="hidden" value="resgister_acc_new" name="tag">
            <div class="form-group">
            <label>Merchant Name</label>
            <input required="" type="text" class="form-control" name="m_name" placeholder="Name your store...">
          </div>
          <div class="form-group">
            <label>Your store description</label>
            <textarea required="" name="m_desc" class="form-control" placeholder="Ex: We create 3D characters and stuff..."></textarea>
          </div>
          <div class="form-group">
            <label>Merchant Contact Email<br><small>So your fans can reach, admire, and suggest things that may help improve your creation etc.</small></label>
            <input type="email" required class="form-control" name="m_emailsupport">
          </div>
          <div class="form-group">
            <label>Store Icon</label>
            <br>
            <input required="" type="file" name="m_icon">
          </div>
          <div class="form-group">
            <label>Store Cover Photo</label>
            <br>
            <input required="" type="file" name="m_cover">
          </div>
          <div class="form-group">
           
          </div>

      </div>
      <div class="modal-footer">
         <button type="submit" class="btn btn-primary"><i class="fas fa-arrow-circle-right"></i> Create a Super 3D Merchant Account</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form>
<form action="<?php echo weblink(); ?>" method="POST" enctype="multipart/form-data">
  <div class="modal" tabindex="-1" role="dialog" id="modal_changepropic">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Change Profile Picture</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="tag" value="acc_control_changeprofilepicture">
        <input type="hidden" value="<?php echo $_SESSION['vaerch_email']; ?>" name="xxemail">

        <div class="form-group">
          <label>Choose image file</label>
          <input type="file" name="profilepic">
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Begin Upload</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>