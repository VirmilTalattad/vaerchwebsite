	<!-- CHARSET AND MOBILE VIEW -->
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- BOOTSTRAP -->
	<link rel="stylesheet" type="text/css" href="components/essentials/core/css/bootstrap.min.css">
	<!-- JQUERY, POPPER, BOOTSRAP JS -->
	<script type="text/javascript" src="components/essentials/core/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="components/essentials/core/popper.min.js"></script>
	<script type="text/javascript" src="components/essentials/core/js/bootstrap.min.js"></script>
	<!-- THEME -->
<!-- 	<link rel="stylesheet" type="text/css" href="theme/sahara/style.css"> -->
	<link href="components/essentials/core/fontaws/css/all.css" rel="stylesheet">
	<!-- DATA TABLE -->

	<link rel="stylesheet" type="text/css" href="components/essentials/core/DataTables/datatables.min.css"/>
 
<script type="text/javascript" src="components/essentials/core/DataTables/datatables.min.js"></script>

<!-- CHART JS -->
<script src="components/essentials/core/chartjs/dist/Chart.min.js"></script>