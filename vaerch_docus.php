<?php
include("theme/index.php");
include("func_admin/auth.php");
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
<title>Vaerch Dot.Admin</title>
<?php include("inc/essentials.php") ?>
</head>
<body>
<?php include("admin_components/nav.php") ?>
	<div class="container">
		<br>
		<h1>Docus</h1>
		<p>Documentation System</p>
		<div class="row">
			<div class="col-sm-10">
				<div class="form-group">
					<div class="row">
					</div>
					<table class="table table-sm table-bordered" id="tbl_allinfomodal">
						<thead>
							<tr>
								<th>Created</th>
								<th>Category</th>
								<th>Title</th>
								<th>Preview</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="tbl_of_documentation">
							
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-sm-2">
				<h4>Actions</h4>
				<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal_comp_new_doc">Compose new Document</button>
			</div>
		</div>
</div>
</body>
</html>
<?php
include("admin_components/docus_modal.php");
?>
<script type="text/javascript">
	$("#tbl_allinfomodal").DataTable();
	ReloadTableDocu();


	function ReloadTableDocu(){
		$("#tbl_allinfomodal").DataTable().destroy();
		$.ajax({
		type: "POST",
		url: "func_admin/func.php",
		data: {tag:"get_all_document"},
		success: function(data){
		// alert(data);
		$("#tbl_of_documentation").html(data);
		$("#tbl_allinfomodal").DataTable({
			"ordering": false
		});
		}
		})
	}
</script>